matplotlib~=3.3.2
scipy~=1.5.3
PySide2~=5.15.1
numpy~=1.19.3
pyinstaller
openpyxl~=3.0.5