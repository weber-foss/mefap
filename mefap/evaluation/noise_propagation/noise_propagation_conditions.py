import numpy as np
import math
from scipy.spatial import distance

from mefap.evaluation.noise_propagation.noise_propagation_list_entry import NoisePropagationEntry
from mefap.evaluation.noise_propagation.bresenham_line import Bresenham
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_free_field
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_semi_diffus
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_flat_room


class NoisePropagationMatrix:
    def __init__(self, columns, rows):
        self.noise_prop_list = []
        # self.noise_prop_matrix = np.zeros(shape=(layout.size, layout.shape[0], layout.shape[1]))

        for cell_y in range(0, columns, 1):
            for cell_x in range(0, rows, 1):
                self.noise_prop_list.append(NoisePropagationEntry(cell_x, cell_y, columns, rows))

    def get_immission_cell_value(self, source_cell, immission_cell):
        for entry in self.noise_prop_list:
            if source_cell == entry.cell:
                immission_cell_value = entry.layout[immission_cell[0], immission_cell[1]]
        return immission_cell_value

    def get_np_layout_by_cell(self, source_cell):
        for entry in self.noise_prop_list:
            if source_cell == entry.cell:
                return entry.layout

    def calc_noise_propagation_conditions(self, factory) -> None:
        """
        Calculate a noise_propagation_matrix for all cells in Layout -> 3D-Matrix with deltaL (N x N x N)

        - Consistency with modules of ..evaluation.noise_propagation.calc_noise_propagation should be maintained (GUI)
        """

        # todo: ausschließen von "-50" cells um rechenaufwand zu verringen?

        for cell_y in range(0, factory.layout.shape[0], 1):
            for cell_x in range(0, factory.layout.shape[1], 1):

                # set flac
                barrier_flac = False
                wall_corner_flac = False

                # get right sub_layout from NPM
                curr_npc_layout = self.get_np_layout_by_cell([cell_y, cell_x])

                # get niose_area_id of cell
                cell_noise_area_id = factory.noise_area.get_noise_area_id(cell_x, cell_y)

                # get noise_wall_list
                noise_wall_list = factory.noise_area.get_all_noise_walls()

                for immission_cell_y in range(0, factory.layout.shape[0], 1):
                    for immission_cell_x in range(0, factory.layout.shape[1], 1):

                        if cell_y == immission_cell_y and cell_x == immission_cell_x:
                            continue

                        immission_cell_noise_area_id = factory.noise_area.get_noise_area_id(immission_cell_x,
                                                                                            immission_cell_y)

                        # get noise propagation path from source to immission point
                        bresenham = Bresenham([cell_y, cell_x], [immission_cell_y, immission_cell_x])
                        # check direction of path
                        if bresenham.path[0] != [cell_y, cell_x]:
                            bresenham.path.reverse()
                        #########################################################################################
                        # count each wall and reduce massively
                        # !!! not validated !!!
                        """
                        # reset wall_counter
                        wall_counter = [cell_noise_area_id]
                        
                        # check if cell in path is barrier, wall, corner
                        for path_cell in bresenham.path:
                        
                            if factory.noise_area.noise_area_matrix[path_cell[0], path_cell[1]] != wall_counter[-1]:
                                wall_counter.append(factory.noise_area.noise_area_matrix[path_cell[0], path_cell[1]])

                        wall_count = 0
                        for entry in range(1, len(wall_counter), 1):
                            if wall_counter[entry - 1] != wall_counter[entry]:
                                wall_count += 1
                        """
                        #########################################################################################
                        # take only one wall
                        wall_count = 0
                        barrier_count = 0

                        # check if cell in path is barrier, wall, corner
                        for path_cell in bresenham.path:

                            if factory.noise_area.is_noise_wall(path_cell[1], path_cell[0]):
                                if immission_cell_noise_area_id != cell_noise_area_id:
                                    wall_count += 1
                                    break
                                elif immission_cell_noise_area_id == -1 and cell_noise_area_id == -1:
                                    barrier_count += 1
                                    break
                                # kill some noise

                        ###########################
                        # calc full propagation

                        if wall_count == 0:
                            # calc euclidean distance
                            euclidean_distance = distance.euclidean([cell_y, cell_x],
                                                                    [immission_cell_y,
                                                                     immission_cell_x]) * factory.cell_size

                            # calc sound pressure reducation value
                            if factory.parameter["noise_semi_diffuse"]:
                                curr_npc_layout[immission_cell_y, immission_cell_x] = calc_noise_propagation_semi_diffus(
                                    factory.parameter, 0, euclidean_distance, cell_noise_area_id)

                            elif factory.parameter["noise_flat_room"]:
                                curr_npc_layout[immission_cell_y, immission_cell_x] = calc_noise_propagation_flat_room(
                                    factory.parameter, 0, euclidean_distance, factory.restrictions[2].matrix[0, 0])
                            else:
                                curr_npc_layout[immission_cell_y, immission_cell_x] = calc_noise_propagation_free_field(
                                    factory.parameter, 0, euclidean_distance)
                        else:
                            # if noise_wall between noise_source and immission_cell -> set 0, because other calc
                            curr_npc_layout[immission_cell_y, immission_cell_x] = 0

"""
old version: with easy/direct reduction through noise wall

                        # calc euclidean distance
                        euclidean_distance = distance.euclidean([cell_y, cell_x],
                                                                [immission_cell_y,
                                                                 immission_cell_x]) * factory.cell_size

                        # calc sound pressure reducation value
                        if factory.parameter["noise_semi_diffuse"]:
                            curr_npc_layout[immission_cell_y, immission_cell_x] = \
                                (0 - (((factory.parameter["sound_pressure_reduction"] / math.log10(2))
                                       * math.log10(euclidean_distance))
                                      - factory.parameter["sound_propagation_conditions"])
                                 + (10 * math.log10(1 + ((8 * math.pi * math.pow(euclidean_distance, 2))
                                                         / factory.parameter["noise_room_condition"][immission_cell_noise_area_id])))
                                 - factory.parameter["noise_factory_wall_isolation"] * wall_count
                                 - factory.parameter["noise_barrier_isolation"] * barrier_count)
                        elif factory.parameter["noise_flat_room"]:
                            if wall_count == 0:
                                height = factory.restrictions[2].matrix[0, 0]  # todo: make overall and calc only ones
                                rho_floor = 0.9  # = 1 - alpha_Boden
                                rho_ceil = 0.7  # = 1 - alpha_Decke
                                rho_a = (rho_ceil+rho_floor) / 2  # arithmetic mean
                                rho_g = math.sqrt(rho_floor*rho_ceil)  # geometric mean
                                b = 1 + math.log(1+(0.66/(1-rho_g)))  # schirmer 2006, S. 393
                                curr_npc_layout[immission_cell_y, immission_cell_x] = (0 + 10 * math.log10((1/(math.pi * math.pow(height)))*((rho_a/math.pow((1+math.pow(euclidean_distance)/math.pow(height)), 3/2))+(math.pow(rho_g)/(1-rho_g))*(b/math.pow(math.pow(b) + math.pow(euclidean_distance)/math.pow(height), 3/2)))))
                            else:
                                S_T = 240
                                L_wt = (0 - factory.parameter["noise_factory_wall_isolation"] + 10 * math.log10(S_T/1) - 3)
                                curr_npc_layout[immission_cell_y, immission_cell_x] =
                        else:
                            curr_npc_layout[immission_cell_y, immission_cell_x] = \
                                (0 - (((factory.parameter["sound_pressure_reduction"] / math.log10(2))
                                       * math.log10(euclidean_distance))
                                      - factory.parameter["sound_propagation_conditions"])
                                 - factory.parameter["noise_factory_wall_isolation"] * wall_count
                                 - factory.parameter["noise_barrier_isolation"] * barrier_count)
"""
