from scipy.spatial import distance
import numpy as np
import math
from mefap.evaluation.noise_propagation.bresenham_line import Bresenham


def calc_noise_propagation_free_field(parameter, noise_emission, euclidean_distance) -> float:
    return noise_emission - ((parameter["sound_pressure_reduction"] / math.log10(2))
                             * math.log10(euclidean_distance)) - parameter["sound_propagation_conditions"]


def calc_noise_propagation_flat_room(parameter, noise_emission, euclidean_distance, height) -> float:
    rho_floor = 1 - parameter["noise_absorption_floor"]
    rho_ceil = 1 - parameter["noise_absorption_ceil"]
    rho_a = (rho_ceil + rho_floor) / 2  # arithmetic mean
    rho_g = math.sqrt(rho_floor * rho_ceil)  # geometric mean
    b = 1 + math.log(1 + (0.66 / (1 - rho_g)))  # schirmer 2006, S. 393
    # calc noise_immission
    return noise_emission + 10 * math.log10(
        (1 / (math.pi * math.pow(height, 2))) * (
                (rho_a / math.pow((1 + math.pow(euclidean_distance, 2) / math.pow(height, 2)), 3 / 2))
                + (math.pow(rho_g, 2) / (1 - rho_g))
                * (b / math.pow(math.pow(b, 2) + math.pow(euclidean_distance, 2) / math.pow(height, 2), 3 / 2))))


def calc_noise_propagation_semi_diffus(parameter, noise_emission, euclidean_distance, noise_area) -> float:
    return noise_emission - (
            ((parameter["sound_pressure_reduction"] / math.log10(2)) * math.log10(euclidean_distance))
            - parameter["sound_propagation_conditions"]) \
           + (10 * math.log10(1 + ((8 * math.pi * math.pow(euclidean_distance, 2))
                                   / parameter["noise_room_condition"][noise_area])))


def calc_noise_propagation_layout(facility, factory, noise_source_pos) -> None:
    """
    calculation of noise_propagation_layout for a facility WITHOUT considering NoiseWalls
    - actual only used re_calc_matrix for gui
    - for consideration of NoiseWalls see calc_noise_propagation_layout_extended
    """

    # get noise_emission
    noise_emission = factory.facility_characteristics[facility.index_num].noise_exposure

    # init noise_propagation_layout
    facility.noise_propagation_layout = np.zeros(shape=(factory.columns, factory.rows))

    # check if noise source
    if noise_emission > 0:
        # set noise source
        facility.noise_propagation_layout[noise_source_pos[0], noise_source_pos[1]] = noise_emission
        # calc noise propagation over layout for each noise source
        for y_index in range(0, facility.noise_propagation_layout.shape[0], 1):
            for x_index in range(0, facility.noise_propagation_layout.shape[1], 1):
                # calc euclidean distance
                measuring_point = [y_index, x_index]
                euclidean_distance = distance.euclidean(noise_source_pos, measuring_point) * factory.cell_size
                if euclidean_distance == 0:
                    continue

                # get noise_area_id of  measuring_point
                measuring_point_noise_area_id = factory.noise_area.get_noise_area_id(measuring_point[1],
                                                                                     measuring_point[0])
                # calc sound pressure value at measuring point
                if factory.parameter["noise_semi_diffuse"]:
                    noise_pressure_measuring_point = calc_noise_propagation_semi_diffus(
                        factory.parameter, noise_emission, euclidean_distance, measuring_point_noise_area_id)

                elif factory.parameter["noise_flat_room"]:
                    noise_pressure_measuring_point = calc_noise_propagation_flat_room(
                        factory.parameter, noise_emission, euclidean_distance, factory.restrictions[2].matrix[0, 0])
                else:
                    noise_pressure_measuring_point = calc_noise_propagation_free_field(
                        factory.parameter, noise_emission, euclidean_distance)

                if noise_pressure_measuring_point >= 0:
                    facility.noise_propagation_layout[y_index][x_index] = noise_pressure_measuring_point
                else:
                    facility.noise_propagation_layout[y_index][x_index] = 0

    facility.noise_source_position = noise_source_pos


def calc_noise_room_condition(factory):
    # R´ = A´ges / (1 - α´)
    # A´ges = sum(αi*Ai) + sum(A´j) | A´j = Absorptoion von Menschen und Einrichtungen
    # αges = Ages/ sum(Ai)
    # based on Ingenieurakustik, Sinambari, Gh. Reza; Sentpali, Stefan, S. 411 - 413

    noise_areas = factory.noise_area.get_all_noise_walls()

    for noise_area_id in noise_areas:
        noise_area = factory.noise_area.get_noise_walls_by_index(noise_area_id)
        noise_area_length = noise_area[noise_area.shape[0] - 1, 0] - noise_area[0, 0] + 1
        noise_area_width = noise_area[noise_area.shape[0] - 1, 1] - noise_area[0, 1] + 1

        height = factory.restrictions[2].matrix[noise_area[0, 0], noise_area[0, 1]]

        A_sum = ((math.pow(factory.cell_size, 2) * noise_area_length * noise_area_width) * 2  # ceiling + floor
                 + (factory.cell_size * noise_area_length * height) * 2  # wand north + south
                 + (factory.cell_size * noise_area_width * height) * 2)  # wand east + west

        A_strich_ges = ((math.pow(factory.cell_size, 2) * noise_area_length * noise_area_width) *
                        factory.parameter["noise_absorption_floor"]
                        + (math.pow(factory.cell_size, 2) * noise_area_length * noise_area_width) *
                        factory.parameter["noise_absorption_ceil"]
                        + ((factory.cell_size * noise_area_length * height) * 2 +
                           (factory.cell_size * noise_area_width * height) * 2) *
                        factory.parameter["noise_absorption_wall"])

        alpha_sum = A_strich_ges / A_sum

        R_strich = A_strich_ges / (1 - alpha_sum)
        factory.parameter["noise_room_condition"].update({noise_area_id: R_strich})

    # calc A_stich_ges for noise_area = -1 (factory)
    height = factory.restrictions[2].matrix[0, 0]

    # todo: Reduce A_strich considering NoiseWalls
    A_sum = ((math.pow(factory.cell_size, 2) * factory.columns * factory.rows) * 2  # ceiling
             + (factory.cell_size * factory.columns * height) * 2  # wand north + south
             + (factory.cell_size * factory.rows * height) * 2)  # wand east + west

    A_strich_ges = ((math.pow(factory.cell_size, 2) * factory.columns * factory.rows) *
                    factory.parameter["noise_absorption_floor"]
                    + (math.pow(factory.cell_size, 2) * factory.columns * factory.rows) *
                    factory.parameter["noise_absorption_ceil"]
                    + ((factory.cell_size * factory.columns * height) * 2 +
                       (factory.cell_size * factory.rows * height) * 2) *
                    factory.parameter["noise_absorption_wall"])

    alpha_sum = A_strich_ges / A_sum
    R_strich = A_strich_ges / (1 - alpha_sum)

    factory.parameter["noise_room_condition"].update({-1: R_strich})

    # print(factory.parameter["noise_room_condition"])


def add_up_noise(factory, noise_pressure_layouts):
    # add up noise -> get resulting sound pressure level
    for y_index in range(0, factory.layout.shape[0], 1):
        for x_index in range(0, factory.layout.shape[1], 1):
            resulting_noise_pressure = 0
            for noise_sources in range(0, len(noise_pressure_layouts), 1):
                resulting_noise_pressure = resulting_noise_pressure \
                                           + pow(10, (noise_pressure_layouts[noise_sources][y_index][x_index] / 10))

            # check if resulting_noise_pressure is valid >= 1 --> ln(0) = -infinity
            if resulting_noise_pressure >= 1:
                # calc resulting noise
                if factory.parameter["noise_calc_wall_corner"] and [y_index, x_index] in factory.noise_wall_list:
                    factory.noise_matrix[y_index, x_index] = 10 * math.log10(resulting_noise_pressure) + 3
                elif factory.parameter["noise_calc_wall_corner"] and [y_index, x_index] in factory.noise_corner_list:
                    factory.noise_matrix[y_index, x_index] = 10 * math.log10(resulting_noise_pressure) + 9
                else:
                    factory.noise_matrix[y_index, x_index] = 10 * math.log10(resulting_noise_pressure)
            else:
                factory.noise_matrix[y_index, x_index] = 0


def calc_noise_propagation_layout_extended(facility, factory, noise_source_pos) -> None:
    """
    calculation of noise_propagation_layout for a facility WITH considering NoiseWalls
    - actual only used re_calc_matrix for gui
    - without consideration of NoiseWalls see calc_noise_propagation_layout
    """

    # get noise_emission
    noise_emission = factory.facility_characteristics[facility.index_num].noise_exposure

    # init noise_propagation_layout
    facility.noise_propagation_layout = np.zeros(shape=(factory.columns, factory.rows))

    # check if noise source
    if noise_emission > 0:

        facility.noise_propagation_layout[noise_source_pos[0], noise_source_pos[1]] = noise_emission

        # get noise_area_id of cell
        cell_noise_area_id = factory.noise_area.get_noise_area_id(noise_source_pos[1], noise_source_pos[0])

        # calc noise propagation over layout for each noise source
        for y_index in range(0, facility.noise_propagation_layout.shape[0], 1):
            for x_index in range(0, facility.noise_propagation_layout.shape[1], 1):
                # calc euclidean distance
                measuring_point = [y_index, x_index]
                euclidean_distance = distance.euclidean(noise_source_pos, measuring_point) * factory.cell_size

                if euclidean_distance == 0:
                    continue

                # get noise_area_id of  measuring_point
                measuring_point_noise_area_id = factory.noise_area.get_noise_area_id(measuring_point[1],
                                                                                     measuring_point[0])

                ##############################################
                # calc walls
                # get noise propagation path from source to immission point
                bresenham = Bresenham(noise_source_pos, measuring_point)
                # check direction of path
                if bresenham.path[0] != noise_source_pos:
                    bresenham.path.reverse()

                # take only one wall
                wall_count = 0
                barrier_count = 0

                # check if cell in path is barrier, wall, corner
                for path_cell in bresenham.path:

                    if factory.noise_area.is_noise_wall(path_cell[1], path_cell[0]):
                        if measuring_point_noise_area_id != cell_noise_area_id:
                            # kill some noise
                            wall_count += 1
                            break
                        elif measuring_point_noise_area_id == -1 and cell_noise_area_id == -1:
                            # kill some noise
                            barrier_count += 1
                            break

                ##############################################

                if wall_count == 0:
                    # calc sound pressure value at measuring point
                    if factory.parameter["noise_semi_diffuse"]:
                        noise_pressure_measuring_point = calc_noise_propagation_semi_diffus(
                            factory.parameter, noise_emission, euclidean_distance, measuring_point_noise_area_id)
                        # - factory.parameter["noise_factory_wall_isolation"] * wall_count
                        # - factory.parameter["noise_barrier_isolation"] * barrier_count)
                    elif factory.parameter["noise_flat_room"]:
                        noise_pressure_measuring_point = calc_noise_propagation_flat_room(
                            factory.parameter, noise_emission, euclidean_distance, factory.restrictions[2].matrix[0, 0])
                        # - factory.parameter["noise_factory_wall_isolation"] * wall_count
                        # - factory.parameter["noise_barrier_isolation"] * barrier_count)
                    else:
                        noise_pressure_measuring_point = calc_noise_propagation_free_field(
                            factory.parameter, noise_emission, euclidean_distance)
                        # - factory.parameter["noise_factory_wall_isolation"] * wall_count
                        # - factory.parameter["noise_barrier_isolation"] * barrier_count)
                else:
                    noise_pressure_measuring_point = 0

                # check if <0
                if noise_pressure_measuring_point >= 0:
                    facility.noise_propagation_layout[y_index][x_index] = noise_pressure_measuring_point
                else:
                    facility.noise_propagation_layout[y_index][x_index] = 0

    facility.noise_source_position = noise_source_pos
