import copy

import numpy as np
from scipy.spatial import distance
from mefap.evaluation.pre_calculation import pre_calc_check_rotation
from mefap.evaluation.pre_calculation import pre_calc_centroid
from mefap.evaluation.noise_propagation.factory_wall_corner_cells import wall_and_corner_cells
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_layout
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_propagation_layout_extended
from mefap.evaluation.noise_propagation.calc_noise_propagation import add_up_noise
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_room_condition
from mefap.evaluation.noise_propagation.noise_propagation_wall import calc_noise_propagation_wall


def eval_illuminance(factory):
    """ eval illuminance"""
    illuminance_count = 0
    # check if facilities are enough illuminated
    for facility in range(0, len(factory.facility_list), 1):
        # check position of facility
        y_facility_width, x_facility_width = pre_calc_check_rotation(factory, facility)
        # count NOT illuminated cells of facilities
        for y_index in range(0, y_facility_width, 1):
            for x_index in range(0, x_facility_width, 1):
                if factory.illuminance_matrix[y_index + factory.facility_list[facility].curr_position[0],
                                              x_index + factory.facility_list[facility].curr_position[1]].value \
                        < factory.facility_characteristics[facility].illuminance.value:
                    illuminance_count += 1

    # check if paths are NOT enough illuminated
    for y_index in range(0, factory.layout.shape[0], 1):
        for x_index in range(0, factory.layout.shape[1], 1):
            if factory.layout[y_index, x_index] == factory.parameter["local_road_value"] \
                    or factory.layout[y_index, x_index] == factory.parameter["fixed_road_value"]:
                if factory.illuminance_matrix[y_index, x_index].value < factory.parameter["road_illuminance"]:
                    illuminance_count += 1

    # count empty cells
    empty_space_counter = 0
    for row in factory.layout:
        for cell in row:
            if cell == factory.parameter["empty_cell_value"] or cell == factory.parameter["hidden_cell_value"]:
                empty_space_counter += 1

    # calc rating
    illuminance_rating = 1 - (illuminance_count / (factory.layout.size - empty_space_counter))

    # write data
    factory.quamfab.illuminance[0] = illuminance_count
    factory.quamfab.illuminance[1] = illuminance_rating


def eval_noise(factory):
    """ eval noise """
    noise_pressure_layouts = []
    for facility in factory.facility_list:

        noise_source_pos = pre_calc_centroid(factory, facility.index_num)
        # if noise_source_pos haven't changed, don't recalc matrix (time saving argument)
        if not noise_source_pos == facility.noise_source_position:

            # get noise_emission
            noise_emission = factory.facility_characteristics[facility.index_num].noise_exposure

            # init noise_propagation_layout
            facility.noise_propagation_layout = np.full(shape=(factory.columns, factory.rows),
                                                        fill_value=noise_emission)

            # get NPC-Layout of source point
            npc_matrix = factory.noise_propagation_matrix.get_np_layout_by_cell(noise_source_pos)

            facility.noise_propagation_layout = facility.noise_propagation_layout + npc_matrix

            # test if <0
            for y_index in range(0, facility.noise_propagation_layout.shape[0], 1):
                for x_index in range(0, facility.noise_propagation_layout.shape[1], 1):
                    if facility.noise_propagation_layout[y_index, x_index] < 0:
                        facility.noise_propagation_layout[y_index, x_index] = 0
                    elif facility.noise_propagation_layout[y_index, x_index] == noise_emission and \
                            noise_source_pos != [y_index, x_index]:
                        facility.noise_propagation_layout[y_index, x_index] = 0

        noise_pressure_layouts.append(facility.noise_propagation_layout)

    # add up noise -> get resulting sound pressure level -> store in factory.noise_matrix
    add_up_noise(factory, noise_pressure_layouts)

    noise_pressure_layouts = [copy.copy(factory.noise_matrix)]

    noise_areas = factory.noise_area.get_all_noise_walls()
    for noise_area in noise_areas:
        noise_pressure_layouts.append(calc_noise_propagation_wall(factory, noise_area))

    add_up_noise(factory, noise_pressure_layouts)

    # compare with sensitivity
    noise_count = 0
    # check if facilities are enough illuminated
    for facility in range(0, len(factory.facility_list), 1):
        # check position of facility based on rotation
        y_facility_width, x_facility_width = pre_calc_check_rotation(factory, facility)

        for y_index in range(0, y_facility_width, 1):
            for x_index in range(0, x_facility_width, 1):
                if factory.noise_matrix[y_index + factory.facility_list[facility].curr_position[0],
                                        x_index + factory.facility_list[facility].curr_position[1]] \
                        > factory.facility_characteristics[facility].noise_sensitivity:
                    noise_count += (factory.noise_matrix[y_index + factory.facility_list[facility].curr_position[0],
                                                         x_index + factory.facility_list[facility].curr_position[1]] -
                                    factory.facility_characteristics[facility].noise_sensitivity)

    # count empty cells, paths and roads
    empty_space_counter = 0
    for row in factory.layout:
        for cell in row:
            if cell == factory.parameter["empty_cell_value"] or cell == factory.parameter["local_road_value"] \
                    or cell == factory.parameter["fixed_road_value"] or cell == factory.parameter["hidden_cell_value"]:
                empty_space_counter += 1

    # calc rating
    noise_rating = 1 - (noise_count / (factory.layout.size - empty_space_counter))

    # write data
    factory.quamfab.noise[0] = noise_count
    factory.quamfab.noise[1] = None  # noise_rating


def re_calc_noise_matrix(factory):
    """ eval noise """

    # calc noise_room_conditions if necessary
    if factory.parameter["noise_semi_diffuse"]:
        calc_noise_room_condition(factory)

    # check if list is not empty
    if factory.facility_list:

        # setup wall and corner cell lists
        factory.noise_wall_list, factory.noise_corner_list = wall_and_corner_cells(factory.layout, factory.noise_area)

        # init list
        noise_pressure_layouts = []

        for facility in factory.facility_list:
            # generate empty layout
            # noise_pressure_layouts.append(np.zeros(shape=(factory.columns, factory.rows)))
            # check if noise source
            if factory.facility_characteristics[facility.index_num].noise_exposure <= 0 or \
                    facility.curr_position == [None, None]:
                continue
            # calc center point / centre of gravity -> set noise source
            noise_source_pos = pre_calc_centroid(factory, facility.index_num)

            # calc noise layout and add to list (WITHOUT or WITH considering NoiseWalls)
            """
            calc_noise_propagation_layout(facility, factory, noise_source_pos)
            """
            calc_noise_propagation_layout_extended(facility, factory, noise_source_pos)
            #"""
            noise_pressure_layouts.append(facility.noise_propagation_layout)

        # add up noise -> get resulting sound pressure level -> store in factory.noise_matrix
        add_up_noise(factory, noise_pressure_layouts)

        noise_pressure_layouts = [copy.copy(factory.noise_matrix)]

        noise_areas = factory.noise_area.get_all_noise_walls()
        for noise_area in noise_areas:
            noise_pressure_layouts.append(calc_noise_propagation_wall(factory, noise_area))

        add_up_noise(factory, noise_pressure_layouts)


def eval_vibration(factory):
    """ eval vibration """
    vibration_distance = 0
    for facility_one in range(0, len(factory.facility_list), 1):
        centroid_one = pre_calc_centroid(factory, facility_one)
        for facility_two in range(0, len(factory.facility_list), 1):
            if facility_one != facility_two:
                centroid_two = pre_calc_centroid(factory, facility_two)
                euclidean_distance = distance.euclidean(centroid_one, centroid_two) * factory.cell_size
                vibration_factor = (factory.facility_characteristics[facility_one].vibration_sensitivity +
                                    factory.facility_characteristics[facility_two].vibration_excitation)
                # calc weight distance
                vibration_distance += (vibration_factor * euclidean_distance)

            else:
                continue
    # add vibration factor to distance
    vibration_distance = factory.parameter["vibration_factor"] * vibration_distance

    # write data
    factory.quamfab.vibration[0] = vibration_distance
    factory.quamfab.vibration[1] = None  # vibration_rating


def eval_cleanliness(factory):
    """ eval cleanliness """
    cleanliness_distance = 0
    for facility_one in range(0, len(factory.facility_list), 1):
        centroid_one = pre_calc_centroid(factory, facility_one)
        for facility_two in range(0, len(factory.facility_list), 1):
            if facility_one != facility_two:
                centroid_two = pre_calc_centroid(factory, facility_two)
                euclidean_distance = distance.euclidean(centroid_one, centroid_two) * factory.cell_size
                cleanliness_factor = abs(factory.facility_characteristics[facility_one].cleanliness -
                                         factory.facility_characteristics[facility_two].cleanliness)
                # calc weight distance
                cleanliness_distance += (cleanliness_factor * euclidean_distance)

            else:
                continue
    # add cleanliness factor to distance
    cleanliness_distance = factory.parameter["cleanliness_factor"] * cleanliness_distance

    # write data
    factory.quamfab.cleanliness[0] = cleanliness_distance
    factory.quamfab.cleanliness[1] = None  # cleanliness_rating


def eval_temperature(factory):
    """ eval temperature """
    temperature_distance = 0
    for facility_one in range(0, len(factory.facility_list), 1):
        centroid_one = pre_calc_centroid(factory, facility_one)
        for facility_two in range(0, len(factory.facility_list), 1):
            if facility_one != facility_two:
                centroid_two = pre_calc_centroid(factory, facility_two)
                euclidean_distance = distance.euclidean(centroid_one, centroid_two) * factory.cell_size
                temperature_factor = abs(factory.facility_characteristics[facility_one].temperature -
                                         factory.facility_characteristics[facility_two].temperature)
                # calc weight distance
                temperature_distance += (temperature_factor * euclidean_distance)

            else:
                continue
    # add temperature factor to distance
    temperature_distance = factory.parameter["temperature_factor"] * temperature_distance

    # write data
    factory.quamfab.temperature[0] = temperature_distance
    factory.quamfab.temperature[1] = None  # temperature_rating
