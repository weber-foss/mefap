import math


def pre_calc_centroid(factory, facility_index):
    """ calc centroid of facility"""

    # get rotation
    y_facility_width, x_facility_width = pre_calc_check_rotation(factory, facility_index)

    # calc centroid
    if float(factory.facility_list[facility_index].cell_width1 / 2).is_integer() \
            and float(factory.facility_list[facility_index].cell_width2 / 2).is_integer():
        centroid = [factory.facility_list[facility_index].curr_position[0] + math.floor(y_facility_width / 2),
                    factory.facility_list[facility_index].curr_position[1] + math.floor(x_facility_width / 2)]
    else:
        centroid = [factory.facility_list[facility_index].curr_position[0] + math.floor(y_facility_width / 2),
                    factory.facility_list[facility_index].curr_position[1] + math.floor(x_facility_width / 2)]

    return centroid


def pre_calc_check_rotation(factory, facility_index):
    """ get rotation """
    # check position of facility
    if factory.facility_list[facility_index].curr_rotation == 0 \
            or factory.facility_list[facility_index].curr_rotation == 2:
        y_facility_width = factory.facility_list[facility_index].cell_width2
        x_facility_width = factory.facility_list[facility_index].cell_width1
    elif factory.facility_list[facility_index].curr_rotation == 1 \
            or factory.facility_list[facility_index].curr_rotation == 3:
        y_facility_width = factory.facility_list[facility_index].cell_width1
        x_facility_width = factory.facility_list[facility_index].cell_width2
    else:
        print(factory.facility_list[facility_index].curr_position)
        print(factory.facility_list[facility_index].curr_rotation)
        print(factory.layout)

    return y_facility_width, x_facility_width
