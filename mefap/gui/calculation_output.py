from PySide2.QtCore import Slot
from PySide2.QtGui import QTextCursor
from PySide2.QtWidgets import QTextBrowser, QVBoxLayout, QDialog


class CalculationOutput(QDialog):
    """
    This QDialog will be opened during optimation calculation.
    Through this Dialog the user gets informations about the ongoing calculations.
    """

    def __init__(self, cores, parent=None):
        super().__init__(parent)
        self.resize(500, 200)
        self._console = QTextBrowser(self)
        self.active_process = cores

        layout = QVBoxLayout()
        layout.addWidget(self._console)
        self.setLayout(layout)

        self._console.cursorPositionChanged.connect(self.check_end)

    @Slot(str)
    def append_text(self, text):
        """
        If Signal from CalculationOutputReceiver is emitted, this method gets triggered and appends the text to
        the output console.
        :param text: Text for appending
        :return: None
        """
        self._console.moveCursor(QTextCursor.End)
        self._console.insertPlainText(text)
        if text == 'Done!':
            self.active_process -= 1
            if self.active_process == 0:
                self.accept()

    def check_end(self) -> None:
        """
        This method is checking for keywords in the console.
        If a keyword is found. The CalculationOutputDialog closes.
        :return: None
        """
        self._console.verticalScrollBar().setValue(self._console.verticalScrollBar().maximum())
