from PySide2.QtCore import Qt
from PySide2.QtWidgets import QGraphicsView, QFrame


class FacilityView(QGraphicsView):
    """
    This view is used for showing the Facilities.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setFrameShape(QFrame.NoFrame)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
