from PySide2.QtWidgets import QDialog, QTableWidgetItem, QHeaderView

from mefap.factory.evaluation_data import EvaluationData
from mefap.factory.weighting import Weighting
from mefap.gui.generated.EvaluationTableDialog import Ui_evaluation_table_dialog


class EvaluationDialog(QDialog):
    """
    This dialog shows the evaluation results in pairwaise comparision
    """

    def __init__(self, eval1: EvaluationData, eval2: EvaluationData, weights: Weighting) -> None:
        super().__init__()
        self.ui = Ui_evaluation_table_dialog()
        self.ui.setupUi(self)
        self.eval1 = eval1
        self.eval2 = eval2
        self.weights = weights
        self.load_evaluation_data()

    def load_evaluation_data(self) -> None:
        """
        This method is loading the elements of the EvaluationTable.
        :return: None
        """
        self.ui.table_eval.setRowCount(14 + 1)
        self.ui.table_eval.setColumnCount(6)
        header = self.ui.table_eval.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)

        self.ui.table_eval.setHorizontalHeaderLabels(
            [self.tr("Kriterien"), self.tr("Gewichtung[%]"), self.tr("Ergebniswerte"),
             self.tr("Bewertung"), self.tr("Bestwerte"), self.tr("Bewertung")])
        self.ui.table_eval.verticalHeader().hide()

        self.ui.table_eval.setItem(0, 0, QTableWidgetItem(self.tr('material_flow_length')))
        self.ui.table_eval.setItem(0, 1, QTableWidgetItem(str(self.weights.material_flow_length)))
        self.ui.table_eval.setItem(0, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.mf_distance[0], 0)))))
        self.ui.table_eval.setItem(0, 3, QTableWidgetItem(str(round(self.eval1.mf_distance[1], 2))))
        self.ui.table_eval.setItem(0, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.mf_distance[0], 0)))))
        self.ui.table_eval.setItem(0, 5, QTableWidgetItem(str(round(self.eval2.mf_distance[1], 2))))

        self.ui.table_eval.setItem(1, 0, QTableWidgetItem(self.tr('no_overlapping')))
        self.ui.table_eval.setItem(1, 1, QTableWidgetItem(str(self.weights.no_overlapping)))
        self.ui.table_eval.setItem(1, 2, QTableWidgetItem(
            str(int(round(self.eval1.mf_overlapping[0], 0)))))
        self.ui.table_eval.setItem(1, 3,
                                   QTableWidgetItem(str(round(self.eval1.mf_overlapping[1], 2))))
        self.ui.table_eval.setItem(1, 4, QTableWidgetItem(
            str(int(round(self.eval2.mf_overlapping[0], 0)))))
        self.ui.table_eval.setItem(1, 5,
                                   QTableWidgetItem(str(round(self.eval2.mf_overlapping[1], 2))))

        self.ui.table_eval.setItem(2, 0, QTableWidgetItem(self.tr('route_continuity')))
        self.ui.table_eval.setItem(2, 1, QTableWidgetItem(str(self.weights.route_continuity)))
        self.ui.table_eval.setItem(2, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.mf_constancy[0], 0)))))
        self.ui.table_eval.setItem(2, 3,
                                   QTableWidgetItem(str(round(self.eval1.mf_constancy[1], 2))))
        self.ui.table_eval.setItem(2, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.mf_constancy[0], 0)))))
        self.ui.table_eval.setItem(2, 5,
                                   QTableWidgetItem(str(round(self.eval2.mf_constancy[1], 2))))

        self.ui.table_eval.setItem(3, 0, QTableWidgetItem(self.tr('land_use_degree')))
        self.ui.table_eval.setItem(3, 1, QTableWidgetItem(str(self.weights.land_use_degree)))
        self.ui.table_eval.setItem(3, 2, QTableWidgetItem(
            str(int(round(self.eval1.area_utilization[0], 0)))))
        self.ui.table_eval.setItem(3, 3,
                                   QTableWidgetItem(str(round(self.eval1.area_utilization[1], 2))))
        self.ui.table_eval.setItem(3, 4, QTableWidgetItem(
            str(int(round(self.eval2.area_utilization[0], 0)))))
        self.ui.table_eval.setItem(3, 5,
                                   QTableWidgetItem(str(round(self.eval2.area_utilization[1], 2))))

        self.ui.table_eval.setItem(4, 0, QTableWidgetItem(self.tr('media_availability')))
        self.ui.table_eval.setItem(4, 1, QTableWidgetItem(str(self.weights.media_availability)))
        self.ui.table_eval.setItem(4, 2, QTableWidgetItem(
            str(int(round(self.eval1.media_availability[0], 0)))))
        self.ui.table_eval.setItem(4, 3, QTableWidgetItem(
            str(round(self.eval1.media_availability[1], 2))))
        self.ui.table_eval.setItem(4, 4, QTableWidgetItem(
            str(int(round(self.eval2.media_availability[0], 0)))))
        self.ui.table_eval.setItem(4, 5, QTableWidgetItem(
            str(round(self.eval2.media_availability[1], 2))))

        self.ui.table_eval.setItem(5, 0, QTableWidgetItem(self.tr('media_compatibility')))
        self.ui.table_eval.setItem(5, 1, QTableWidgetItem(str(self.weights.media_compatibility)))
        self.ui.table_eval.setItem(5, 2, QTableWidgetItem(
            str(int(round(self.eval1.media_compatibility[0], 0)))))
        self.ui.table_eval.setItem(5, 3, QTableWidgetItem(
            str(round(self.eval1.media_compatibility[1], 2))))
        self.ui.table_eval.setItem(5, 4, QTableWidgetItem(
            str(int(round(self.eval2.media_compatibility[0], 0)))))
        self.ui.table_eval.setItem(5, 5, QTableWidgetItem(
            str(round(self.eval2.media_compatibility[1], 2))))

        self.ui.table_eval.setItem(6, 0, QTableWidgetItem(self.tr('lighting')))
        self.ui.table_eval.setItem(6, 1, QTableWidgetItem(str(self.weights.lighting)))
        self.ui.table_eval.setItem(6, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.illuminance[0], 0)))))
        self.ui.table_eval.setItem(6, 3, QTableWidgetItem(str(round(self.eval1.illuminance[1], 2))))
        self.ui.table_eval.setItem(6, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.illuminance[0], 0)))))
        self.ui.table_eval.setItem(6, 5, QTableWidgetItem(str(round(self.eval2.illuminance[1], 2))))

        self.ui.table_eval.setItem(7, 0, QTableWidgetItem(self.tr('quiet')))
        self.ui.table_eval.setItem(7, 1, QTableWidgetItem(str(self.weights.quiet)))
        self.ui.table_eval.setItem(7, 2, QTableWidgetItem(str(int(round(self.eval1.noise[0], 0)))))
        self.ui.table_eval.setItem(7, 3, QTableWidgetItem(str(round(self.eval1.noise[1], 2))))
        self.ui.table_eval.setItem(7, 4, QTableWidgetItem(str(int(round(self.eval2.noise[0], 0)))))
        self.ui.table_eval.setItem(7, 5, QTableWidgetItem(str(round(self.eval2.noise[1], 2))))

        self.ui.table_eval.setItem(8, 0, QTableWidgetItem(self.tr('vibration')))
        self.ui.table_eval.setItem(8, 1, QTableWidgetItem(str(self.weights.vibration)))
        self.ui.table_eval.setItem(8, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.vibration[0], 0)))))
        self.ui.table_eval.setItem(8, 3, QTableWidgetItem(str(round(self.eval1.vibration[1], 2))))
        self.ui.table_eval.setItem(8, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.vibration[0], 0)))))
        self.ui.table_eval.setItem(8, 5, QTableWidgetItem(str(round(self.eval2.vibration[1], 2))))

        self.ui.table_eval.setItem(9, 0, QTableWidgetItem(self.tr('temperature')))
        self.ui.table_eval.setItem(9, 1, QTableWidgetItem(str(self.weights.temperature)))
        self.ui.table_eval.setItem(9, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.temperature[0], 0)))))
        self.ui.table_eval.setItem(9, 3, QTableWidgetItem(str(round(self.eval1.temperature[1], 2))))
        self.ui.table_eval.setItem(9, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.temperature[0], 0)))))
        self.ui.table_eval.setItem(9, 5, QTableWidgetItem(str(round(self.eval2.temperature[1], 2))))

        self.ui.table_eval.setItem(10, 0, QTableWidgetItem(self.tr('cleanliness')))
        self.ui.table_eval.setItem(10, 1, QTableWidgetItem(str(self.weights.cleanliness)))
        self.ui.table_eval.setItem(10, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.cleanliness[0], 0)))))
        self.ui.table_eval.setItem(10, 3,
                                   QTableWidgetItem(str(round(self.eval1.cleanliness[1], 2))))
        self.ui.table_eval.setItem(10, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.cleanliness[0], 0)))))
        self.ui.table_eval.setItem(10, 5,
                                   QTableWidgetItem(str(round(self.eval2.cleanliness[1], 2))))

        self.ui.table_eval.setItem(11, 0, QTableWidgetItem(self.tr('direct_communication')))
        self.ui.table_eval.setItem(11, 1, QTableWidgetItem(str(self.weights.direct_communication)))
        self.ui.table_eval.setItem(11, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.direct_com[0], 0)))))
        self.ui.table_eval.setItem(11, 3, QTableWidgetItem(str(round(self.eval1.direct_com[1], 2))))
        self.ui.table_eval.setItem(11, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.direct_com[0], 0)))))
        self.ui.table_eval.setItem(11, 5, QTableWidgetItem(str(round(self.eval2.direct_com[1], 2))))

        self.ui.table_eval.setItem(12, 0, QTableWidgetItem(self.tr('formal_communication')))
        self.ui.table_eval.setItem(12, 1, QTableWidgetItem(str(self.weights.formal_communication)))
        self.ui.table_eval.setItem(12, 2,
                                   QTableWidgetItem(str(int(round(self.eval1.formal_com[0], 0)))))
        self.ui.table_eval.setItem(12, 3, QTableWidgetItem(str(round(self.eval1.formal_com[1], 2))))
        self.ui.table_eval.setItem(12, 4,
                                   QTableWidgetItem(str(int(round(self.eval2.formal_com[0], 0)))))
        self.ui.table_eval.setItem(12, 5, QTableWidgetItem(str(round(self.eval2.formal_com[1], 2))))

        self.ui.table_eval.setItem(13, 0, QTableWidgetItem(self.tr("Weighted Sum")))
        self.ui.table_eval.setItem(13, 2, QTableWidgetItem(str(round(self.eval1.weight_sum, 3))))
        if self.eval2.weight_sum:
            self.ui.table_eval.setItem(13, 4,
                                       QTableWidgetItem(str(round(self.eval2.weight_sum, 3))))
