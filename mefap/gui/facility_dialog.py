import logging
from typing import List

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog, QMessageBox

from mefap.factory.facility_attributs import FacilityAttributes
from mefap.gui.facility_scene import FacilityScene
from mefap.gui.facility_view import FacilityView
from mefap.gui.generated.FacilityDialog import Ui_facility_dialog


class FacilityDialog(QDialog):
    """
    This Dialog is called to allocate sink and source of all facilities of the FactoryModel.
    Each facility will be opened as new scene. With the right and left button the actual facility
    can be changed.
    """

    def __init__(self, facilities: List[FacilityAttributes], cell_size: int, cell_zoom: int) -> None:
        super().__init__()

        self.ui = Ui_facility_dialog()
        self.ui.setupUi(self)
        self.facility_list = facilities
        self.index = 0
        self.cell_size = cell_size
        self.cell_zoom = cell_zoom

        self.setWindowTitle(self.tr("Allokation Quelle und Senke der Facilites"))

        self.widest = 0
        self.highest = 0
        for facility in self.facility_list:
            if facility.cell_width1 > self.widest:
                self.widest = facility.cell_width1
            if facility.cell_width2 > self.highest:
                self.highest = facility.cell_width2

        # initialise facility visualisation
        self.graphicsView_factory = FacilityView(self)
        self.ui.horizontal_layout_1.addWidget(self.graphicsView_factory)

        self.graphicsView_factory.setToolTip(self.tr("With the left mouse button you can assign either a source, a sink or both at the same time to the unit cells."))

        self.load_layout()

        self.ui.push_button_previous.setText("Close")
        self.ui.push_button_next.clicked.connect(self.next_facility)
        self.ui.push_button_previous.clicked.connect(self.previous_facility)

    def fit_view_after_layout_loading(self) -> None:
        """
        Fits content of facility in given space in this dialog.
        :return: None
        """
        self.graphicsView_factory.fitInView(0, 0, self.widest * self.cell_zoom,
                                            self.highest * self.cell_zoom,
                                            Qt.KeepAspectRatio)

    def load_layout(self) -> None:
        """
        This method is loading the scene of the next facility depending on the actual index.
        If the scene is the last one. Right button changes to "Save".
        If the scene is the first one, the user can leave this dialog with the left button.
        :return: None
        """
        logging.info(
            "Allokiere Quelle und Senke für {}.".format(self.facility_list[self.index].name))
        self.factory_scene = FacilityScene(self.facility_list[self.index], self.cell_size,
                                           self.cell_zoom,
                                           parent=self.graphicsView_factory)
        self.ui.label_actual_facility.setText(self.facility_list[self.index].name)
        self.graphicsView_factory.setScene(self.factory_scene)
        self.factory_scene.init_facility_grid()

        self.graphicsView_factory.fitInView(0, 0, self.widest * self.cell_zoom,
                                            self.highest * self.cell_zoom,
                                            Qt.KeepAspectRatio)

    def next_facility(self) -> None:
        """
        When the user clicks the "right" button the next facility scene will load.
        :return: None
        """
        if self.index == len(self.facility_list) - 2:
            self.ui.push_button_next.setText(self.tr("Save"))
        elif self.index == len(self.facility_list) - 1:
            if self.are_all_sink_source_set():
                self.calculate_sequence_factor_and_edge()
                self.accept()
                return
            else:
                msg_box = QMessageBox()
                msg_box.setText(self.tr("Nicht alle Quelle und Senken Positionen wurden definiert"))
                msg_box.setInformativeText(self.tr(
                    "Vor der weiteren Bearbeitung muessen alle Quellen und Senken definiert werden."))
                msg_box.setStandardButtons(QMessageBox.Ok)
                msg_box.setDefaultButton(QMessageBox.Ok)
                msg_box.setInformativeText(QMessageBox.Critical)
                msg_box.exec_()
        else:
            self.ui.push_button_next.setText(">")
        if self.index == 0:
            self.ui.push_button_previous.setText("<")
        if self.is_actual_sink_source_set():
            self.index += 1
            self.load_layout()
        else:
            msg_box = QMessageBox()
            msg_box.setText(self.tr("Quelle und Senken Position wurden noch nicht definiert"))
            msg_box.setInformativeText(self.tr(
                "Vor der weiteren Bearbeitung muessen Quelle und Senke definiert werden."))
            msg_box.setStandardButtons(QMessageBox.Ok)
            msg_box.setDefaultButton(QMessageBox.Ok)
            msg_box.setIcon(QMessageBox.Critical)
            msg_box.exec_()

    def previous_facility(self) -> None:
        """
        When the user clicks the "left" button the previous facility scene will load.
        :return: None
        """
        if self.index == 1:
            self.ui.push_button_previous.setText("Close")
        elif self.index == 0:
            msg_box = QMessageBox()
            msg_box.setText(self.tr("Allokation der Quelle und Senke wird verlassen"))
            msg_box.setInformativeText(self.tr(
                "Vor der weiteren Bearbeitung muessen alle Quellen und Senken definiert werden."
                "\nAlle Aenderungen gehen verloren!"))
            msg_box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
            msg_box.setDefaultButton(QMessageBox.Cancel)
            ret = msg_box.exec_()
            if ret == QMessageBox.Ok:
                self.reject()
            return
        else:
            self.ui.push_button_previous.setText("<")
        if self.index == len(self.facility_list) - 1:
            self.ui.push_button_next.setText(">")
        self.index -= 1
        self.load_layout()

    def are_all_sink_source_set(self) -> bool:
        """
        This method is checking if all sink and source positions are set.
        :return: True if sink and source for all facilities set.
        """
        for facility in self.facility_list:
            if (not facility.sink_position) or (not facility.source_position):
                return False
        return True

    def is_actual_sink_source_set(self) -> bool:
        """
        This method is checking if sink and source positions for the actual facility are set.
        :return: True if sink and source are set
        """
        if self.facility_list[self.index].source_position \
                and self.facility_list[self.index].sink_position:
            return True
        return False

    def calculate_sequence_factor_and_edge(self) -> None:
        """TODO"""
        for facility in self.facility_list:
            # TODO: temporary solution ... wrap in method somewhere
            for iteration in range(0, 2, 1):
                if iteration == 0:
                    help_position = facility.source_position
                    on_edge = False
                elif iteration == 1:
                    help_position = facility.sink_position
                    on_edge = False
                # calculate current sourcePos or sinkPos
                if facility.cell_width2 - 1 > help_position[0] >= 0 and help_position[1] == 0:
                    origin_sequence_factor = 0
                    if help_position[0] == 0 and help_position[1] == 0:
                        on_edge = True

                elif help_position[0] == 0 and help_position[1] > 0:
                    origin_sequence_factor = 1
                    if help_position[0] == 0 and help_position[1] == facility.cell_width1 - 1:
                        on_edge = True

                elif help_position[0] > 0 and help_position[1] == facility.cell_width1 - 1:
                    origin_sequence_factor = 2
                    if help_position[0] == facility.cell_width2 - 1 and help_position[
                        1] == facility.cell_width1 - 1:
                        on_edge = True

                # ToDo: Doppeldefinition von (Y,X) prüfen
                elif help_position[0] == facility.cell_width2 - 1 and facility.cell_width1 - 1 > \
                        help_position[1] >= 0:
                    origin_sequence_factor = 3
                    if help_position[0] == facility.cell_width2 - 1 and help_position[1] == 0:
                        on_edge = True

                else:
                    # facility has only one cell
                    origin_sequence_factor = 0
                    on_edge = True

                # write data
                if iteration == 0:
                    facility.source_sequence_factor = origin_sequence_factor
                    facility.source_sequence_edge = on_edge
                elif iteration == 1:
                    facility.sink_sequence_factor = origin_sequence_factor
                    facility.sink_sequence_edge = on_edge
