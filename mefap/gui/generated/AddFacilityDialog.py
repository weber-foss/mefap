# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'FactoryFacilityDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_AddFacilityDialog(object):
    def setupUi(self, AddFacilityDialog):
        if not AddFacilityDialog.objectName():
            AddFacilityDialog.setObjectName(u"AddFacilityDialog")
        AddFacilityDialog.resize(523, 854)
        self.verticalLayout = QVBoxLayout(AddFacilityDialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(AddFacilityDialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.label_14 = QLabel(AddFacilityDialog)
        self.label_14.setObjectName(u"label_14")

        self.verticalLayout.addWidget(self.label_14)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_2 = QLabel(AddFacilityDialog)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.label_3 = QLabel(AddFacilityDialog)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.comboBox_type = QComboBox(AddFacilityDialog)
        self.comboBox_type.setObjectName(u"comboBox_type")

        self.gridLayout.addWidget(self.comboBox_type, 1, 1, 1, 1)

        self.label_7 = QLabel(AddFacilityDialog)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout.addWidget(self.label_7, 2, 0, 1, 1)

        self.spinBox_department = QSpinBox(AddFacilityDialog)
        self.spinBox_department.setObjectName(u"spinBox_department")

        self.gridLayout.addWidget(self.spinBox_department, 2, 1, 1, 1)

        self.lineEdit_name = QLineEdit(AddFacilityDialog)
        self.lineEdit_name.setObjectName(u"lineEdit_name")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_name.sizePolicy().hasHeightForWidth())
        self.lineEdit_name.setSizePolicy(sizePolicy)
        self.lineEdit_name.setFrame(True)
        self.lineEdit_name.setClearButtonEnabled(False)

        self.gridLayout.addWidget(self.lineEdit_name, 0, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout)

        self.label_13 = QLabel(AddFacilityDialog)
        self.label_13.setObjectName(u"label_13")

        self.verticalLayout.addWidget(self.label_13)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.gridLayout_2.setContentsMargins(-1, -1, -1, 0)
        self.doubleSpinBox_width = QDoubleSpinBox(AddFacilityDialog)
        self.doubleSpinBox_width.setObjectName(u"doubleSpinBox_width")
        self.doubleSpinBox_width.setMaximum(1000.000000000000000)
        self.doubleSpinBox_width.setValue(1.000000000000000)

        self.gridLayout_2.addWidget(self.doubleSpinBox_width, 0, 1, 1, 1)

        self.doubleSpinBox_height = QDoubleSpinBox(AddFacilityDialog)
        self.doubleSpinBox_height.setObjectName(u"doubleSpinBox_height")
        self.doubleSpinBox_height.setMaximum(50.000000000000000)
        self.doubleSpinBox_height.setValue(1.000000000000000)

        self.gridLayout_2.addWidget(self.doubleSpinBox_height, 2, 1, 1, 1)

        self.label_8 = QLabel(AddFacilityDialog)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_2.addWidget(self.label_8, 2, 0, 1, 1)

        self.doubleSpinBox_lenght = QDoubleSpinBox(AddFacilityDialog)
        self.doubleSpinBox_lenght.setObjectName(u"doubleSpinBox_lenght")
        self.doubleSpinBox_lenght.setMaximum(1000.000000000000000)
        self.doubleSpinBox_lenght.setValue(1.000000000000000)

        self.gridLayout_2.addWidget(self.doubleSpinBox_lenght, 1, 1, 1, 1)

        self.label_5 = QLabel(AddFacilityDialog)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout_2.addWidget(self.label_5, 0, 0, 1, 1)

        self.label_6 = QLabel(AddFacilityDialog)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_2.addWidget(self.label_6, 1, 0, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_2)

        self.label_15 = QLabel(AddFacilityDialog)
        self.label_15.setObjectName(u"label_15")

        self.verticalLayout.addWidget(self.label_15)

        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(-1, -1, -1, 10)
        self.label_10 = QLabel(AddFacilityDialog)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_3.addWidget(self.label_10, 0, 0, 1, 1)

        self.doubleSpinBox_ceilingload = QDoubleSpinBox(AddFacilityDialog)
        self.doubleSpinBox_ceilingload.setObjectName(u"doubleSpinBox_ceilingload")
        self.doubleSpinBox_ceilingload.setMaximum(10000.000000000000000)

        self.gridLayout_3.addWidget(self.doubleSpinBox_ceilingload, 0, 1, 1, 1)

        self.label_9 = QLabel(AddFacilityDialog)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_3.addWidget(self.label_9, 1, 0, 1, 1)

        self.doubleSpinBox_floorload = QDoubleSpinBox(AddFacilityDialog)
        self.doubleSpinBox_floorload.setObjectName(u"doubleSpinBox_floorload")
        self.doubleSpinBox_floorload.setMaximum(10000.000000000000000)

        self.gridLayout_3.addWidget(self.doubleSpinBox_floorload, 1, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_3)

        self.label_16 = QLabel(AddFacilityDialog)
        self.label_16.setObjectName(u"label_16")

        self.verticalLayout.addWidget(self.label_16)

        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_4.setContentsMargins(-1, -1, -1, 0)
        self.label_11 = QLabel(AddFacilityDialog)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout_4.addWidget(self.label_11, 0, 0, 1, 1)

        self.doubleSpinBox_quiet = QDoubleSpinBox(AddFacilityDialog)
        self.doubleSpinBox_quiet.setObjectName(u"doubleSpinBox_quiet")
        self.doubleSpinBox_quiet.setMaximum(150.000000000000000)

        self.gridLayout_4.addWidget(self.doubleSpinBox_quiet, 0, 1, 1, 1)

        self.label_12 = QLabel(AddFacilityDialog)
        self.label_12.setObjectName(u"label_12")

        self.gridLayout_4.addWidget(self.label_12, 1, 0, 1, 1)

        self.doubleSpinBox_noise = QDoubleSpinBox(AddFacilityDialog)
        self.doubleSpinBox_noise.setObjectName(u"doubleSpinBox_noise")
        self.doubleSpinBox_noise.setMaximum(150.000000000000000)

        self.gridLayout_4.addWidget(self.doubleSpinBox_noise, 1, 1, 1, 1)

        self.label_17 = QLabel(AddFacilityDialog)
        self.label_17.setObjectName(u"label_17")

        self.gridLayout_4.addWidget(self.label_17, 2, 0, 1, 1)

        self.comboBox_light = QComboBox(AddFacilityDialog)
        self.comboBox_light.addItem("")
        self.comboBox_light.addItem("")
        self.comboBox_light.addItem("")
        self.comboBox_light.addItem("")
        self.comboBox_light.addItem("")
        self.comboBox_light.addItem("")
        self.comboBox_light.addItem("")
        self.comboBox_light.setObjectName(u"comboBox_light")

        self.gridLayout_4.addWidget(self.comboBox_light, 2, 1, 1, 1)

        self.label_18 = QLabel(AddFacilityDialog)
        self.label_18.setObjectName(u"label_18")

        self.gridLayout_4.addWidget(self.label_18, 3, 0, 1, 1)

        self.comboBox_vibration_excitation = QComboBox(AddFacilityDialog)
        self.comboBox_vibration_excitation.addItem("")
        self.comboBox_vibration_excitation.addItem("")
        self.comboBox_vibration_excitation.addItem("")
        self.comboBox_vibration_excitation.setObjectName(u"comboBox_vibration_excitation")

        self.gridLayout_4.addWidget(self.comboBox_vibration_excitation, 3, 1, 1, 1)

        self.label_21 = QLabel(AddFacilityDialog)
        self.label_21.setObjectName(u"label_21")

        self.gridLayout_4.addWidget(self.label_21, 4, 0, 1, 1)

        self.comboBox_vibration_sensitivity = QComboBox(AddFacilityDialog)
        self.comboBox_vibration_sensitivity.addItem("")
        self.comboBox_vibration_sensitivity.addItem("")
        self.comboBox_vibration_sensitivity.addItem("")
        self.comboBox_vibration_sensitivity.setObjectName(u"comboBox_vibration_sensitivity")

        self.gridLayout_4.addWidget(self.comboBox_vibration_sensitivity, 4, 1, 1, 1)

        self.label_22 = QLabel(AddFacilityDialog)
        self.label_22.setObjectName(u"label_22")

        self.gridLayout_4.addWidget(self.label_22, 5, 0, 1, 1)

        self.comboBox_temperatur = QComboBox(AddFacilityDialog)
        self.comboBox_temperatur.addItem("")
        self.comboBox_temperatur.addItem("")
        self.comboBox_temperatur.addItem("")
        self.comboBox_temperatur.setObjectName(u"comboBox_temperatur")

        self.gridLayout_4.addWidget(self.comboBox_temperatur, 5, 1, 1, 1)

        self.label_23 = QLabel(AddFacilityDialog)
        self.label_23.setObjectName(u"label_23")

        self.gridLayout_4.addWidget(self.label_23, 6, 0, 1, 1)

        self.comboBox_cleanliness = QComboBox(AddFacilityDialog)
        self.comboBox_cleanliness.addItem("")
        self.comboBox_cleanliness.addItem("")
        self.comboBox_cleanliness.addItem("")
        self.comboBox_cleanliness.setObjectName(u"comboBox_cleanliness")

        self.gridLayout_4.addWidget(self.comboBox_cleanliness, 6, 1, 1, 1)

        self.label_24 = QLabel(AddFacilityDialog)
        self.label_24.setObjectName(u"label_24")

        self.gridLayout_4.addWidget(self.label_24, 7, 0, 1, 1)

        self.comboBox_mobility = QComboBox(AddFacilityDialog)
        self.comboBox_mobility.addItem("")
        self.comboBox_mobility.addItem("")
        self.comboBox_mobility.addItem("")
        self.comboBox_mobility.setObjectName(u"comboBox_mobility")

        self.gridLayout_4.addWidget(self.comboBox_mobility, 7, 1, 1, 1)

        self.label_25 = QLabel(AddFacilityDialog)
        self.label_25.setObjectName(u"label_25")

        self.gridLayout_4.addWidget(self.label_25, 8, 0, 1, 1)

        self.spinBox_employee = QSpinBox(AddFacilityDialog)
        self.spinBox_employee.setObjectName(u"spinBox_employee")
        self.spinBox_employee.setMaximum(5000)

        self.gridLayout_4.addWidget(self.spinBox_employee, 8, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_4)

        self.label_19 = QLabel(AddFacilityDialog)
        self.label_19.setObjectName(u"label_19")

        self.verticalLayout.addWidget(self.label_19)

        self.gridLayout_5 = QGridLayout()
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout_5.setContentsMargins(-1, -1, -1, 0)
        self.label_20 = QLabel(AddFacilityDialog)
        self.label_20.setObjectName(u"label_20")

        self.gridLayout_5.addWidget(self.label_20, 0, 0, 1, 1)

        self.checkBox_wall = QCheckBox(AddFacilityDialog)
        self.checkBox_wall.setObjectName(u"checkBox_wall")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.checkBox_wall.sizePolicy().hasHeightForWidth())
        self.checkBox_wall.setSizePolicy(sizePolicy1)

        self.gridLayout_5.addWidget(self.checkBox_wall, 0, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_5)

        self.buttonBox = QDialogButtonBox(AddFacilityDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Save)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(AddFacilityDialog)
        self.buttonBox.accepted.connect(AddFacilityDialog.accept)
        self.buttonBox.rejected.connect(AddFacilityDialog.reject)

        QMetaObject.connectSlotsByName(AddFacilityDialog)
    # setupUi

    def retranslateUi(self, AddFacilityDialog):
        AddFacilityDialog.setWindowTitle(QCoreApplication.translate("AddFacilityDialog", u"MeFaP - Add Facility", None))
        self.label.setText(QCoreApplication.translate("AddFacilityDialog", u"<html><head/><body><p align=\"center\"><span style=\"\n"
"       font-size:24pt; font-weight:600;\">Add Facility</span></p></body></html>\n"
"      ", None))
        self.label_14.setText(QCoreApplication.translate("AddFacilityDialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Allgemein</span></p></body></html>", None))
        self.label_2.setText(QCoreApplication.translate("AddFacilityDialog", u"Bereichsname", None))
        self.label_3.setText(QCoreApplication.translate("AddFacilityDialog", u"Flaechenart", None))
        self.label_7.setText(QCoreApplication.translate("AddFacilityDialog", u"Abteilung", None))
        self.lineEdit_name.setPlaceholderText(QCoreApplication.translate("AddFacilityDialog", u"Name", None))
        self.label_13.setText(QCoreApplication.translate("AddFacilityDialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Abmessungen</span></p></body></html>", None))
        self.label_8.setText(QCoreApplication.translate("AddFacilityDialog", u"Hoehe [m]", None))
        self.label_5.setText(QCoreApplication.translate("AddFacilityDialog", u"min. Breite [m]", None))
        self.label_6.setText(QCoreApplication.translate("AddFacilityDialog", u"min. Laenge [m]", None))
        self.label_15.setText(QCoreApplication.translate("AddFacilityDialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Traglasten</span></p></body></html>", None))
        self.label_10.setText(QCoreApplication.translate("AddFacilityDialog", u"Erforderliche Deckentraglast [kg/m2]", None))
        self.label_9.setText(QCoreApplication.translate("AddFacilityDialog", u"Gewicht / erf. Bodentraglast [kg/m2]", None))
        self.label_16.setText(QCoreApplication.translate("AddFacilityDialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Eigenschaften</span></p></body></html>", None))
        self.label_11.setText(QCoreApplication.translate("AddFacilityDialog", u"Ruhebedarf [dB]", None))
        self.label_12.setText(QCoreApplication.translate("AddFacilityDialog", u"Laermexposition [dB]", None))
        self.label_17.setText(QCoreApplication.translate("AddFacilityDialog", u"Lichtbedarf [lx]", None))
        self.comboBox_light.setItemText(0, QCoreApplication.translate("AddFacilityDialog", u"< 100", None))
        self.comboBox_light.setItemText(1, QCoreApplication.translate("AddFacilityDialog", u"100 - 200", None))
        self.comboBox_light.setItemText(2, QCoreApplication.translate("AddFacilityDialog", u"200 - 300", None))
        self.comboBox_light.setItemText(3, QCoreApplication.translate("AddFacilityDialog", u"300 - 500", None))
        self.comboBox_light.setItemText(4, QCoreApplication.translate("AddFacilityDialog", u"500 - 750", None))
        self.comboBox_light.setItemText(5, QCoreApplication.translate("AddFacilityDialog", u"750 - 1000", None))
        self.comboBox_light.setItemText(6, QCoreApplication.translate("AddFacilityDialog", u"1000 - 1500", None))

        self.label_18.setText(QCoreApplication.translate("AddFacilityDialog", u"Erschuetterungsanregung", None))
        self.comboBox_vibration_excitation.setItemText(0, QCoreApplication.translate("AddFacilityDialog", u"Niedrig", None))
        self.comboBox_vibration_excitation.setItemText(1, QCoreApplication.translate("AddFacilityDialog", u"Mittel", None))
        self.comboBox_vibration_excitation.setItemText(2, QCoreApplication.translate("AddFacilityDialog", u"Hoch", None))

        self.label_21.setText(QCoreApplication.translate("AddFacilityDialog", u"Erschuetterungssensitivitaet", None))
        self.comboBox_vibration_sensitivity.setItemText(0, QCoreApplication.translate("AddFacilityDialog", u"Niedrig", None))
        self.comboBox_vibration_sensitivity.setItemText(1, QCoreApplication.translate("AddFacilityDialog", u"Mittel", None))
        self.comboBox_vibration_sensitivity.setItemText(2, QCoreApplication.translate("AddFacilityDialog", u"Hoch", None))

        self.label_22.setText(QCoreApplication.translate("AddFacilityDialog", u"Temperatur", None))
        self.comboBox_temperatur.setItemText(0, QCoreApplication.translate("AddFacilityDialog", u"Hitzeemfindlich", None))
        self.comboBox_temperatur.setItemText(1, QCoreApplication.translate("AddFacilityDialog", u"Neutral", None))
        self.comboBox_temperatur.setItemText(2, QCoreApplication.translate("AddFacilityDialog", u"Hitzeemittierend", None))

        self.label_23.setText(QCoreApplication.translate("AddFacilityDialog", u"Sauberkeit", None))
        self.comboBox_cleanliness.setItemText(0, QCoreApplication.translate("AddFacilityDialog", u"Schmutzemfindlich", None))
        self.comboBox_cleanliness.setItemText(1, QCoreApplication.translate("AddFacilityDialog", u"Neutral", None))
        self.comboBox_cleanliness.setItemText(2, QCoreApplication.translate("AddFacilityDialog", u"Schmutzemittierend", None))

        self.label_24.setText(QCoreApplication.translate("AddFacilityDialog", u"Mobilitaet", None))
        self.comboBox_mobility.setItemText(0, QCoreApplication.translate("AddFacilityDialog", u"Niedrig", None))
        self.comboBox_mobility.setItemText(1, QCoreApplication.translate("AddFacilityDialog", u"Mittel", None))
        self.comboBox_mobility.setItemText(2, QCoreApplication.translate("AddFacilityDialog", u"Hoch", None))

        self.label_25.setText(QCoreApplication.translate("AddFacilityDialog", u"Mitarbeiteranzahl", None))
        self.label_19.setText(QCoreApplication.translate("AddFacilityDialog", u"<html><head/><body><p><span style=\" font-weight:600;\">Weitere\n"
"       Restriktionen</span></p></body></html>\n"
"      ", None))
        self.label_20.setText(QCoreApplication.translate("AddFacilityDialog", u"Aussenwandpositioinierung", None))
        self.checkBox_wall.setText("")
    # retranslateUi

