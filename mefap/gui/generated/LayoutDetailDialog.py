# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'LayoutDetailDialog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_LayoutDetailDialog(object):
    def setupUi(self, LayoutDetailDialog):
        if not LayoutDetailDialog.objectName():
            LayoutDetailDialog.setObjectName(u"LayoutDetailDialog")
        LayoutDetailDialog.resize(685, 733)

        self.retranslateUi(LayoutDetailDialog)

        QMetaObject.connectSlotsByName(LayoutDetailDialog)
    # setupUi

    def retranslateUi(self, LayoutDetailDialog):
        LayoutDetailDialog.setWindowTitle(QCoreApplication.translate("LayoutDetailDialog", u"Layout Details", None))
    # retranslateUi

