from typing import List

from PySide2.QtCore import Qt
from PySide2.QtGui import QBrush
from PySide2.QtWidgets import QGraphicsRectItem, QGraphicsTextItem
from numpy.linalg import norm

from mefap.factory.facility_attributs import FacilityAttributes
from mefap.gui.factory_cell import FactoryCell
from mefap.gui.util import colors


class CompareFacility(QGraphicsRectItem):
    """
    The FactoryFacility class is the representation of a Facility of the factory.
    It will be placed on the grid inside the FactoryLayoutScene.
    """

    def __init__(self, facility: FacilityAttributes, cell_size, cell_zoom, *args, user=False,
                 **kwargs) -> None:
        QGraphicsRectItem.__init__(self, *args, **kwargs)
        self.attributes: FacilityAttributes = facility
        if user or self.attributes.fixed_position:
            self.setBrush(QBrush(facility.color, bs=Qt.Dense5Pattern))
            self.attributes.fixed_position = True
        else:
            self.setBrush(QBrush(facility.color))
            self.attributes.fixed_position = False

        self.sink_source: List[QGraphicsTextItem] = []

        self.name = QGraphicsTextItem(facility.name, self)
        self.name.setDefaultTextColor(colors.BLACK)
        self.name.setPos(self.boundingRect().topLeft())
        self.name.adjustSize()
        scale = self.name.scale()
        if self.name.boundingRect().width() > self.boundingRect().width():
            scale = min(scale, self.boundingRect().width() / self.name.boundingRect().width())
        if self.name.boundingRect().height() > self.boundingRect().height():
            scale = min(scale, self.boundingRect().height() / self.name.boundingRect().height())
        self.name.setScale(scale)
        self.name.setTextInteractionFlags(Qt.NoTextInteraction)
        if not facility.curr_mirror:
            self.attributes.curr_mirror = False
        if not facility.curr_rotation:
            self.attributes.curr_rotation = 0
        self.snap_to_grid()

    def rotate_after_loading(self) -> None:
        """
        This method is rotating the facility to the right position depending on the current_rot.
        +1 is the border thickness of the cells. facility.
        :return: None
        """
        if self.attributes.cell_width2 == self.attributes.cell_width1:
            self.setTransformOriginPoint(self.boundingRect().center())
            self.setRotation(self.attributes.curr_rotation * 90)
        elif self.attributes.curr_rotation == 1:
            self.setTransformOriginPoint(self.boundingRect().topLeft())
            self.setRotation(90)
            self.moveBy(self.attributes.cell_width2 *
                        self.scene().factory.cell_zoom,
                        0)
        elif self.attributes.curr_rotation == 2:
            self.setTransformOriginPoint(self.boundingRect().topLeft())
            self.setRotation(180)
            self.moveBy(
                self.attributes.cell_width1 * self.scene().factory.cell_zoom - 1,
                self.attributes.cell_width2 * self.scene().factory.cell_zoom - 1)
        elif self.attributes.curr_rotation == 3:
            self.setTransformOriginPoint(self.boundingRect().topLeft())
            self.setRotation(270)
            self.moveBy(0,
                        self.attributes.cell_width1 *
                        self.scene().factory.cell_zoom)

    def snap_to_grid(self) -> None:
        """
        Snaps this graphic object to the grid.
        :return:
        """
        self_scene_pos = self.top_left_scene_pos()
        items = self.collidingItems()
        items = [x for x in items if isinstance(x, FactoryCell)]
        if items:
            item_scene_pos = items[0].mapToScene(items[0].boundingRect().topLeft())
            nearest = (
                items[0], norm([self_scene_pos.x() - item_scene_pos.x(),
                                self_scene_pos.y() - item_scene_pos.y()]))
            for item in items:
                item_scene_pos = item.mapToScene(item.boundingRect().topLeft())
                distance = norm([self_scene_pos.x() - item_scene_pos.x(),
                                 self_scene_pos.y() - item_scene_pos.y()])
                if distance < nearest[1]:
                    nearest = (item, distance)

            item_scene_pos = nearest[0].mapToScene(nearest[0].boundingRect().topLeft())
            self.moveBy(item_scene_pos.x() - self_scene_pos.x() + 1,
                        item_scene_pos.y() - self_scene_pos.y() + 1)

    def top_left_scene_pos(self):
        """
        Returns the top left position independent from the current rotation.
        :return: top left position of graphic object.
        """
        if self.rotation() == 0:
            self_scene_pos = self.mapToScene(self.boundingRect().topLeft())
        elif self.rotation() == 90:
            self_scene_pos = self.mapToScene(self.boundingRect().bottomLeft())
        elif self.rotation() == 180:
            self_scene_pos = self.mapToScene(self.boundingRect().bottomRight())
        else:
            self_scene_pos = self.mapToScene(self.boundingRect().topRight())
        return self_scene_pos
