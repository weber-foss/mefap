from PySide2.QtWidgets import QDialog, QMessageBox

from mefap.factory.factory_model import FactoryModel
from mefap.gui.generated.WeightDialog import Ui_weight_dialog
from mefap.gui.pairwise_comparison_dialog import PairwiseComparisonDialog
from mefap.gui.pre_pairwise_comparison_dialog import PrePairwiseComparisonDialog


class WeightDialog(QDialog):
    """for weighting factors for optimization.py"""

    def __init__(self, factory: FactoryModel, calc=False) -> None:
        super().__init__()
        self.ui = Ui_weight_dialog()
        self.ui.setupUi(self)

        self.factory = factory

        if not calc:
            self.ui.pushButton_calculate.setText("OK")

        self.ui.pushButton_balance.clicked.connect(self.balance)
        self.ui.pushButton_calculate.clicked.connect(self.finish)
        self.ui.pushButton_comparison.clicked.connect(self.compare)

        # Mutability
        self.ui.doubleSpinBox_media_availability.setValue(self.factory.weighting.media_availability)
        self.ui.doubleSpinBox_media_availability.valueChanged.connect(self.update_media_availability)
        self.ui.doubleSpinBox_media_compatibility.setValue(self.factory.weighting.media_compatibility)
        self.ui.doubleSpinBox_media_compatibility.valueChanged.connect(self.update_media_compatibility)

        # Materialflow an logistic
        self.ui.doubleSpinBox_material_flow_length.setValue(self.factory.weighting.material_flow_length)
        self.ui.doubleSpinBox_material_flow_length.valueChanged.connect(self.update_material_flow_length)
        self.ui.doubleSpinBox_no_overlapping.setValue(self.factory.weighting.no_overlapping)
        self.ui.doubleSpinBox_no_overlapping.valueChanged.connect(self.update_no_overlapping)
        self.ui.doubleSpinBox_route_continuity.setValue(self.factory.weighting.route_continuity)
        self.ui.doubleSpinBox_route_continuity.valueChanged.connect(self.update_route_continuity)
        self.ui.doubleSpinBox_land_use_degree.setValue(self.factory.weighting.land_use_degree)
        self.ui.doubleSpinBox_land_use_degree.valueChanged.connect(self.update_land_use_degree)

        # Environmental effects
        self.ui.doubleSpinBox_cleanliness.setValue(self.factory.weighting.cleanliness)
        self.ui.doubleSpinBox_cleanliness.valueChanged.connect(self.update_cleanliness)
        self.ui.doubleSpinBox_temperatur.setValue(self.factory.weighting.temperature)
        self.ui.doubleSpinBox_temperatur.valueChanged.connect(self.update_temperatur)
        self.ui.doubleSpinBox_vibration.setValue(self.factory.weighting.vibration)
        self.ui.doubleSpinBox_vibration.valueChanged.connect(self.update_vibration)
        self.ui.doubleSpinBox_quiet.setValue(self.factory.weighting.quiet)
        self.ui.doubleSpinBox_quiet.valueChanged.connect(self.update_quiet)
        self.ui.doubleSpinBox_lighting.setValue(self.factory.weighting.lighting)
        self.ui.doubleSpinBox_lighting.valueChanged.connect(self.update_lighting)

        # Communication
        self.ui.doubleSpinBox_formal_communication.setValue(self.factory.weighting.formal_communication)
        self.ui.doubleSpinBox_formal_communication.valueChanged.connect(self.update_formal_communication)
        self.ui.doubleSpinBox_direct_communication.setValue(self.factory.weighting.direct_communication)
        self.ui.doubleSpinBox_direct_communication.valueChanged.connect(self.update_direct_communication)

        self.update_sum()

    def update_media_availability(self) -> None:
        """
        This method is updating the media_availability in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.media_availability = self.ui.doubleSpinBox_media_availability.value()
        self.update_sum()

    def update_media_compatibility(self) -> None:
        """
        This method is updating the media_compatibility in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.media_compatibility = self.ui.doubleSpinBox_media_compatibility.value()
        self.update_sum()

    def update_material_flow_length(self) -> None:
        """
        This method is updating the material_flow_length in the Weighting object
        of the FactoryModel, depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.material_flow_length = self.ui.doubleSpinBox_material_flow_length.value()
        self.update_sum()

    def update_no_overlapping(self) -> None:
        """
        This method is updating the no_overlapping in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.no_overlapping = self.ui.doubleSpinBox_no_overlapping.value()
        self.update_sum()

    def update_route_continuity(self) -> None:
        """
        This method is updating the route_continuity in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.route_continuity = self.ui.doubleSpinBox_route_continuity.value()
        self.update_sum()

    def update_land_use_degree(self) -> None:
        """
        This method is updating the land_use_degree in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.land_use_degree = self.ui.doubleSpinBox_land_use_degree.value()
        self.update_sum()

    def update_cleanliness(self) -> None:
        """
        This method is updating the cleanliness in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.cleanliness = self.ui.doubleSpinBox_cleanliness.value()
        self.update_sum()

    def update_temperatur(self) -> None:
        """
        This method is updating the temperature in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.temperature = self.ui.doubleSpinBox_temperatur.value()
        self.update_sum()

    def update_vibration(self) -> None:
        """
        This method is updating the vibration in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.vibration = self.ui.doubleSpinBox_vibration.value()
        self.update_sum()

    def update_quiet(self) -> None:
        """
        This method is updating the quiet in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.quiet = self.ui.doubleSpinBox_quiet.value()
        self.update_sum()

    def update_lighting(self) -> None:
        """
        This method is updating the lightning in the Weighting object of the FactoryModel,
        depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.lighting = self.ui.doubleSpinBox_lighting.value()
        self.update_sum()

    def update_formal_communication(self) -> None:
        """
        This method is updating the formal_communication in the Weighting object
        of the FactoryModel, depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.formal_communication = self.ui.doubleSpinBox_formal_communication.value()
        self.update_sum()

    def update_direct_communication(self) -> None:
        """
        This method is updating the direct_communication in the Weighting object
        of the FactoryModel, depending on the given value inside the WeightingDialog.
        :return: None
        """
        self.factory.weighting.direct_communication = self.ui.doubleSpinBox_direct_communication.value()
        self.update_sum()

    def update_sum(self) -> None:
        """
        The method update_sum is responsible for calculating the sum of all weighting attributes
        and displaying the sum inside the dialog. If the sum is greater 100 the text is displayed
        red.
        :return: None
        """
        sum_val = round(self.factory.weighting.direct_communication + \
                        self.factory.weighting.formal_communication + \
                        self.factory.weighting.quiet + \
                        self.factory.weighting.vibration + \
                        self.factory.weighting.temperature + \
                        self.factory.weighting.cleanliness + \
                        self.factory.weighting.land_use_degree + \
                        self.factory.weighting.route_continuity + \
                        self.factory.weighting.no_overlapping + \
                        self.factory.weighting.material_flow_length + \
                        self.factory.weighting.media_compatibility + \
                        self.factory.weighting.media_availability + \
                        self.factory.weighting.lighting, 2)
        self.ui.sum_label.setText(str(sum_val))
        if sum_val > 1:
            self.ui.sum_label.setStyleSheet("font-weight: bold; color: red")
        elif sum_val < 1:
            self.ui.sum_label.setStyleSheet("font-weight: bold; color: red")
        else:
            self.ui.sum_label.setStyleSheet("")

    def balance(self) -> None:
        """
        Balance is called by clicking the balance button.
        Balance is responsible for giving all weighting attributes an äquivivalent value. The sum
        of all values is 100.
        :return: None
        """
        # Mutability
        self.ui.doubleSpinBox_media_availability.setValue(0.08)
        self.ui.doubleSpinBox_media_compatibility.setValue(0.08)

        # Materialflow an logistic
        self.ui.doubleSpinBox_material_flow_length.setValue(0.08)
        self.ui.doubleSpinBox_no_overlapping.setValue(0.08)
        self.ui.doubleSpinBox_route_continuity.setValue(0.08)
        self.ui.doubleSpinBox_land_use_degree.setValue(0.08)

        # Environmental effects
        self.ui.doubleSpinBox_vibration.setValue(0.08)
        self.ui.doubleSpinBox_quiet.setValue(0.08)
        self.ui.doubleSpinBox_lighting.setValue(0.08)
        self.ui.doubleSpinBox_temperatur.setValue(0.07)
        self.ui.doubleSpinBox_cleanliness.setValue(0.07)

        # Communication
        self.ui.doubleSpinBox_formal_communication.setValue(0.07)
        self.ui.doubleSpinBox_direct_communication.setValue(0.07)

        self.update_sum()

    def finish(self) -> None:
        """
        If the sum of the weights is not equal 100 the user cant accept the dialog.
        :return:
        """
        if float(self.ui.sum_label.text()) != 1:
            errordialog = QMessageBox(self)
            errordialog.setText(self.tr("Weighting error!"))
            errordialog.setInformativeText(self.tr(
                "The sum of the weights must be 1!\n Actual sum: {}").format(self.ui.sum_label.text()))
            errordialog.setIcon(QMessageBox.Warning)
            errordialog.exec_()
        else:
            self.accept()

    def compare(self) -> None:
        """
        Opens the PrePairwiseComparisonDialog and PairwiseComparisonDialog.
        :return: None
        """
        pre_compare = PrePairwiseComparisonDialog(self.factory.weighting)
        pre_compare.setModal(True)
        pre_compare.show()
        if pre_compare.exec_():
            criteria = [pre_compare.ui.listWidget_left.item(x).text() for x in
                        range(pre_compare.ui.listWidget_left.model().rowCount())]
            pair_compare_dialog = PairwiseComparisonDialog(self.factory.weighting, criteria, pre_compare.attr_name)
            pair_compare_dialog.setModal(True)
            pair_compare_dialog.show()
            pair_compare_dialog.exec_()
            self.ui.doubleSpinBox_media_availability.setValue(self.factory.weighting.media_availability)
            self.ui.doubleSpinBox_media_compatibility.setValue(self.factory.weighting.media_compatibility)
            self.ui.doubleSpinBox_material_flow_length.setValue(self.factory.weighting.material_flow_length)
            self.ui.doubleSpinBox_no_overlapping.setValue(self.factory.weighting.no_overlapping)
            self.ui.doubleSpinBox_route_continuity.setValue(self.factory.weighting.route_continuity)
            self.ui.doubleSpinBox_land_use_degree.setValue(self.factory.weighting.land_use_degree)
            self.ui.doubleSpinBox_cleanliness.setValue(self.factory.weighting.cleanliness)
            self.ui.doubleSpinBox_temperatur.setValue(self.factory.weighting.temperature)
            self.ui.doubleSpinBox_vibration.setValue(self.factory.weighting.vibration)
            self.ui.doubleSpinBox_quiet.setValue(self.factory.weighting.quiet)
            self.ui.doubleSpinBox_lighting.setValue(self.factory.weighting.lighting)
            self.ui.doubleSpinBox_formal_communication.setValue(self.factory.weighting.formal_communication)
            self.ui.doubleSpinBox_direct_communication.setValue(self.factory.weighting.direct_communication)
            self.update_sum()
