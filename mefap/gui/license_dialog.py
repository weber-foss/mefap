import os

from PySide2.QtWidgets import QDialog

from mefap.gui.generated.LicenseDialog import Ui_License_dialog


class LicenseDialog(QDialog):
    """This Dialog shows the License of this program"""

    def __init__(self) -> None:
        super().__init__()
        self.ui = Ui_License_dialog()
        self.ui.setupUi(self)

        application_path = os.path.dirname(os.path.abspath(__file__))

        license_path = "../../LICENSE"

        file = open(os.path.join(application_path, license_path), encoding='utf-8')

        self.ui.textEdit.setText(file.read())
