import logging
import sys

from mefap.evaluation.evaluate_layout import update_objective_value
from mefap.factory.database import Database
from mefap.factory.reset_layout import reset_layout_to_variant
from mefap.optimization.npp_optimization import npp_optimization


def start_multi_p(multi_tuple) -> None:
    """
    Starts optimisation for the values in the given input tuple and writes the result back into a thread and process
    safe queue.
    :param multi_tuple: The tuple contains 4 values. 1. FactoryModel 2. Outputstream 3. Result queue 4. core id
    :return: None
    """
    sys.stdout = multi_tuple[1]
    try:
        npp_optimization(multi_tuple[0])
        multi_tuple[2].put((multi_tuple[3], multi_tuple[0]))
    except Exception as e:
        multi_tuple[4].put(str(e))
    multi_tuple[2].close()
    multi_tuple[4].close()
    print("Done!")
    logging.info('Dont know why, but with this logging statements the program does not crash on windows')


def save_multi_result(factory, result_list) -> None:
    """
    After the optimisation this method get called to merge all produced results into the main factory.
    :param factory: FactoryModel where the results get saved.
    :param result_list: List of FactoryModel produced during optimisation.
    :return: None
    """
    database = Database()
    backup_base = Database()

    backup_base.save_in_database(factory)

    for result in result_list:
        for facility in result[1].facility_list:
            if facility.curr_position == [None, None]:
                break
        else:
            if len(result_list) < 5:
                for layout_idx in range(len(result[1].layouts)):
                    database.save_variant_in_database(result[1], layout_idx)
            else:
                database.save_in_database(result[1])

    # set layout to 1 version
    reset_layout_to_variant(factory, database, 0)

    update_objective_value(factory, database)

    # set factory to backup. No change should be applied to factory layout, without user interaction
    reset_layout_to_variant(factory, backup_base, 0)

    database.sort_by_weight_sum()

    # transfer db to factory
    factory.transfer_all_variants_to_factory(database)
