from PySide2.QtCore import QObject
from PySide2.QtGui import QBrush, QColor

from mefap.factory.restrictive_area import RestrictiveArea
from mefap.gui.cell import Cell
from mefap.gui.util.colors import GREY_CELL, EMPTY_CELL, DARK_GREY_CELL, BLACK


class FactoryCell(Cell, QObject):
    """
    Object representing a cell on the factory grid.
    Its x and y coordinates define where in the grid the cell resides.
    """

    def __init__(self, x_c, y_c, params, *args, **kwargs):
        Cell.__init__(self, x_c, y_c, *args, **kwargs)

        self.factory_params = params
        # representing index
        self.index = -1
        self.index_old = -1

        self.gray = False

    def set_index_in_matrix(self, index_new: int) -> None:
        """
        Changes the index of this object and the value of this object in the factory.layout.
        :param index_new: new index
        :return: None
        """
        self.index_old = self.index
        self.index = index_new
        self.scene().factory.layout[self.y_cord][self.x_cord] = index_new

    def graying(self) -> None:
        """
        This method change the color of this cell on his layout
        to grey and marks the cell to be not existing
        in the layout matrix of the factory model.
        :return: None
        """
        current_index = self.scene().factory.layout[self.y_cord][self.x_cord]
        if current_index >= 0:
            for facility in self.scene().facilities:
                if facility.attributes.index_num == current_index:
                    facility.delete(without_propagation=True)
                    break
        self.set_index_in_matrix(self.factory_params['hidden_cell_value'])
        self.setBrush(QBrush(BLACK))
        self.gray = True

    def color_road(self) -> None:
        """
        Colors this cell as normal road.
        :return: None
        """
        self.setBrush(QBrush(GREY_CELL))

    def color_fixed_road(self) -> None:
        """
        Colors this cell as fixed road.
        :return: None
        """
        self.setBrush(QBrush(DARK_GREY_CELL))

    def color_empty(self) -> None:
        """
        Colors this cell as empty cell. (No road)
        :return: Node
        """
        self.setBrush(QBrush(EMPTY_CELL))

    def color_sound_wall(self) -> None:
        """
        Colors this cell as sound wall cell.
        :return: Node
        """
        self.setBrush(QBrush(BLACK))

    def media_graying(self) -> None:
        """
        This method change the color of this cell on his layout
        to grey and marks the cell to be not existing
        in the layout matrix of the factory model.
        :return: None
        """
        self.set_index_in_matrix(self.factory_params['empty_cell_value'])
        self.setBrush(QBrush(GREY_CELL))

    def tone(self):
        """
        This method change the color of this cell on his layout to cyan and marks the cell as empty
        in the layout matrix of the factory model.
        :return: None
        """
        self.set_index_in_matrix(self.factory_params['empty_cell_value'])
        self.setBrush(QBrush(EMPTY_CELL))
        self.gray = False

    def color(self, color: QColor, facility_id: int) -> None:
        """
        This method change the color of this cell on his layout
        to the given QColor and marks the cell as the given id
        in the layout matrix of the FactoryModel.
        :param color: The QColor which this FactoryCell going to colored with.
        :param facility_id: The id which will be written into the FactoryModel
        :return: None
        """
        self.setBrush(QBrush(color))
        self.set_index_in_matrix(facility_id)

    def color_media(self, color) -> None:
        """
        Set the color of this cell to the given color.
        :param color: new Color
        :return:
        """
        self.setToolTip("")
        self.setBrush(color)

    def hide(self) -> None:
        """
        This method hides this cell on his scene and marks the cell to be not existing
        in the layout matrix of the factory model.
        :return: None
        """
        self.set_index_in_matrix(self.factory_params['hidden_cell_value'])
        super().hide()
        self.scene().update()

    def on_wall(self) -> bool:
        """
        Checks own position wether the cell is a cell on a wall or not.
        :return: True if this cell is on a wall
        """
        layout = self.scene().factory.layout
        if self.x_cord + 1 > layout.shape[1] - 1:
            return True
        elif self.x_cord - 1 < 0:
            return True
        elif self.y_cord + 1 > layout.shape[0] - 1:
            return True
        elif self.y_cord - 1 < 0:
            return True
        else:
            for x in range(-1, 2):
                for y in range(-1, 2):
                    if layout[self.y_cord + y][self.x_cord + x] == self.factory_params['hidden_cell_value']:
                        return True
        return False

    def wall_count(self):
        count = 0
        layout = self.scene().factory.layout
        if self.x_cord + 1 > layout.shape[1] - 1:
            count += 1
        if self.x_cord - 1 < 0:
            count += 1
        if self.y_cord + 1 > layout.shape[0] - 1:
            count += 1
        if self.y_cord - 1 < 0:
            count += 1
        else:
            for x in range(-1, 2):
                for y in range(-1, 2):
                    try:
                        if layout[self.y_cord + y][self.x_cord + x] == self.factory_params['hidden_cell_value']:
                            count += 1
                    except:
                        pass
        return count

    def change_area(self, area: RestrictiveArea) -> None:
        """
        This method changes to which restrictive area this position belongs.
        :param area: The area type to be changed to.
        :return: None
        """
        self.scene().factory.restrictive_area[self.y_cord][self.x_cord] = area.id
        self.display_area()

    def get_area(self) -> int:
        """
        This methods checks which restictive area id belongs to this cell position.
        :return: area type id
        """
        return self.scene().factory.restrictive_area[self.y_cord][self.x_cord]

    def display_area(self) -> None:
        """
        This method is coloring this cell depending on the underlaying restrictive area.
        :return: None
        """
        actual_id = self.scene().factory.restrictive_area[self.y_cord][self.x_cord]
        if actual_id != 0:
            for area in self.scene().factory.area_types:
                if area.id == actual_id:
                    self.setBrush(area.color)
                    self.setToolTip(self.tr("Name: {}".format(area.name)))
                    break
        else:
            self.setBrush(EMPTY_CELL)

    def update_path(self) -> bool:
        """
        Colors this cell either as empty, road or fixed road.
        :return: True if colored to a road value.
        """
        if self.scene().factory.layout[self.y_cord][self.x_cord] == self.factory_params['local_road_value']:
            self.color_road()
            return True
        elif self.scene().factory.layout[self.y_cord][self.x_cord] == self.factory_params['empty_cell_value']:
            self.color_empty()
            return True
        elif self.scene().factory.layout[self.y_cord][self.x_cord] == self.factory_params['fixed_road_value']:
            self.color_fixed_road()
            return True
        return False

    def update_path_with_cell_value(self, cell_value) -> bool:
        """
        Colors the cell to the right road, depending on the given cell value.
        :param cell_value:
        :return: True if colored to a road value.
        """
        if cell_value == self.factory_params['local_road_value']:
            self.color_road()
            return True
        elif cell_value == self.factory_params['empty_cell_value']:
            self.color_empty()
            return True
        elif cell_value == self.factory_params['fixed_road_value']:
            self.color_fixed_road()
            return True
        return False

    def set_fixed_road(self) -> None:
        """
        Sets value of underlying layout to fixed road.
        :return: None
        """
        actual_value = self.scene().factory.layout[self.y_cord][self.x_cord]
        if actual_value == self.factory_params['empty_cell_value'] \
                or actual_value == self.factory_params['local_road_value']:
            self.set_index_in_matrix(self.factory_params['fixed_road_value'])
            self.color_fixed_road()

    def remove_fixed_road(self) -> None:
        """
        Removes coloring of fixed road.
        :return: None
        """
        if self.scene().factory.layout[self.y_cord][self.x_cord] == self.factory_params['fixed_road_value']:
            self.set_index_in_matrix(self.factory_params['local_road_value'])
            self.color_road()

    def show_noise(self, colormap, db):
        self.setToolTip(str(round(db)) + " [dB]")
        db_highest = 85
        db_threshold = 40
        db_highest -= db_threshold
        db -= db_threshold
        color = colormap(1 - db / db_highest, bytes=True)
        self.setBrush(QBrush(QColor(color[0], color[1], color[2], color[3])))

    def __repr__(self):
        return "({}x{})".format(self.x_cord, self.y_cord)
