from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog

from mefap.gui.factory_scene_layout import FactoryLayoutScene
from mefap.gui.factory_view import FactoryView
from mefap.gui.generated.FactorySetSize import Ui_set_size_dialog
from mefap.gui.util.layout_mode import LayoutMode


class FactorySetSizeDialog(QDialog):
    """
    This QDialog is responsible for removing unnecessary FactoryCells from our FactoryModel.
    This is for resizing the factory.
    """

    def __init__(self, factory) -> None:
        super().__init__()
        self.ui = Ui_set_size_dialog()
        self.ui.setupUi(self)

        self.factory = factory

        # initialise factory visualisation
        self.factory_scene = FactoryLayoutScene(self.factory, LayoutMode.REMOVE)

        self.graphicsView_factory = FactoryView(self)
        self.ui.vertical_layout.addWidget(self.graphicsView_factory)
        self.ui.vertical_layout.addWidget(self.ui.button_box)
        self.graphicsView_factory.setScene(self.factory_scene)

        self.factory_scene.init_grid(with_hidden=True)
        self.factory_scene.load_facilities()
        self.factory_scene.transparent_facilities()

        self.graphicsView_factory.setToolTip(self.tr("With the right mouse button cells can be marked which will later be hidden.\n\nWhite = in use\nBlack = not in use"))

    def fit_view(self) -> None:
        """
        Fits the view into the given space in this dialog.
        :return: None
        """
        self.graphicsView_factory.fitInView(0, 0,
                                            self.factory.layout.shape[1] * self.factory.cell_zoom,
                                            self.factory.layout.shape[0] * self.factory.cell_zoom,
                                            Qt.KeepAspectRatio)
