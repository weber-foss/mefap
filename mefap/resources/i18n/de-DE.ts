<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AddFacilityDialog</name>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="14"/>
        <source>MeFaP - Add Facility</source>
        <translation>MeFaP - Fabrikobjekt hinzufügen</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot;
       font-size:24pt; font-weight:600;&quot;&gt;Add Facility&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
      </source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot;font-size:24pt; font-weight:600;&quot;&gt;Fabrikobjekt hinzufügen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="32"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Allgemein&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Allgemein&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="41"/>
        <source>Bereichsname</source>
        <translation>Fabrikobjektname</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="58"/>
        <source>Abteilung</source>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="77"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="89"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Abmessungen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Abmessungen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="141"/>
        <source>min. Breite [m]</source>
        <translation>min. Breite [m]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Traglasten&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Traglasten&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="199"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Eigenschaften&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Eigenschaften&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="211"/>
        <source>Ruhebedarf [dB]</source>
        <translation>max. Schallimmission [dB]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="239"/>
        <source>Lichtbedarf [lx]</source>
        <translation>Lichtbedarf [lx]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="397"/>
        <source>Niedrig</source>
        <translation>Niedrig</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="402"/>
        <source>Mittel</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="407"/>
        <source>Hoch</source>
        <translation>Hoch</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="337"/>
        <source>Temperatur</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="345"/>
        <source>Hitzeemfindlich</source>
        <translation>Hitzeemfindlich</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="376"/>
        <source>Neutral</source>
        <translation>Neutral</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="355"/>
        <source>Hitzeemittierend</source>
        <translation>Hitzeemittierend</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="363"/>
        <source>Sauberkeit</source>
        <translation>Sauberkeit</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="371"/>
        <source>Schmutzemfindlich</source>
        <translation>Schmutzemfindlich</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="381"/>
        <source>Schmutzemittierend</source>
        <translation>Schmutzemittierend</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="415"/>
        <source>Mitarbeiteranzahl</source>
        <translation>Mitarbeiteranzahl</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="431"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Weitere
       Restriktionen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
      </source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Weitere Restriktionen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="48"/>
        <source>Flaechenart</source>
        <translation>Funktionsflächenart</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="124"/>
        <source>Hoehe [m]</source>
        <translation>Höhe [m]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="148"/>
        <source>min. Laenge [m]</source>
        <translation>min. Länge [m]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="169"/>
        <source>Erforderliche Deckentraglast [kg/m2]</source>
        <translation>Erforderliche Deckentraglast [kg/m²]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="183"/>
        <source>Gewicht / erf. Bodentraglast [kg/m2]</source>
        <translation>Gewicht / erf. Bodentraglast [kg/m²]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="225"/>
        <source>Laermexposition [dB]</source>
        <translation>Schallemmission [dB]</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="247"/>
        <source>&lt; 100</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="252"/>
        <source>100 - 200</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="257"/>
        <source>200 - 300</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="262"/>
        <source>300 - 500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="267"/>
        <source>500 - 750</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="272"/>
        <source>750 - 1000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="277"/>
        <source>1000 - 1500</source>
        <translation></translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="285"/>
        <source>Erschuetterungsanregung</source>
        <translation>Vibrationsemmission</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="311"/>
        <source>Erschuetterungssensitivitaet</source>
        <translation>Vibrationssensitivität</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="389"/>
        <source>Mobilitaet</source>
        <translation>Mobilität</translation>
    </message>
    <message>
        <location filename="FactoryFacilityDialog.ui" line="445"/>
        <source>Aussenwandpositioinierung</source>
        <translation>Außenwandpositioinierung</translation>
    </message>
</context>
<context>
    <name>CollapsibleDialog</name>
    <message>
        <location filename="collapsible_dialog.py" line="130"/>
        <source>Availible Facilities</source>
        <translation>Verfügbare Fabrikobjekte</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="131"/>
        <source>Restrictive area&apos;s</source>
        <translation>Restriktive Nutzungsbereiche</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="217"/>
        <source>Diagram</source>
        <translation>Diagramm</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="61"/>
        <source>Movable facility</source>
        <translation>Bewegbares Farbikobjekt</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="67"/>
        <source>Fixed facility</source>
        <translation>Fixiertes Fabrikobjekt</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="129"/>
        <source>Legend</source>
        <translation>Legende</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="278"/>
        <source>Noise Legend</source>
        <translation>Lärm Legende</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="85"/>
        <source>Variable transport route</source>
        <translation>Variabler Transportweg</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="91"/>
        <source>Fixed transport route</source>
        <translation>Fixierter Transportweg</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="224"/>
        <source>A network diagram with all evaluation criteria is displayed here.</source>
        <translation>Hier wird ein Netzwerkdiagramm mit allen Bewertungskriterien angezeigt.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="117"/>
        <source>All restrictive areas of use and their associated color are displayed here.</source>
        <translation>Hier werden alle Restriktive Nutzungsbereiche und die dazugehörige Farbe angezeigt.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="120"/>
        <source>Here is a legend showing what which cell means.</source>
        <translation>Hier ist eine Legende, die zeigt, was welche Zelle bedeutet.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="122"/>
        <source>This shows which facilities have already been placed.</source>
        <translation>Dies zeigt, welche Einrichtungen bereits platziert wurden.</translation>
    </message>
    <message>
        <location filename="collapsible_dialog.py" line="279"/>
        <source>A legend is shown here which indicates which color means which volume.</source>
        <translation>Hier wird eine Legende angezeigt, die angibt, welche Farbe welches Volumen bedeutet.</translation>
    </message>
</context>
<context>
    <name>CompareDialog</name>
    <message>
        <location filename="compare_dialog.py" line="53"/>
        <source>Layout {}: {:.3f}%</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EvaluationDialog</name>
    <message>
        <location filename="evaluation_dialog.py" line="33"/>
        <source>Kriterien</source>
        <translation>Zielkriterien</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="33"/>
        <source>Gewichtung[%]</source>
        <translation>Gewichtung [%]</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="33"/>
        <source>Ergebniswerte</source>
        <translation>Ergebniswerte</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="34"/>
        <source>Bewertung</source>
        <translation>Bewertung [%]</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="34"/>
        <source>Bestwerte</source>
        <translation>Bestwerte</translation>
    </message>
    <message>
        <location filename="evaluation_dialog.py" line="164"/>
        <source>Weighted Sum</source>
        <translation>Gewichtete Summe</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="28"/>
        <source>no_overlapping</source>
        <translation>Überschneidung des Materialfluss</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="27"/>
        <source>material_flow_length</source>
        <translation>Materialflusslänge</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="29"/>
        <source>route_continuity</source>
        <translation>Stetigkeit des Materialfluss</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="30"/>
        <source>land_use_degree</source>
        <translation>Flächennutzungsgrad</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="26"/>
        <source>media_availability</source>
        <translation>Medienverfügbarkeit</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="25"/>
        <source>media_compatibility</source>
        <translation>Medienkomptibilität</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="31"/>
        <source>lighting</source>
        <translation>Beleuchtung</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="32"/>
        <source>quiet</source>
        <translation>Ruhe / Schallausbreitung</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="33"/>
        <source>vibration</source>
        <translation>Vibration</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="34"/>
        <source>temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="35"/>
        <source>cleanliness</source>
        <translation>Sauberkeit</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="36"/>
        <source>direct_communication</source>
        <translation>Direkte Kommunikation</translation>
    </message>
    <message>
        <location filename="factory_compare_dialog.py" line="40"/>
        <source>formal_communication</source>
        <translation>Formale Kommunikation</translation>
    </message>
</context>
<context>
    <name>FacilityDialog</name>
    <message>
        <location filename="facility_dialog.py" line="30"/>
        <source>Allokation Quelle und Senke der Facilites</source>
        <translation>Allokation Quelle und Senke der Fabrikobjekte</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="87"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="95"/>
        <source>Nicht alle Quelle und Senken Positionen wurden definiert</source>
        <translation>Nicht alle Quellen- und Senkenpositionen wurden definiert</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="111"/>
        <source>Quelle und Senken Position wurden noch nicht definiert</source>
        <translation>Quellen- und Senkenposition wurden noch nicht definiert</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="128"/>
        <source>Allokation der Quelle und Senke wird verlassen</source>
        <translation>Allokation der Quelle und Senke wird verlassen</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="97"/>
        <source>Vor der weiteren Bearbeitung muessen alle Quellen und Senken definiert werden.</source>
        <translation>Vor der weiteren Bearbeitung müssen alle Quellen und Senken definiert werden.</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="113"/>
        <source>Vor der weiteren Bearbeitung muessen Quelle und Senke definiert werden.</source>
        <translation>Vor der weiteren Bearbeitung müssen Quelle und Senke definiert werden.</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="131"/>
        <source>Vor der weiteren Bearbeitung muessen alle Quellen und Senken definiert werden.
Alle Aenderungen gehen verloren!</source>
        <translation>Vor der weiteren Bearbeitung müssen alle Quellen und Senken definiert werden.
Alle Änderungen gehen verloren!</translation>
    </message>
    <message>
        <location filename="facility_dialog.py" line="44"/>
        <source>With the left mouse button you can assign either a source, a sink or both at the same time to the unit cells.</source>
        <translation>Mit der linken Maustaste können Sie den Einheitszellen gleichzeitig entweder eine Quelle, eine Senke oder beides zuweisen.</translation>
    </message>
</context>
<context>
    <name>FacilityScene</name>
    <message>
        <location filename="facility_scene.py" line="83"/>
        <source>SinkSourceMenu</source>
        <translation>Quellen/Senken Menü</translation>
    </message>
    <message>
        <location filename="facility_scene.py" line="84"/>
        <source>Quelle</source>
        <translation>Quelle</translation>
    </message>
    <message>
        <location filename="facility_scene.py" line="86"/>
        <source>Senke</source>
        <translation>Senke</translation>
    </message>
</context>
<context>
    <name>FactoryAddFacilityDialog</name>
    <message>
        <location filename="facility_add_dialog.py" line="107"/>
        <source>Add Item</source>
        <translation>Add facility</translation>
    </message>
    <message>
        <location filename="facility_add_dialog.py" line="114"/>
        <source>Add area type</source>
        <translation>Bereichstyp hinzufügen</translation>
    </message>
    <message>
        <location filename="facility_add_dialog.py" line="114"/>
        <source>Enter a name for the new area type.</source>
        <translation>Geben Sie einen Namen für den neuen Bereichstyp ein.</translation>
    </message>
</context>
<context>
    <name>FactoryCell</name>
    <message>
        <location filename="factory_cell.py" line="199"/>
        <source>Name: {}</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FactoryChangeSizeDialog</name>
    <message>
        <location filename="FactoryChangeSizeDialog.ui" line="14"/>
        <source>Change facory size</source>
        <translation>Fabrikgröße ändern</translation>
    </message>
    <message>
        <location filename="FactoryChangeSizeDialog.ui" line="22"/>
        <source>Factory width [m]</source>
        <translation>Fabrikbreite [m]</translation>
    </message>
    <message>
        <location filename="FactoryChangeSizeDialog.ui" line="52"/>
        <source>Factory length [m]</source>
        <translation>Fabriklänge [m]</translation>
    </message>
    <message>
        <location filename="factory_change_size_dialog.py" line="48"/>
        <source>Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!</source>
        <translation>Die Größe der Fabrik muss teilbar durch die Zellgröße sein!</translation>
    </message>
</context>
<context>
    <name>FactoryExtendedSettings</name>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="14"/>
        <source>Extended Settings</source>
        <translation>Erweiterte Einstellungen</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="36"/>
        <source>Empty cell costs</source>
        <translation>Wegnutzungskosten - leere Zelle</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="43"/>
        <source>Local road costs</source>
        <translation>Wegnutzungskosten - variabler Weg</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="50"/>
        <source>Fixed road costs</source>
        <translation>Wegnutzungskosten - fixierter Weg</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="85"/>
        <source>media availability factor</source>
        <translation>Medienverfügbarkeitsfaktor</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="103"/>
        <source>Beleuchtung</source>
        <translation>Beleuchtung</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="109"/>
        <source>Road illuminance</source>
        <translation>minimale Wegebeleuchtung</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="168"/>
        <source>sound pressure reduction</source>
        <translation>Schaldruckpegelreduktion</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="175"/>
        <source>Sound propagation conditions</source>
        <translation>Schallausbreitungsbedingung</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="461"/>
        <source>lrs search steps</source>
        <translation>LRS: Iterationsanzahl</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="475"/>
        <source>lrs step size</source>
        <translation>LRS: Schrittweite</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="538"/>
        <source>mees area difference</source>
        <translation>MEES: Flächenabweichung [%]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="561"/>
        <source>fas order</source>
        <translation>OAS: Freiflächensortierung</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="581"/>
        <source>mees step size</source>
        <translation>MEES: Schrittweite</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="30"/>
        <source>A*-Algorithmus</source>
        <translation>A*-Algorithmus</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="162"/>
        <source>Ruhe</source>
        <translation>Ruhe</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="455"/>
        <source>Parameter der Optimierung</source>
        <translation>Parameter der Optimierung</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="495"/>
        <source>mees facility range from</source>
        <translation>MEES: min. Fabrikobjektanzahl</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="518"/>
        <source>mees facility range to</source>
        <translation>MEES: max. Fabrikobjektanzahl</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="208"/>
        <source>Sound reflection on walls</source>
        <translation>Schallreflexion an Wänden</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="601"/>
        <source>termination criterion</source>
        <translation>Abbruchkriterium</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="79"/>
        <source>Medienverfuegbarkeit</source>
        <translation>Medienverfügbarkeit</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="123"/>
        <source>Kein Licht [&lt; 100lx]</source>
        <translation>Kein Licht [≤100lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="128"/>
        <source>Sehr wenig Licht [100 - 200 lx]</source>
        <translation>Sehr wenig Licht  [&gt;100 - ≤200 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="133"/>
        <source>Wenig Licht 200 - 300lx]</source>
        <translation>Wenig Licht [&gt;200 - ≤300lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="138"/>
        <source>Normales Licht [300 - 500 lx]</source>
        <translation>Normales Licht [&gt;300 - ≤500 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="143"/>
        <source>Helles Licht [500 - 750 lx]</source>
        <translation>Helles Licht [&gt;500 - ≤750 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="148"/>
        <source>Sehr helles Licht [750 - 1000 lx]</source>
        <translation>Sehr helles Licht [&gt;750 - ≤1000 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="153"/>
        <source>maximales Licht [1000 - 1500 lx]</source>
        <translation>Maximales Licht [&gt;1000 - ≤1500 lx]</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="222"/>
        <source>&lt;h2&gt;Sound propagation&lt;/h2&gt;</source>
        <translation>&lt;h2&gt;Schallausbreitung&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="229"/>
        <source>Semi diffuse</source>
        <translation>Semi-Diffus</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="236"/>
        <source>Flat room</source>
        <translation>Flachraum</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="243"/>
        <source>&lt;h2&gt;Sound reduction&lt;/h2&gt;

</source>
        <translation>&lt;h2&gt;Schallreduktion&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="252"/>
        <source>Encapsulation</source>
        <translation>Schallschutzkapsel</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="259"/>
        <source>Barrier isolation</source>
        <translation>Schallschutzbarriere</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="266"/>
        <source>Factory wall isolation</source>
        <translation>Schallschutzwand</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="273"/>
        <source>&lt;h2&gt;Sound absorption&lt;/h2&gt;
</source>
        <translation>&lt;h2&gt;Schallabsorption&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="281"/>
        <source>Absorption coefficient</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="288"/>
        <source>Absorption floor</source>
        <translation>Boden</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="295"/>
        <source>Absorption ceil</source>
        <translation>Decke</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="302"/>
        <source>Absorption wall</source>
        <translation>Wände</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="624"/>
        <source>Number of experiments</source>
        <translation>Anzahl der Versuche</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="426"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="FactoryExtendedSettings.ui" line="440"/>
        <source>Color facilties by noise exposure</source>
        <translation>Color facilties by noise exposure</translation>
    </message>
</context>
<context>
    <name>FactoryFacility</name>
    <message>
        <location filename="factory_facility.py" line="111"/>
        <source>Mirror</source>
        <translation>vertikale Spiegelung</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="101"/>
        <source>Rotation</source>
        <translation>Rotation</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="108"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="114"/>
        <source>Un-/Lock position</source>
        <translation>Position fixieren / lösen</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="514"/>
        <source>Placement error</source>
        <translation>Platzierungsfehler</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="358"/>
        <source>No Placing outside the grid allowed!</source>
        <translation>Kein Platzieren außerhalb des Gitters erlaubt!</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="102"/>
        <source>Rotate 90</source>
        <translation>90° drehen</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="105"/>
        <source>Rotate 180</source>
        <translation>180° drehen</translation>
    </message>
    <message>
        <location filename="factory_facility.py" line="542"/>
        <source>Name: {}
Position: {}
Rotation: {}
Spiegelung: {}
Fixiert: {}</source>
        <translation>Name: {}
Position: {}
Rotation: {}°
Spiegelung: {}
Fixiert: {}</translation>
    </message>
</context>
<context>
    <name>FactoryLayoutScene</name>
    <message>
        <location filename="factory_scene_layout.py" line="184"/>
        <source>&lt;h1&gt;Ansicht: </source>
        <translation>Ansicht: </translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="114"/>
        <source>&lt;h1&gt;Ansicht: Cell Info&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Zelleninformation&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="128"/>
        <source>&lt;h1&gt;Ansicht: Standard&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Standard&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="142"/>
        <source>&lt;h1&gt;Ansicht: Licht&lt;/h2&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Beleuchtung&lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="219"/>
        <source>&lt;h1&gt;Ansicht: Restrictive Area&apos;s&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: restriktive Nutzungsbereiche&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="407"/>
        <source>Some areas {} are not large enough to accommodate all of the related facilities!</source>
        <translation>Einige Bereiche {} sind nicht groß genug, um alle zugehörigen Einrichtungen aufzunehmen!</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="420"/>
        <source>Some facilities {} are placed outside their area of use and will be deleted!</source>
        <translation>Einige Bereiche {} sind außerhalb der Nutzungsbreich(e) positioniert und werden gelöscht!</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="233"/>
        <source>&lt;h1&gt;Ansicht: Noise matrix&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Schallausbreitung&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="330"/>
        <source>&lt;h1&gt;Ansicht: Material flow (out)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Materialfluss (ausgehend)&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="357"/>
        <source>&lt;h1&gt;Ansicht: Communication flow (out)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Kommunikationsfluss (ausgehend)&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="340"/>
        <source>&lt;h1&gt;Ansicht: Material flow (in)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Materialfluss (eingehend)&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="367"/>
        <source>&lt;h1&gt;Ansicht: Communication flow (in)&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Ansicht: Kommunikationsfluss (eingehend)&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="404"/>
        <source>Value error!</source>
        <translation>Wertfehler!</translation>
    </message>
    <message>
        <location filename="factory_scene_layout.py" line="418"/>
        <source>Some facilities will be deleted!</source>
        <translation>Einige Fabrikobjekte werden gelöscht!</translation>
    </message>
</context>
<context>
    <name>FactorySetSizeDialog</name>
    <message>
        <location filename="factory_set_size_dialog.py" line="35"/>
        <source>With the right mouse button cells can be marked which will later be hidden.

White = in use
Black = not in use</source>
        <translation>Mit der rechten Maustaste können Zellen markiert werden, die später ausgeblendet werden.

Weiß = in Gebrauch
Schwarz = nicht in Gebrauch</translation>
    </message>
</context>
<context>
    <name>FactorySizeDialog</name>
    <message>
        <location filename="factory_size_dialog.py" line="56"/>
        <source>Die Groesse der Fabrik muss teilbar durch die Zellgroesse sein!</source>
        <translation>Die Größe der Fabrik muss teilbar durch die Zellgröße sein!</translation>
    </message>
</context>
<context>
    <name>FactoryTable</name>
    <message>
        <location filename="factory_table.py" line="166"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="117"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="118"/>
        <source>Department</source>
        <translation>Abteilung</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="120"/>
        <source>Breite</source>
        <translation>Breite</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="123"/>
        <source>Bodentragkraft</source>
        <translation>Bodentraglast</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="124"/>
        <source>Deckentraglast</source>
        <translation>Deckentraglast</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="125"/>
        <source>Wandpositionierung</source>
        <translation>Außenwandpositionierung</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="167"/>
        <source>Ruhebedarf</source>
        <translation>max. Schallimmission [dB]</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="169"/>
        <source>Lichtbedarf</source>
        <translation>Beleuchtungsbedarf</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="172"/>
        <source>Temperatur</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="173"/>
        <source>Sauberkeit</source>
        <translation>Sauberkeit</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="199"/>
        <source>Facility type unknown!</source>
        <translation>Unbekanntes Fabrikobjekt!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="209"/>
        <source>Department unknown!</source>
        <translation>Abteilung unbekannt!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="394"/>
        <source>Value must be in range {}-{}!</source>
        <translation>Der Wert muss im Bereich {} - {} liegen!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="430"/>
        <source>Value must be greater equals 0!</source>
        <translation>Wert muss größer/gleich 0 sein!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="479"/>
        <source>Delete Facility</source>
        <translation>Lösche Fabrikobjekt</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="512"/>
        <source>After the facility has been deleted, the generated layouts are no longer optimal layouts!</source>
        <translation>Nachdem das Fabrikobjekt gelöscht wurde, sind die erzeugten Layouts nicht mehr optimal!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="200"/>
        <source>Choose an existing facility.</source>
        <translation>Wählen Sie eine vorhandenes Farbikobjekt.</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="210"/>
        <source>Choose an existing Department.</source>
        <translation>Wählen Sie eine vorhandene Abteilung.</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="429"/>
        <source>Value error!</source>
        <translation>Wertfehler!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="509"/>
        <source>Are you sure you want to delete the {} facility?!</source>
        <translation>Sind sie sich sicher, dass sie das {} Farbikobjekt löschen möchten?!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="556"/>
        <source>Add Media</source>
        <translation>Medien hinzufügen</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="556"/>
        <source>Enter the media name</source>
        <translation>Geben Sie den Mediennamen ein</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="562"/>
        <source>This media already exists!</source>
        <translation>Dieses Medium existiert bereits!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="563"/>
        <source>Choose a different name for this media.</source>
        <translation>Wählen Sie einen anderen Namen für dieses Medium.</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="586"/>
        <source>Are you sure you want to delete the {} media?</source>
        <translation>Möchten Sie das {} Medium wirklich löschen?</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="589"/>
        <source>After the media has been deleted, the generated layouts are no longer optimal layouts!</source>
        <translation>Nach dem Löschen des Mediums sind die generierten Layouts keine optimalen Layouts mehr!</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="119"/>
        <source>Flaeche</source>
        <translation>Fläche[m²]</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="121"/>
        <source>Laenge</source>
        <translation>Länge [m]</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="122"/>
        <source>Hoehe</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="168"/>
        <source>Laermexposition</source>
        <translation>Schallemmission [dB]</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="170"/>
        <source>Erschuetterungsanregung</source>
        <translation>Vibrationsemmission</translation>
    </message>
    <message>
        <location filename="factory_table.py" line="171"/>
        <source>Erschuetterungssensitivitaet</source>
        <translation>Vibrationssensitivität</translation>
    </message>
</context>
<context>
    <name>FactoryView</name>
    <message>
        <location filename="factory_view.py" line="94"/>
        <source>FacilityMenu</source>
        <translation>Fabrikobjektmenü</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="111"/>
        <source> vorhanden</source>
        <translation> vorhanden</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="112"/>
        <source>Kein </source>
        <translation>Kein </translation>
    </message>
    <message>
        <location filename="factory_view.py" line="205"/>
        <source>Geben Sie einen Wert ein</source>
        <translation>Geben Sie einen Wert ein</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="126"/>
        <source>LightMenu</source>
        <translation>Beleuchtungsmenü</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="244"/>
        <source>Fixed Road</source>
        <translation>Fixierte Wegfläche</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="252"/>
        <source>Remove Fixed Road</source>
        <translation>Lösche fixierte Wegfläche</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="193"/>
        <source>Toggle In/Out</source>
        <translation>Wechsel eingehend/ausgehend</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="228"/>
        <source>Value error!</source>
        <translation>Wertfehler!</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="230"/>
        <source>Not all cells could be updated because the objects placed on them need a larger {} value than the entered value.</source>
        <translation>Nicht alle Zellen konnten aktualisiert werden, da die darauf platzierten Objekte einen größeren {} Wert als den eingegebenen Wert benötigen.</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="304"/>
        <source>Facility can&apos;t be placed here!</source>
        <translation>Einrichtung kann hier nicht platziert werden!</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="127"/>
        <source>Kein Licht [&lt; 100lx]</source>
        <translation>Kein Licht [≤100lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="128"/>
        <source>Sehr wenig Licht [100 - 200 lx]</source>
        <translation>Sehr wenig Licht  [&gt;100 - ≤200 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="129"/>
        <source>Wenig Licht [200 - 300lx]</source>
        <translation>Wenig Licht [&gt;200 - ≤300lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="130"/>
        <source>Normales Licht [300 - 500 lx]</source>
        <translation>Normales Licht [&gt;300 - ≤500 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="131"/>
        <source>Helles Licht [500 - 750 lx]</source>
        <translation>Helles Licht [&gt;500 - ≤750 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="132"/>
        <source>Sehr helles Licht [750 - 1000 lx]</source>
        <translation>Sehr helles Licht [&gt;750 - ≤1000 lx]</translation>
    </message>
    <message>
        <location filename="factory_view.py" line="133"/>
        <source>maximales Licht [1000 - 1500 lx]</source>
        <translation>Maximales Licht [&gt;1000 - ≤1500 lx]</translation>
    </message>
</context>
<context>
    <name>Factory_all_layouts</name>
    <message>
        <location filename="FactoryAllLayouts.ui" line="14"/>
        <source>All Layouts</source>
        <translation>Alle Layoutvarianten</translation>
    </message>
</context>
<context>
    <name>LayoutDetailDialog</name>
    <message>
        <location filename="LayoutDetailDialog.ui" line="14"/>
        <source>Layout Details</source>
        <translation>Layoutdetails</translation>
    </message>
</context>
<context>
    <name>License_dialog</name>
    <message>
        <location filename="LicenseDialog.ui" line="14"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="main_view.py" line="662"/>
        <source>All facilities must be placed before evaluation!</source>
        <translation>Um ein Layout bewerten zu können, müssen alle Fabrikobjekte positionert werden!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="221"/>
        <source>Do you want to load data from an Excel sheet?</source>
        <translation>Möchten Sie Daten aus einer Excel-Tabelle laden?</translation>
    </message>
    <message>
        <location filename="main_view.py" line="351"/>
        <source>The new factory size is smaller than the previous one!</source>
        <translation>Die neue Fabrikgröße ist kleiner als die vorherige!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="353"/>
        <source>Some facilities may get deleted.
All previously optimised layouts will get deleted!</source>
        <translation>Einige Fabrikobjekte werden möglicherweise gelöscht.
Alle zuvor optimierten Layouts werden gelöscht!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="367"/>
        <source>The new factory size differs from the the previous one!</source>
        <translation>Die neue Fabrikgröße unterscheidet sich von der vorherigen!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="368"/>
        <source>All previously optimised layouts will get deleted!</source>
        <translation>Alle zuvor optimierten Layouts werden gelöscht!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="542"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="main_view.py" line="543"/>
        <source>Facility {} already exist.</source>
        <translation>Fabrikobjekt {} existiert bereits.</translation>
    </message>
    <message>
        <location filename="main_view.py" line="545"/>
        <source>Change the name of {} or it won&apos;t get imported!
New Name:</source>
        <translation>Ändern Sie den Namen von {} oder es wird nicht importiert!
Neuer Name:</translation>
    </message>
    <message>
        <location filename="main_view.py" line="632"/>
        <source>An error occured during optimization!</source>
        <translation>Bei der Optimierung ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="661"/>
        <source>Not enough information!</source>
        <translation>Nicht genug Information!</translation>
    </message>
    <message>
        <location filename="main_view.py" line="805"/>
        <source>Do you really want to reset the current layout?</source>
        <translation>Do you really want to reset the current layout?</translation>
    </message>
    <message>
        <location filename="main_view.py" line="806"/>
        <source>All facilties except the fixed ones, will be deleted!</source>
        <translation>Alle Fabrikobjekte außer den festen werden gelöscht!</translation>
    </message>
</context>
<context>
    <name>OptimizeDialog</name>
    <message>
        <location filename="optimize_dialog.py" line="75"/>
        <source> (Available cores: </source>
        <translation> (verfügbare Kerne: </translation>
    </message>
    <message>
        <location filename="optimize_dialog.py" line="740"/>
        <source>All facilities must be placed for this opening type!</source>
        <translation>Alle Fabrikobjekte müssen für diesen Öffnungstyp platziert werden!</translation>
    </message>
    <message>
        <location filename="optimize_dialog.py" line="1066"/>
        <source>You have already optimized the factory. Any further optimization would lose all of your progress.</source>
        <translation>Sie haben die Fabrik bereits optimiert. Bei jeder weiteren Optimierung würde Sie Ihren gesamten Fortschritt verlieren.</translation>
    </message>
    <message>
        <location filename="optimize_dialog.py" line="1067"/>
        <source>Choose &apos;Ist-Layout&apos; to improve your current layout!</source>
        <translation>Wählen Sie &quot;Ist-Layout&quot;, um Ihr aktuelles Layout zu verbessern!</translation>
    </message>
</context>
<context>
    <name>PrePairwiseComparisonDialog</name>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Choose your weighting criteria&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Wählen Sie Ihre Zielkriterien&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="31"/>
        <source>Criteria</source>
        <translation>Zielkriterien</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="58"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="65"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="PrePairwaiseComparisonDialog.ui" line="89"/>
        <source>Non criteria</source>
        <translation>Keine Kriterien</translation>
    </message>
</context>
<context>
    <name>StartDialog</name>
    <message>
        <location filename="start_dialog.py" line="28"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="start_dialog.py" line="29"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
</context>
<context>
    <name>WeightDialog</name>
    <message>
        <location filename="weight_dialog.py" line="244"/>
        <source>Weighting error!</source>
        <translation>Gewichtungsfehler!</translation>
    </message>
    <message>
        <location filename="weight_dialog.py" line="246"/>
        <source>The sum of the weights must be 1!
 Actual sum: {}</source>
        <translation>Die Summe der Gewichtung muss 1 bzw. 100% sein!
 Aktuelle Summe: {}</translation>
    </message>
</context>
<context>
    <name>compare_dialog</name>
    <message>
        <location filename="CompareDialog.ui" line="14"/>
        <source>Results</source>
        <translation>Ergebnisse</translation>
    </message>
    <message>
        <location filename="CompareDialog.ui" line="20"/>
        <source>&lt;h1&gt;Results&lt;/h1&gt;
</source>
        <translation>&lt;h1&gt;Ergebnisse&lt;/h1&gt;
</translation>
    </message>
</context>
<context>
    <name>evaluation_table_dialog</name>
    <message>
        <location filename="EvaluationTableDialog.ui" line="14"/>
        <source>Evaluation</source>
        <translation>Auswertung</translation>
    </message>
</context>
<context>
    <name>facility_dialog</name>
    <message>
        <location filename="FacilityDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="20"/>
        <source>Facility Name</source>
        <translation>Name des Fabrikobjekts</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="38"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="61"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="35"/>
        <source>Switch to the previous facility.</source>
        <translation>Wechseln Sie zur vorherigen Farbikobjekt.</translation>
    </message>
    <message>
        <location filename="FacilityDialog.ui" line="58"/>
        <source>Switch to the next facility.</source>
        <translation>Wechseln Sie zum nächsten Fabrikobjekt.</translation>
    </message>
</context>
<context>
    <name>factory_size_dialog</name>
    <message>
        <location filename="FactorySizeDialog.ui" line="14"/>
        <source>Factory size</source>
        <translation>Fabrikdimensionen</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="22"/>
        <source>Edge length of the unit cells [m]</source>
        <translation>Kantenlänge der Elementarzellen [m]</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="65"/>
        <source>Factory width [m]</source>
        <translation>Fabrikbreite [m]</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="98"/>
        <source>Factory height [m]</source>
        <translation>Fabriklänge [m]</translation>
    </message>
    <message>
        <location filename="FactorySizeDialog.ui" line="120"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>help_dialog</name>
    <message>
        <location filename="HelpDialog.ui" line="14"/>
        <source>Guide</source>
        <translation type="obsolete">Anleitung</translation>
    </message>
</context>
<context>
    <name>main_window</name>
    <message>
        <location filename="MeFaPMainView.ui" line="14"/>
        <source>MeFaP</source>
        <translation>MeFaP</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;View: Standard&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Ansicht: Standard&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="113"/>
        <source>Evaluate</source>
        <translation>Bewerten</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="123"/>
        <source>Optimize</source>
        <translation>Optimieren</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="145"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="158"/>
        <source>Factory data</source>
        <translation>Fabrikdaten</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="171"/>
        <source>Views</source>
        <translation>Ansichten</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="189"/>
        <source>Parameter</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="198"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="220"/>
        <source>Control parameters</source>
        <translation>Steuerparameter</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="228"/>
        <source>New layout</source>
        <translation>Neues Layout</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="239"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="247"/>
        <source>Save layout as</source>
        <translation>Layout speichern unter</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="255"/>
        <source>Load layout ...</source>
        <translation>Layout laden...</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="266"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="274"/>
        <source>Machine park</source>
        <translation>Fabrikobjektliste</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="339"/>
        <source>Material flow</source>
        <translation>Materialfluss</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="331"/>
        <source>Communication flow</source>
        <translation>Kommunikationsfluss</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="295"/>
        <source>Cell properties</source>
        <translation>Quellen und Senken</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="300"/>
        <source>Lighting</source>
        <translation>Beleuchtung</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="305"/>
        <source>Elevation profile</source>
        <translation>Höhenprofil</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="310"/>
        <source>Ceiling load</source>
        <translation>Deckentraglast</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="315"/>
        <source>Weighting</source>
        <translation>Gewichtung</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="320"/>
        <source>Save layout</source>
        <translation>Layout speichern</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="344"/>
        <source>Floor load</source>
        <translation>Bodentraglast</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="349"/>
        <source>Restrictive Area&apos;s</source>
        <translation>Restriktive Nutzungsbereiche</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="354"/>
        <source>Results</source>
        <translation>Ergebnisse</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="362"/>
        <source>Layout comparison</source>
        <translation>Layoutvergleich</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="370"/>
        <source>Extended Settings</source>
        <translation>Erweiterte Einstellungen</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="378"/>
        <source>Media requirement</source>
        <translation>Medienanforderung</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="205"/>
        <source>Erweitert</source>
        <translation>Erweitert</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="234"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="261"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="269"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="280"/>
        <source>Ctrl+I</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="323"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="357"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="365"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="373"/>
        <source>Ctrl+,</source>
        <translation></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="383"/>
        <source>Environmental factors</source>
        <translation>Umgebungseinflüsse</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="388"/>
        <source>All Layouts</source>
        <translation>Alle Layoutvarianten</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="33"/>
        <source>Hi, I am a Logo.</source>
        <translation>Hallo, ich bin ein Logo.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="49"/>
        <source>This shows which aspects of the layout are currently being displayed to you.</source>
        <translation>Dies zeigt, welche Aspekte des Layouts Ihnen gerade angezeigt werden.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="88"/>
        <source>Recalculates all routes between the facilities.</source>
        <translation>Berechnet alle Routen zwischen den Farbikobjekten neu.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="91"/>
        <source>Recalculate paths</source>
        <translation>Transportwege neu berechnen</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="98"/>
        <source>Resets the current layout.</source>
        <translation>Setzt das aktuelle Layout zurück.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="101"/>
        <source>Reset</source>
        <translation>Layout zurücksetzen</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="110"/>
        <source>When all the facilities have been placed, an evaluation of the current layout can be carried out here.</source>
        <translation>Wenn alle Farbikobjekte platziert wurden, kann hier eine Bewertung des aktuellen Layouts durchgeführt werden.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="120"/>
        <source>The optimization of the current layout can be started here.</source>
        <translation>Hier kann mit der Optimierung des aktuellen Layouts begonnen werden.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="393"/>
        <source>Noise</source>
        <translation>Schallausbreitung</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="398"/>
        <source>Media</source>
        <translation>Medien</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="403"/>
        <source>Factory size</source>
        <translation>Fabrikdimensionen</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="408"/>
        <source>Excluded areas</source>
        <translation>Ausgeschlossene Fabrikbereiche</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="413"/>
        <source>Import excel</source>
        <translation>Import Excel</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="223"/>
        <source>Settings on the control parameters can be made here.</source>
        <translation>Hier können Einstellungen zu den Steuerungsparametern vorgenommen werden.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="231"/>
        <source>A new factory can be created here.</source>
        <translation>Hier kann eine neue Fabrik angelegt werden.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="242"/>
        <source>Here you switch to the standard view of the factory.</source>
        <translation>Hier wechseln Sie zur Standardansicht der Fabrik.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="250"/>
        <source>Save factory</source>
        <translation>Fabrik speichern</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="258"/>
        <source>Load fayout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="277"/>
        <source>Here you can see and adjust all information about the facilities.</source>
        <translation>Hier können Sie alle Informationen über die Einrichtungen einsehen und einstellen.</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="418"/>
        <source>Anleitung</source>
        <translation>Anleitung</translation>
    </message>
    <message>
        <location filename="MeFaPMainView.ui" line="423"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
</context>
<context>
    <name>optimize_dialog</name>
    <message>
        <location filename="OptimizeDialog.ui" line="14"/>
        <source>Optimization settings</source>
        <translation>Optimierungseinstellungen</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="44"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Metaheuristics
&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Metaheuristiken
&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="53"/>
        <source>Tabu Search (TS)</source>
        <translation>Tabu Search (TS)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="58"/>
        <source>Simulated Annealing (SA)</source>
        <translation>Simulated Annealing (SA)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="63"/>
        <source>Genetic Algorithm (GA)</source>
        <translation>Genetischer Algorithmus (GA)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="68"/>
        <source>GA_TS</source>
        <translation>GA &amp; TS</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="73"/>
        <source>GA_SA</source>
        <translation>GA &amp; SA</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="103"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Tabu Search&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Tabu Search&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="583"/>
        <source>Length of tabu list</source>
        <translation>Länge der Tabu-Liste</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="590"/>
        <source>Iteration</source>
        <translation>Iterationen</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="177"/>
        <source>
&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Simulated Annealing&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>
&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Simulated Annealing&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="197"/>
        <source>Starting temperatur)</source>
        <translation>Starttemperatur</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="851"/>
        <source>Cooling rate</source>
        <translation>Abkühlungsrate</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="868"/>
        <source>Increment</source>
        <translation>Schrittweite</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="260"/>
        <source>Cooling type</source>
        <translation>Abkühlungsart</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="890"/>
        <source>Linear</source>
        <translation>Linear</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="895"/>
        <source>Logarithm fast</source>
        <translation>Logarithmisch (schnell)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="900"/>
        <source>Logarithm slow</source>
        <translation>Logarithmisch (langsam)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="298"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Genetic Algorithm&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;Genetischer Algorithmus&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="727"/>
        <source>Population size</source>
        <translation>Populationsgröße</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="741"/>
        <source>Number of generations</source>
        <translation>Generationen</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="755"/>
        <source>Crossover rate</source>
        <translation>Crossoverrate</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="762"/>
        <source>Number of elite parents</source>
        <translation>Anzahl der Elite-Eltern</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="776"/>
        <source>Mutation rate</source>
        <translation>Mutationsrate</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="783"/>
        <source>Mutation max shift step</source>
        <translation>Maximaler Verschiebungsschritt der Mutation</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="797"/>
        <source>Mutation max exchange</source>
        <translation>Maximale Anzahl zu vertauschender Fabrikobjekte</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="811"/>
        <source>Mutation type</source>
        <translation>Mutationstyp</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="819"/>
        <source>Shift mutation</source>
        <translation>Mutation: Verschieben</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="824"/>
        <source>Exchange mutation</source>
        <translation>Mutation: Vertauschen</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="829"/>
        <source>Shift &amp; Exchange mutation</source>
        <translation>Mutation: Verschieben &amp; Vertauschen</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="514"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA_TS&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA &amp; TS&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="908"/>
        <source>Number take over variants</source>
        <translation>Anzahl weiterbetrachteter Varianten</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="713"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA_SA&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;GA &amp; SA&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="837"/>
        <source>Starting Temperature</source>
        <translation>Starttemperatur</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="882"/>
        <source>Cooling Type</source>
        <translation>Abkühlungsart</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="979"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Opening procedure&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Eröffnungsverfahren&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="988"/>
        <source>Random positioning and selection</source>
        <translation>Zufällige Auswahl und Positionierung</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="998"/>
        <source>Random positioning with factory objects sorted in descending order</source>
        <translation>Zufällige Positionierung der Fabrikobjekte sortiert nach absteigender Fläche</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1005"/>
        <source>Schmigalla</source>
        <translation>Dreicksverfahren nach Schmigalla</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1012"/>
        <source>Actual layout</source>
        <translation>Ist-Layout</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1042"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Neighborhood search type&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Nachbarschaftssuchtyp&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1051"/>
        <source>Local Reallocation Search</source>
        <translation>Lokale Neupositionierung (LRS)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1074"/>
        <source>Free Area Search</source>
        <translation>Freiflächensuche (OAS)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1094"/>
        <source>Multiple Elements Exchange Search</source>
        <translation>Vertauschen von Fabrikobjekten (MEES)</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1122"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Optional restrictions&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Optionale Einschränkungen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1131"/>
        <source>Rotatability</source>
        <translation>Drehbarkeit</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1141"/>
        <source>Mirror</source>
        <translation>vertikale Spiegelung</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Other&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-weight:600; text-decoration: underline;&quot;&gt;Sonstiges&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1162"/>
        <source>Pathplanning</source>
        <translation>Wegeplanung</translation>
    </message>
    <message>
        <location filename="OptimizeDialog.ui" line="1182"/>
        <source>Multicore</source>
        <translation>Multicore</translation>
    </message>
</context>
<context>
    <name>pairwise_comparison_dialog</name>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="14"/>
        <source>Pairwise Comparison</source>
        <translation>Paarweiser Vergleich</translation>
    </message>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Which one is more important?&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;h1&gt;Welches ist wichtiger?&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="64"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="PairwaiseComparisonDialog.ui" line="78"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>set_size_dialog</name>
    <message>
        <location filename="FactorySetSize.ui" line="14"/>
        <source>Set size</source>
        <translation>Größe festlegen</translation>
    </message>
</context>
<context>
    <name>start_dialog</name>
    <message>
        <location filename="StartDialog.ui" line="26"/>
        <source>MeFaP - Start</source>
        <translation>MeFaP - Start</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="35"/>
        <source>New factory layout</source>
        <translation>Neues Fabriklayout</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="55"/>
        <source>Welcome to MeFaP</source>
        <translation>Willkommen zu MeFaP</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="68"/>
        <source>Load factory layout</source>
        <translation>Fabriklayout laden</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="32"/>
        <source>A new factory can be created here</source>
        <translation>Hier kann eine neue Fabrik angelegt werden</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="65"/>
        <source>If you already created a factory you can reload it here.</source>
        <translation>Wenn Sie bereits eine Factory erstellt haben, können Sie diese hier neu laden.</translation>
    </message>
    <message>
        <location filename="StartDialog.ui" line="137"/>
        <source>Here you can change between multiple languages.</source>
        <translation>Hier können Sie zwischen mehreren Sprachen wechseln.</translation>
    </message>
</context>
<context>
    <name>table_dialog</name>
    <message>
        <location filename="TableDialog.ui" line="14"/>
        <source>Factory information</source>
        <translation>Fabrikinformationen</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="24"/>
        <source>Material flow</source>
        <translation>Materialfluss</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="34"/>
        <source>Communication flow</source>
        <translation>Kommunikationsfluss</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="44"/>
        <source>Machine park</source>
        <translation>Fabrikobjekte</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="54"/>
        <source>Medien</source>
        <translation>Medien</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="64"/>
        <source>Umgebungsfaktoren</source>
        <translation>Umgebungsfaktoren</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="88"/>
        <source>Add Facility</source>
        <translation>Fabrikobjekt hinzufügen</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="101"/>
        <source>Delete Facility</source>
        <translation>Lösche Fabrikobjekt</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="111"/>
        <source>Add Media</source>
        <translation>Medien hinzufügen</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="121"/>
        <source>Delete Media</source>
        <translation>Medien löschen</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="85"/>
        <source>A new facility can be added here.</source>
        <translation>Eine neues Fabrikobjekt kann hier hinzugefügt werden.</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="98"/>
        <source>All marked facilities can be deleted here.</source>
        <translation>All marked facilities can be deleted here.</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="108"/>
        <source>A new medium can be added to the factory and named here.</source>
        <translation>Hier kann der Fabrik eun neues Medium hinzugefügt, sowie benannt werden.</translation>
    </message>
    <message>
        <location filename="TableDialog.ui" line="118"/>
        <source>All marked media can be deleted here.</source>
        <translation>Alle markierten Medien, können hier gelöscht werden.</translation>
    </message>
</context>
<context>
    <name>weight_dialog</name>
    <message>
        <location filename="WeightDialog.ui" line="14"/>
        <source>Weighting</source>
        <translation>Gewichtung</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Set weighting&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:24pt; font-weight:600;&quot;&gt;Gewichtung einstellen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="42"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Versatility&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Wandlungsfähigkeit&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="54"/>
        <source>Media availability</source>
        <translation>Medienverfügbarkeit</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="81"/>
        <source>Media compatibility</source>
        <translation>Medienkompatibilität</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="123"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Material flow and logistics&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Materialfluss und Logistik&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="135"/>
        <source>Material flow length</source>
        <translation>Materialflusslänge</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="162"/>
        <source>No overlap</source>
        <translation>Überschneidung des Materialfluss</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="189"/>
        <source>Continuity</source>
        <translation>Stetigkeit des Materialfluss</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="216"/>
        <source>Land use</source>
        <translation>Flächennutzungsgrad</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="258"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Environmental influences&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Umwelteinflüsse&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="270"/>
        <source>Lighting</source>
        <translation>Beleuchtung</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="297"/>
        <source>Quiet</source>
        <translation>Ruhe / Schallausbreitung</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="324"/>
        <source>Vibration</source>
        <translation>Vibration</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="351"/>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="378"/>
        <source>Cleanliness</source>
        <translation>Sauberkeit</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="420"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Communication&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Kommunikation&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="432"/>
        <source>Direct communication</source>
        <translation>Direkte Kommunikation</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="459"/>
        <source>Formal communication</source>
        <translation>Formale Kommunikation</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="494"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Total:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Summe:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="504"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;0&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;0&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="516"/>
        <source>Balance of all criteria</source>
        <translation>Gleichgewichtung aller Kriterien</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="523"/>
        <source>Pairwise comparison</source>
        <translation>Paarweiser Vergleich</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="532"/>
        <source>Evaluate</source>
        <translation>Bewerten</translation>
    </message>
    <message>
        <location filename="WeightDialog.ui" line="542"/>
        <source>Abbrechen</source>
        <translation>Abbrechen</translation>
    </message>
</context>
</TS>
