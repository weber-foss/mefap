import os

import numpy as np
import math
import random

from mefap.optimization.positionFacility import position_facility_on_cell
from mefap.optimization.positionFacility import position_facility_next_to
from mefap.optimization.positionPath import try_path_planning_for_facility
from mefap.optimization.positionPath import try_path_planning_for_layout
from mefap.factory.reset_layout import reset_layout
from mefap.optimization.ga_sub_functions import encode_individual
from mefap.optimization.ga_sub_functions import decode_individual
from mefap.optimization.neighborhood_search import free_area_search


def random_layout_generation(factory):
    break_counter = 0
    random_list = random.sample(range(len(factory.facility_list)), len(factory.facility_list))
    y_length, x_length = factory.layout.shape
    while len(random_list) > 0:

        if break_counter > len(factory.facility_list) ** 2:
            test = 1
            print("Process {}: Layout initialisation failed: random positioning".format(os.getpid()))
            reset_layout(factory)
            return False

        # random generate new location
        y_random = random.randint(0, y_length)
        x_random = random.randint(0, x_length)

        # set rotation and mirror
        if factory.optimization.rotation:
            rotation_random = random.choice([0, 1, 2, 3])
        else:
            rotation_random = 0
        if factory.optimization.mirror:
            mirror_random = random.choice([0, 1])
        else:
            mirror_random = 0

        # check if facility is fixed
        if not factory.facility_list[random_list[0]].fixed_position:
            # position facility | write_path = False | *args = True
            try:
                position_facility_on_cell(factory, factory.facility_list[random_list[0]], x_random, y_random,
                                         rotation_random, mirror_random, False, True)
                random_list.pop(0)
            except:
                break_counter += 1
        else:
            random_list.pop(0)

    for facility in range(0, len(factory.facility_list), 1):
        try:
            try_path_planning_for_facility(factory, factory.facility_list[facility],
                                           factory.facility_list[facility].curr_position[1],
                                           factory.facility_list[facility].curr_position[0],
                                           factory.facility_list[facility].curr_rotation,
                                           factory.facility_list[facility].curr_mirror, write_path=True)
        except:
            test = 1
            print("Layout initialisation failed")
            reset_layout(factory)
            return False

    return True


def random_layout_generation_by_facility_size(factory):
    # init variables
    help_list = np.zeros((len(factory.facility_list), 2), dtype=int)
    fixed_list = []

    for index in range(0, len(factory.facility_list), 1):
        # check if fixed position
        if not factory.facility_list[index].fixed_position:
            help_list[index, 0] = factory.facility_list[index].index_num
            help_list[index, 1] = factory.facility_list[index].cell_num
        else:
            fixed_list.append(index)

    # delete fixed facilities from help_list
    while len(fixed_list) > 0:
        help_list = np.delete(help_list, fixed_list[-1], axis=0)
        fixed_list.pop(-1)

    # sort array by size
    help_list = help_list[help_list[:, 1].argsort()[::-1]]
    y_length, x_length = factory.layout.shape
    break_counter = 0
    break_counter_sub = 10
    last_facility_list = []
    while len(help_list) > 0:

        break_counter += 1
        if break_counter > len(factory.facility_list) ** 2:
            test = 1
            print("Process {}: Layout initialisation failed: random positioning by facility size".format(os.getpid()))
            reset_layout(factory)
            return False

        # set rotation
        if factory.optimization.rotation:
            rotation_random = random.choice([0, 1, 2, 3])
        else:
            rotation_random = 0

        # set mirror
        if factory.optimization.mirror:
            mirror_random = random.choice([0, 1])
        else:
            mirror_random = 0

        # try position facility
        if break_counter_sub >= 10:
            y_random = random.randint(0, y_length)
            x_random = random.randint(0, x_length)
            # write_path = True | *args = True
            try:
                position_facility_on_cell(factory, factory.facility_list[help_list[0, 0]], x_random, y_random,
                                          rotation_random, mirror_random, True, True)
                last_facility_list.append(help_list[0, 0])
                help_list = np.delete(help_list, (0), axis=0)
                break_counter_sub = 0
            except:
                pass
        else:
            break_counter_sub = 0
            while break_counter_sub < 10:
                direction_random = random.choice(["right", "left", "up", "down"])
                if position_facility_next_to(factory, factory.facility_list[last_facility_list[-1]],
                                             factory.facility_list[help_list[0, 0]],
                                             direction_random, rotation_random, mirror_random,
                                             write_path=True):
                    last_facility_list.append(help_list[0, 0])
                    help_list = np.delete(help_list, (0), axis=0)
                    break_counter_sub = 0
                    break
                else:
                    break_counter_sub += 1
    """
    if not try_path_planning_for_layout(factory, write_path=True):
        print("Fehler in der Layout-Initialisierung")
    """
    return True


def schmigalla_layout_generation(factory):
    # generate Dreiecksmatrix
    mf_matrix_transpose = factory.mf_matrix.transpose()
    mf_matrix_full = factory.mf_matrix + mf_matrix_transpose
    mf_matrix_half = np.tril(mf_matrix_full)
    # init variables
    rank_list = []
    last_facility_list = []

    # choose first pair
    facility_pair = np.where(mf_matrix_half == np.amax(mf_matrix_half))
    curr_facility = factory.facility_list[facility_pair[0].item(0)]

    # check if facility is fixed -> don't position
    if not curr_facility.fixed_position:
        # position first facility
        x_coordinate = math.ceil(factory.layout.shape[0] / 2) - curr_facility.cell_width1
        y_coordinate = math.ceil(factory.layout.shape[1] / 2) - math.ceil(curr_facility.cell_width1 / 2)

        # guaranty that facility is in borders
        while x_coordinate < 0:
            x_coordinate += 1
        while y_coordinate < 0:
            y_coordinate += 1

        # set rotation
        if factory.optimization.rotation:
            rotation_random = random.choice([0, 1, 2, 3])
        else:
            rotation_random = 0

        # set mirror
        if factory.optimization.mirror:
            mirror_random = random.choice([0, 1])
        else:
            mirror_random = 0

        try:
            position_facility_on_cell(factory, curr_facility, x_coordinate, y_coordinate,
                                      rotation_random, mirror_random, write_path=0)
        except:
            print("Process {}: Layout initialisation failed: schmigalla".format(os.getpid()))
            return False

    # add to last_fac_list
    last_facility_list.append(curr_facility.index_num)

    # calculate rank_list
    # add facility to rank_list, if it´s not fixed
    if not factory.facility_list[facility_pair[1].item(0)].fixed_position:
        rank_list.append(facility_pair[1].item(0))
    else:
        last_facility_list.append(facility_pair[1].item(0))
    # generate rank_list
    # rank_list = [facility_pair[1].item(0)]

    # generate index_list: shows which facilities are not added to rank list
    index_list = [1] * len(factory.facility_list)
    # delete first and second facility from index_list
    index_list[facility_pair[0].item(0)] = 0
    index_list[facility_pair[1].item(0)] = 0
    # generate schmigalla material flow table
    help_list = np.array(mf_matrix_full[facility_pair[0].item(0)])
    mf_matrix_new_row = np.array(mf_matrix_full[facility_pair[1].item(0)])
    help_list = help_list + mf_matrix_new_row
    help_list = help_list * index_list
    # set breaker for while: breaks if all facilities are in rank_list
    breaker_index_list = max(index_list)

    while breaker_index_list > 0:
        # search for facility with highest mf_connection
        facility_pair = np.where(help_list == np.amax(help_list))
        # add facility to rank_list, if it´s not fixed
        if not factory.facility_list[facility_pair[0].item(0)].fixed_position:
            rank_list.append(facility_pair[0].item(0))
        # delete from index_list
        index_list[facility_pair[0].item(0)] = 0
        # update schmigalla material flow table
        mf_matrix_new_row = np.array(mf_matrix_full[facility_pair[0].item(0)])
        help_list = help_list + mf_matrix_new_row
        help_list = help_list * index_list
        # check if something is left
        breaker_index_list = max(index_list)

    # position facilities
    break_counter = 0
    break_counter_sub = 0
    while len(rank_list) > 0:

        break_counter += 1
        if break_counter > len(factory.facility_list) ** 2:
            test = 1
            print("Process {}: Layout initialisation failed: schmigalla".format(os.getpid()))
            reset_layout(factory)
            return False

        # set rotation and mirror
        if factory.optimization.rotation:
            rotation_random = random.choice([0, 1, 2, 3])
        else:
            rotation_random = 0

        if factory.optimization.mirror:
            mirror_random = random.choice([0, 1])
        else:
            mirror_random = 0

        if break_counter_sub >= 10:
            y_random = random.randint(0, factory.layout.shape[0])
            x_random = random.randint(0, factory.layout.shape[1])
            try:
                position_facility_on_cell(factory, factory.facility_list[rank_list[0]], x_random, y_random,
                                          rotation_random, mirror_random, write_path=True)
                last_facility_list.append(rank_list[0])
                rank_list.pop(0)
                break_counter_sub = 0
            except:
                pass

        else:
            break_counter_sub = 0
            while break_counter_sub < 10:
                direction_random = random.choice(["right", "left", "up", "down"])
                if position_facility_next_to(factory, factory.facility_list[last_facility_list[-1]],
                                             factory.facility_list[rank_list[0]],
                                             direction_random, rotation_random, mirror_random,
                                             write_path=True):
                    last_facility_list.append(rank_list[0])
                    rank_list.pop(0)
                    break_counter_sub = 0
                    break
                else:
                    break_counter_sub += 1
                    # todo: unterbrechnung des Dreiecks verhindern, use last_facility_list[-2]

    return True


def post_npp_init(factory, individual):
    """

    :param individual:
    :param factory:
    :return:
    """

    # ensure that all facilities have a connection to the first
    for index in range(factory.mf_matrix.shape[0]):
        factory.mf_matrix[individual[0][0], index] += 1
        factory.mf_matrix[index, individual[0][0]] += 1

    first_flag = True

    for gen in individual:
        # get facility
        facility = factory.facility_list[gen[0]]
        # check if fixed
        if facility.fixed_position:
            continue

        if not try_locate_facility(factory, facility, gen):
            if not free_area_search(factory, False, False, facility.index_num, old_rotation=gen[1][2]):

                # reset mf_matrix
                for index in range(factory.mf_matrix.shape[0]):
                    factory.mf_matrix[individual[0][0], index] -= 1
                    factory.mf_matrix[index, individual[0][0]] -= 1

                reset_layout(factory)
                break

        # check if first
        if first_flag:
            # path_around_first(factory.layout, facility, factory.parameter)
            first_flag = False

    else:

        # reset mf_matrix
        for index in range(factory.mf_matrix.shape[0]):
            factory.mf_matrix[individual[0][0], index] -= 1
            factory.mf_matrix[index, individual[0][0]] -= 1

        # check if all facilities are positioned
        for facility in factory.facility_list:
            if facility.curr_position == [None, None]:
                breakpoint()

        return True

    # try GA decode method
    if decode_individual(factory, individual):
        return True
    else:
        reset_layout(factory)
        # breakpoint()
        return False


def try_locate_facility(factory, facility, gen):
    facility_y_pos = gen[1][0]
    facility_x_pos = gen[1][1]
    facility_rotation = gen[1][2]
    facility_mirror = gen[1][3]

    # set random positioning parameter
    if factory.optimization.rotation:
        rotation_list = [0, 1, 2, 3]
        random.shuffle(rotation_list)
        facility_rotation_index = rotation_list.index(facility_rotation)
        rotation_list.pop(facility_rotation_index)
        rotation_list.insert(0, facility_rotation)
    else:
        rotation_list = [0]

    if factory.optimization.mirror:
        mirror_list = [0, 1]
        if facility_mirror == 1:
            mirror_list.sort(reverse=True)
    else:
        mirror_list = [0]

    for distance_to_curr_pos in range(0, factory.optimization.lrs_step_size +
                                      round(0.5 * factory.optimization.lrs_step_size), 1):

        for y_start in range(-1 * distance_to_curr_pos + facility_y_pos, distance_to_curr_pos + facility_y_pos, 1):
            for x_start in range(-1 * distance_to_curr_pos + facility_x_pos, distance_to_curr_pos + facility_x_pos, 1):
                for mirror in mirror_list:
                    for rotation in rotation_list:
                        try:
                            position_facility_on_cell(factory, facility, x_start, y_start, rotation, mirror,
                                                      True, True)
                            return True
                        except:
                            pass
    return False
