

def check_functional_areas(factory, curr_facility, x_start, y_start, rotation):
    if rotation == 0 or rotation == 2:
        y_facility_width = curr_facility.cell_width2
        x_facility_width = curr_facility.cell_width1
    elif rotation == 1 or rotation == 3:
        y_facility_width = curr_facility.cell_width1
        x_facility_width = curr_facility.cell_width2

    # get functional area type
    functional_area_type = factory.get_facilitytype_by_name(curr_facility.facility_type)

    # check whether RestrictiveArea/facility_type is restricted (set in array)
    if functional_area_type.id in factory.restrictive_area:

        for y_index in range(0, y_facility_width, 1):
            for x_index in range(0, x_facility_width, 1):
                # check if all cells of facility are positioned in RestrictiveArea
                if factory.restrictive_area[y_start + y_index, x_start + x_index] != functional_area_type.id:
                    raise Exception("Fabrikobjekt muss in definierter Funktionsfläche positioniert werden. \n"
                                    "Facility must be positioned in defined functional area.")

    return True
