from scipy.spatial import distance
from mefap.optimization.positionSourceAndSink import calculate_position_source_sink
from mefap.optimization.astar_algorithm import a_star_algorithm
from mefap.optimization.deletePath import delete_paths_from_path_list
from mefap.optimization.deletePath import delete_paths_from_layout


def try_position_road_on_source_sink_side(factory, curr_facility, x_start, y_start, rotation, mirror, write_path):
    """
    checks if it's possible to position path / road on source side and sink side of a facility
    """

    # check for facilities with 1x1-shape
    if curr_facility.cell_width1 == 1 and curr_facility.cell_width2 == 1:
        # ToDo: try_pos_path(write_path = 0)
        y_facility_width, x_facility_width = 1, 1
        """
        if road_and_free_test_loop(factory, y_start, x_start - 1, y_facility_width, x_layout_width=1) \
                or road_and_free_test_loop(factory, y_start - 1, x_start, y_layout_width=1,
                                           x_layout_width=x_facility_width) \
                or road_and_free_test_loop(factory, y_start, x_start + x_facility_width, y_facility_width,
                                           x_layout_width=1) \
                or road_and_free_test_loop(factory, y_start + y_facility_width, x_start, y_layout_width=1,
                                           x_layout_width=x_facility_width):
            return True
        """
        if road_or_free_test_loop(factory, y_start, x_start - 1, y_facility_width, x_layout_width=1) \
                or road_or_free_test_loop(factory, y_start - 1, x_start, y_layout_width=1,
                                          x_layout_width=x_facility_width) \
                or road_or_free_test_loop(factory, y_start, x_start + x_facility_width, y_facility_width,
                                          x_layout_width=1) \
                or road_or_free_test_loop(factory, y_start + y_facility_width, x_start, y_layout_width=1,
                                          x_layout_width=x_facility_width):
            return True
        else:
            return False

    # check for facilities with 1xN-shape
    # elif curr_facility.cell_width1 == 1 or curr_facility.cell_width2 == 1:
    # ToDo: welche Seite? -> Source and Sink only on one side
    #        pass

    # check for facilities with NxM-shape
    else:
        # set spreading direction based on rotation
        y_facility_width, x_facility_width = calc_width_on_rotation_zwo(curr_facility, rotation)

        # iteration 1: source | iteration 2: sink
        for iteration in range(2):

            # add rotation
            if iteration == 0:
                new_sequence_factor = curr_facility.source_sequence_factor + rotation
                on_edge = curr_facility.source_sequence_edge
            else:
                new_sequence_factor = curr_facility.sink_sequence_factor + rotation
                on_edge = curr_facility.sink_sequence_edge
                cell_flac_1 = cell_flac

            # add mirror
            if mirror == 1:
                if on_edge:
                    if new_sequence_factor == 0 or new_sequence_factor == 4:
                        new_sequence_factor = 1
                    elif new_sequence_factor == 1 or new_sequence_factor == 5:
                        new_sequence_factor = 0
                    elif new_sequence_factor == 2 or new_sequence_factor == 6:
                        new_sequence_factor = 3
                    elif new_sequence_factor == 3 or new_sequence_factor == 7:
                        new_sequence_factor = 2

                else:
                    if new_sequence_factor == 0 or new_sequence_factor == 2:
                        new_sequence_factor = 2
                    elif new_sequence_factor == 2 or new_sequence_factor == 6:
                        new_sequence_factor = 0

            if new_sequence_factor == 0 or new_sequence_factor == 4:
                # check if there is enough space for a road on the left side of the new facility position
                if not road_or_free_test_loop(factory, y_start, x_start - 1, y_facility_width, x_layout_width=1):
                    if on_edge:
                        # if source/sink on edge check also other side
                        if not road_or_free_test_loop(factory, y_start - 1, x_start,
                                                      y_layout_width=1, x_layout_width=x_facility_width):
                            return False
                        else:
                            cell_flac = [y_start - 1, x_start]
                    else:
                        return False
                else:
                    cell_flac = [y_start, x_start - 1]

            elif new_sequence_factor == 1 or new_sequence_factor == 5:
                # check if there is enough space for a road above the new facility position
                if not road_or_free_test_loop(factory, y_start - 1, x_start,
                                              y_layout_width=1, x_layout_width=x_facility_width):
                    if on_edge:
                        # if source/sink on edge check also other side
                        if not road_or_free_test_loop(factory, y_start, x_start + x_facility_width,
                                                      y_facility_width, x_layout_width=1):
                            return False
                        else:
                            cell_flac = [y_start, x_start + x_facility_width]
                    else:
                        return False
                else:
                    cell_flac = [y_start - 1, x_start]

            elif new_sequence_factor == 2 or new_sequence_factor == 6:
                # check if there is enough space for a road on the right side of the new facility position
                if not road_or_free_test_loop(factory, y_start, x_start + x_facility_width,
                                              y_facility_width, x_layout_width=1):
                    if on_edge:
                        # if source/sink on edge check also other side
                        if not road_or_free_test_loop(factory, y_start + y_facility_width, x_start,
                                                      y_layout_width=1, x_layout_width=x_facility_width):
                            return False
                        else:
                            cell_flac = [y_start + y_facility_width, x_start]
                    else:
                        return False
                else:
                    cell_flac = [y_start, x_start + x_facility_width]

            elif new_sequence_factor == 3 or new_sequence_factor == 7:
                # check if there is enough space for a road under the new facility position
                if not road_or_free_test_loop(factory, y_start + y_facility_width, x_start,
                                              y_layout_width=1, x_layout_width=x_facility_width):
                    if on_edge:
                        # if source/sink on edge check also other side
                        if not road_or_free_test_loop(factory, y_start, x_start - 1, y_facility_width,
                                                      x_layout_width=1):
                            return False
                        else:
                            cell_flac = [y_start, x_start - 1]
                    else:
                        return False
                else:
                    cell_flac = [y_start + y_facility_width, x_start]

    if write_path:
        factory.layout[cell_flac_1[0], cell_flac_1[1]] = factory.parameter["local_road_value"]
        factory.layout[cell_flac[0], cell_flac[1]] = factory.parameter["local_road_value"]

    return True


def road_or_free_test_loop(factory, y_start, x_start, y_layout_width, x_layout_width):
    """ checks if all cells on specific side are free or road """
    for y_index in range(y_start, y_start + y_layout_width, 1):
        for x_index in range(x_start, x_start + x_layout_width, 1):
            # check layout borders
            if factory.layout.shape[0] - 1 >= y_index >= 0 and factory.layout.shape[1] - 1 >= x_index >= 0 and \
                    y_index >= 0 and x_index >= 0:
                # check if cells are free
                if factory.layout[y_index, x_index] == factory.parameter["empty_cell_value"] \
                        or factory.layout[y_index, x_index] == factory.parameter["local_road_value"] \
                        or factory.layout[y_index, x_index] == factory.parameter["fixed_road_value"]:
                    pass
                else:
                    return False
            else:
                return False
    return True


def road_and_free_test_loop(factory, y_start, x_start, y_layout_width, x_layout_width):
    """ checks if all cells on specific side are free or road, counts road cells """
    road_count = 0
    for y_index in range(y_start, y_start + y_layout_width, 1):
        for x_index in range(x_start, x_start + x_layout_width, 1):
            # check layout borders
            if factory.layout.shape[0] - 1 >= y_index >= 0 and factory.layout.shape[1] - 1 >= x_index >= 0:
                # check if cells are free
                if factory.layout[y_index, x_index] != factory.parameter["empty_cell_value"]:  # todo: fail out of range
                    road_count = 0
                    return False  # , road_count
                elif factory.layout[y_index, x_index] == factory.parameter["local_road_value"]:
                    road_count += 1
                elif factory.layout[y_index, x_index] == factory.parameter["fixed_road_value"]:
                    road_count += 1
            else:
                return False
    return True  # , road_count


def try_position_road_around(factory, curr_facility, x_start, y_start, rotation):
    """
    try to place transport infrastructure around a facility
    """
    # get shape of the factory
    y_layout_shape, x_layout_shape = factory.layout.shape
    #  set spreading direction based in rotation
    y_facility_width, x_facility_width = calc_width_on_rotation_zwo(curr_facility, rotation)

    # start checking for road space
    # check if there is enough space for a road on the left side of the new facility position
    if x_start - 1 != -1:
        if not road_or_free_test_loop(factory, y_start, x_start - 1, y_facility_width, 1):
            return False
    # check if there is enough space for a road on the right side of the new facility position
    if x_start + x_facility_width != x_layout_shape:
        if not road_or_free_test_loop(factory, y_start, x_start + x_facility_width, y_facility_width, 1):
            return False
    # check if there is enough space for a road above the new facility position
    if y_start - 1 != -1:
        if not road_or_free_test_loop(factory, y_start - 1, x_start, 1, x_facility_width):
            return False
    # check if there is enough space for a road under the new facility position
    if y_start + y_facility_width != y_layout_shape:
        if not road_or_free_test_loop(factory, y_start + y_facility_width, x_start, 1, x_facility_width):
            return False
    # check if there is enough space for a road on the above left corner of the new facility position
    if x_start - 1 != -1 and y_start - 1 != -1:
        if not road_or_free_test_loop(factory, y_start - 1, x_start - 1, 1, 1):
            return False
    # check if there is enough space for a road on the above right corner of the new facility position
    if x_start - 1 != -1 and y_start + y_facility_width != y_layout_shape:
        if not road_or_free_test_loop(factory, y_start + y_facility_width, x_start - 1, 1, 1):
            return False
    # check if there is enough space for a road on the under left corner of the new facility position
    if x_start + x_facility_width != x_layout_shape and y_start - 1 != - 1:
        if not road_or_free_test_loop(factory, y_start - 1, x_start + x_facility_width, 1, 1):
            return False
    # check if there is enough space for a road on the under right corner of the new facility position
    if x_start + x_facility_width != x_layout_shape and y_start + y_facility_width != y_layout_shape:
        if not road_or_free_test_loop(factory, y_start + y_facility_width, x_start + x_facility_width, 1, 1):
            return False
    return True


def try_path_planning_for_layout(factory, write_path):
    """
    try to plan paths for the entire layout respectively for all facilities
    """
    break_condition = False
    for facility in range(len(factory.facility_list)):
        # check if facility is located
        if factory.facility_list[facility].curr_position == [None, None]:
            continue
        source_pos, sink_pos = calculate_position_source_sink(factory.facility_list[facility],
                                                              factory.facility_list[facility].curr_position[1],
                                                              factory.facility_list[facility].curr_position[0],
                                                              factory.facility_list[facility].curr_rotation,
                                                              factory.facility_list[facility].curr_mirror)
        for column in range(factory.mf_matrix.shape[0]):
            if (factory.mf_matrix[factory.facility_list[facility].index_num, column] != 0.0
                or factory.cf_matrix[factory.facility_list[facility].index_num, column] != 0.0) \
                    and factory.facility_list[column].curr_source_position != [None, None] \
                    and factory.facility_list[column].index_num != factory.facility_list[facility].index_num:

                if factory.parameter["no_path_planning"]:
                    path = distance.euclidean(sink_pos, factory.facility_list[column].curr_source_position)
                    if write_path:
                        factory.path_list[factory.facility_list[facility].index_num].paths[factory.facility_list
                        [column].index_num] = path
                else:

                    path = a_star_algorithm(factory, sink_pos, factory.facility_list[column].curr_source_position,
                                            write_path)
                    if path != 0:
                        if write_path:
                            factory.path_list[factory.facility_list[facility].index_num].paths[factory.facility_list
                            [column].index_num] = path
                    else:
                        break_condition = True
                        break

        if break_condition:
            if write_path:
                delete_paths_from_layout(factory, factory.facility_list[facility])
                # delete_paths_from_path_list is included
            else:
                delete_paths_from_path_list(factory, factory.facility_list[facility])
            raise Exception("Wegeplanung nicht erfolgreich \nCouldn't plan paths")  # return False
    return True


def try_path_planning_for_facility(factory, curr_facility, x_start_cell, y_start_cell, rotation, mirror, write_path):
    source_pos, sink_pos = calculate_position_source_sink(curr_facility, x_start_cell, y_start_cell, rotation, mirror)
    break_condition = False
    # path from sink of curr_facility to another source
    for column in range(factory.mf_matrix.shape[0]):
        if (factory.mf_matrix[curr_facility.index_num, column] != 0.0
            or factory.cf_matrix[curr_facility.index_num, column] != 0.0) \
                and factory.facility_list[column].curr_source_position != [None, None] \
                and factory.facility_list[column].index_num != curr_facility.index_num:

            if factory.parameter["no_path_planning"]:
                path = distance.euclidean(sink_pos, factory.facility_list[column].curr_source_position)
                if write_path:
                    factory.path_list[curr_facility.index_num].paths[factory.facility_list[column].index_num] = path
            else:
                path = a_star_algorithm(factory, sink_pos, factory.facility_list[column].curr_source_position,
                                        write_path)
                if path != 0:
                    if write_path:
                        factory.path_list[curr_facility.index_num].paths[factory.facility_list[column].index_num] = path
                else:
                    break_condition = True
                    break

    if not break_condition:
        # path from another sink to source of curr_facility
        for row in range(factory.mf_matrix.shape[1]):
            if (factory.mf_matrix[row, curr_facility.index_num] != 0.0
                or factory.cf_matrix[row, curr_facility.index_num] != 0.0) \
                    and factory.facility_list[row].curr_sink_position != [None, None] \
                    and factory.facility_list[row].index_num != curr_facility.index_num:

                if factory.parameter["no_path_planning"]:
                    path = distance.euclidean(factory.facility_list[row].curr_sink_position, source_pos)
                    if write_path:
                        factory.path_list[factory.facility_list[row].index_num].paths[curr_facility.index_num] = path
                else:

                    path = a_star_algorithm(factory, factory.facility_list[row].curr_sink_position, source_pos,
                                            write_path)
                    if path != 0:
                        if write_path:
                            factory.path_list[factory.facility_list[row].index_num].paths[curr_facility.index_num] = \
                                path
                    else:
                        break_condition = True
                        break

    if break_condition:
        if write_path:
            delete_paths_from_layout(factory, curr_facility)
            # delete_paths_from_path_list is included
        else:
            delete_paths_from_path_list(factory, curr_facility)
        raise Exception("Wegeplanung nicht erfolgreich \nCouldn't plan paths")  # return False
    return True


def do_position_paths(factory, path_list):
    for path in range(len(path_list)):
        for index in path_list[path]:
            factory.layout[index] = factory.parameter["local_road_value"]


def calc_width_on_rotation_zwo(facility, rotation):
    if rotation == 0 or rotation == 2:
        facility_width_x = facility.cell_width1
        facility_width_y = facility.cell_width2
    elif rotation == 1 or rotation == 3:
        facility_width_x = facility.cell_width2
        facility_width_y = facility.cell_width1
    return facility_width_y, facility_width_x
