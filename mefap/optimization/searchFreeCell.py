import numpy as np


class FreeAreas(object):
    def __init__(self, x_start, y_start, x_iterator_end, y_iterator_end):
        self.x_start_position = x_start
        self.y_start_position = y_start
        self.x_ende_position = x_start + x_iterator_end
        self.y_ende_position = y_start + y_iterator_end
        self.x_shape = x_iterator_end + 1  # real width (pythonStart = 0 ...)
        self.y_shape = y_iterator_end + 1
        self.area = self.x_shape * self.y_shape


def search_free_areas_road_around(curr_layout):
    free_areas_list = []
    try_free_area_list = []
    min_area_width = 1
    y_layout_shape, x_layout_shape = curr_layout.shape
    for y_index in range(0, y_layout_shape, 1):
        for x_index in range(0, x_layout_shape, 1):
            if curr_layout[y_index, x_index] == -1:  # Prüfen, ob Startzelle frei, wenn nicht zur nächsten
                for direction in range(0, 2, 1):
                    x_stop = False
                    y_stop = False
                    x_iterator = 0
                    y_iterator = 0
                    if direction == 0:
                        x_iterator_end = x_layout_shape - x_index - 1
                        y_iterator_end = None
                        # Starte Suche in x-Richtung
                        while not y_stop:
                            x_iterator = 0
                            x_stop = False
                            while not x_stop:
                                if x_iterator <= x_iterator_end and y_iterator <= (y_layout_shape - y_index - 1) \
                                        and curr_layout[y_index + y_iterator, x_index + x_iterator] == -1:
                                    x_iterator += 1
                                else:
                                    x_stop = True
                                    if y_iterator > 0 and x_iterator <= x_iterator_end:
                                        if x_iterator >= min_area_width:
                                            # save first area in tryList
                                            y_iterator_end = y_iterator - 1
                                            try_free_area_list.append(FreeAreas(x_index, y_index,
                                                                                x_iterator_end, y_iterator_end))
                                            # update x_iterator_end, um weitere Fläche zu suchen
                                            x_iterator_end = x_iterator - 1
                                        else:
                                            y_stop = True
                                            y_iterator_end = y_iterator - 1
                                    elif y_iterator == 0:
                                        x_iterator_end = x_iterator - 1
                                        if x_iterator == 1:  # freie Area ist genau eine Zelle
                                            y_stop = True
                                            y_iterator_end = y_iterator
                            y_iterator += 1
                    else:
                        # Starte Suche in y-Richtung
                        x_iterator_end = None
                        y_iterator_end = y_layout_shape - y_index - 1
                        while not x_stop:
                            y_iterator = 0
                            y_stop = False
                            while not y_stop:
                                if y_iterator <= y_iterator_end and x_iterator <= (x_layout_shape - x_index - 1) \
                                        and curr_layout[y_index + y_iterator, x_index + x_iterator] == -1:
                                    y_iterator += 1
                                else:
                                    y_stop = True
                                    if x_iterator > 0 and y_iterator <= y_iterator_end:
                                        if y_iterator >= min_area_width:
                                            # save first area in tryList
                                            x_iterator_end = x_iterator - 1
                                            try_free_area_list.append(FreeAreas(x_index, y_index,
                                                                                x_iterator_end, y_iterator_end))
                                            # update x_iterator_end, um weitere Fläche zu suchen
                                            y_iterator_end = y_iterator - 1
                                        else:
                                            x_stop = True
                                            x_iterator_end = x_iterator - 1
                                    elif x_iterator == 0:
                                        y_iterator_end = y_iterator - 1
                                        if y_iterator == 1:  # freie Area ist genau eine Zelle
                                            x_stop = True
                                            x_iterator_end = x_iterator
                            x_iterator += 1
                    try_free_area_list.append(FreeAreas(x_index, y_index, x_iterator_end, y_iterator_end))
                    # In Liste eintragen
                    while len(try_free_area_list) > 0:
                        free_area_check = True
                        if try_free_area_list[0].x_shape >= min_area_width \
                                and try_free_area_list[0].y_shape >= min_area_width:
                            for indexList in range(0, len(free_areas_list), 1):
                                # nur in Liste aufnehmen, wenn die Freifläche mindestens 3x3 -> min_area_width
                                # nur in Liste aufnehmen, wenn es keine schon bekannte größere Freifläche gibt
                                if try_free_area_list[0].x_start_position >= free_areas_list[indexList].x_start_position \
                                        and try_free_area_list[0].y_start_position >= free_areas_list[
                                    indexList].y_start_position \
                                        and try_free_area_list[0].x_ende_position <= free_areas_list[
                                    indexList].x_ende_position \
                                        and try_free_area_list[0].y_ende_position <= free_areas_list[
                                    indexList].y_ende_position:
                                    free_area_check = False
                                    try_free_area_list.pop(0)
                                    break
                        else:
                            free_area_check = False
                            try_free_area_list.pop(0)
                        # add to FreeAreaList, if true
                        if free_area_check:
                            free_areas_list.append(try_free_area_list[0])
                            try_free_area_list.pop(0)
    return free_areas_list


def free_area_test(curr_layout, y_index, x_index, y_layout_shape, x_layout_shape, facility_max_width,
                   facility_min_width):
    y_iterator = 0
    x_iterator = 0
    y_iterator_end = y_layout_shape - y_index - 1
    x_iterator_end = x_layout_shape - x_index - 1
    if y_iterator_end < facility_min_width - 1 or x_iterator_end < facility_min_width - 1:
        return False, False, False, False
    y_stop, x_stop = False, False
    x_max, y_max = True, True
    x_min, y_min = False, False
    facility_maxmin_x_width = facility_max_width

    while y_iterator < facility_max_width and y_iterator <= y_iterator_end and not y_stop:
        while x_iterator < facility_maxmin_x_width and x_iterator <= x_iterator_end and not x_stop:
            if curr_layout[y_index + y_iterator, x_index + x_iterator] == -1:
                x_iterator += 1
            else:
                # quadrant 1
                if x_iterator <= facility_min_width - 1 and y_iterator <= facility_min_width - 1:
                    y_max, x_max = False, False
                    y_stop, x_stop = True, True
                # quadrant 2
                elif x_iterator > facility_min_width - 1 >= y_iterator:  # facility_max_width > x_iterator
                    x_max, x_min = False, True
                    x_stop = True
                    facility_maxmin_x_width = facility_min_width
                # quadrant 3
                elif y_iterator > facility_min_width - 1 >= x_iterator:  # > x_iterator:
                    y_max, y_min = False, True
                    y_stop, x_stop = True, True
                # quadrant 4
                elif y_iterator > facility_min_width - 1 and x_iterator > facility_min_width - 1:
                    # x_max = True, because quadrant 2 must be true!
                    y_max, y_min = False, True
                    x_stop = True
                    facility_maxmin_x_width = facility_min_width
        else:
            if x_iterator_end < x_iterator <= facility_max_width - 1:
                facility_maxmin_x_width = facility_min_width
                x_max, x_min = False, True

        if x_min or x_max:
            y_iterator += 1
            x_iterator = 0
            x_stop = False
        else:
            break

    return y_min, x_min, y_max, x_max


def search_free_areas_on_wall(curr_layout, facility_width1, facility_width2):
    free_areas_list = []
    facility_max_width = max(facility_width1, facility_width2)
    facility_min_width = min(facility_width1, facility_width2)
    y_layout_shape, x_layout_shape = curr_layout.shape

    # search in y_walls
    for y_index in range(y_layout_shape - facility_min_width + 1):
        for x_index in (0, x_layout_shape - facility_min_width, x_layout_shape - facility_max_width):
            if curr_layout[y_index, x_index] == -1:
                y_min, x_min, y_max, x_max = free_area_test(curr_layout, y_index, x_index, y_layout_shape,
                                                            x_layout_shape, facility_max_width,
                                                            facility_min_width)
                # test if curr_point = pareto_point
                if (x_max and y_max) or (x_max and y_min) or (x_min and y_max):
                    if x_max and y_max:
                        x_iterator_end = facility_max_width - 1
                        y_iterator_end = facility_max_width - 1

                    elif x_max and y_min:
                        x_iterator_end = facility_max_width - 1
                        y_iterator_end = facility_min_width - 1

                    elif x_min and y_max:
                        x_iterator_end = facility_min_width - 1
                        y_iterator_end = facility_max_width - 1

                    # store in free_area_list
                    free_areas_list.append(FreeAreas(x_index, y_index, x_iterator_end, y_iterator_end))

    # search on x_walls
    for y_index in (0, y_layout_shape - facility_min_width, y_layout_shape - facility_max_width):
        for x_index in range(x_layout_shape - facility_min_width + 1):
            if curr_layout[y_index, x_index] == -1:
                y_min, x_min, y_max, x_max = free_area_test(curr_layout, y_index, x_index, y_layout_shape,
                                                            x_layout_shape, facility_max_width,
                                                            facility_min_width)
                # test if curr_point = pareto_point
                if (x_max and y_max) or (x_max and y_min) or (x_min and y_max):
                    if x_max and y_max:
                        x_iterator_end = facility_max_width - 1
                        y_iterator_end = facility_max_width - 1

                    elif x_max and y_min:
                        x_iterator_end = facility_max_width - 1
                        y_iterator_end = facility_min_width - 1

                    elif x_min and y_max:
                        x_iterator_end = facility_min_width - 1
                        y_iterator_end = facility_max_width - 1

                    # store in free_area_list
                    free_areas_list.append(FreeAreas(x_index, y_index, x_iterator_end, y_iterator_end))

    return free_areas_list
