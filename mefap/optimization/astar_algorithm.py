# Sample code from https://www.redblobgames.com/pathfinding/a-star/
# Copyright 2014 Red Blob Games <redblobgames@gmail.com>
#
# Feel free to use this code in your own projects, including commercial projects
# License: Apache v2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>

import heapq
import collections
import numpy as np


class Queue:
    def __init__(self):
        self.elements = collections.deque()

    def empty(self):
        return len(self.elements) == 0

    def put(self, x):
        self.elements.append(x)

    def get(self):
        return self.elements.popleft()


# utility functions for dealing with square grids
def from_id_width(id, width):
    return (id % width, id // width)


def draw_tile(graph, id, style, width):
    r = "."
    if 'number' in style and id in style['number']: r = "%d" % style['number'][id]
    if 'point_to' in style and style['point_to'].get(id, None) is not None:
        (x1, y1) = id
        (x2, y2) = style['point_to'][id]
        if x2 == x1 + 1: r = ">"
        if x2 == x1 - 1: r = "<"
        if y2 == y1 + 1: r = "v"
        if y2 == y1 - 1: r = "^"
    if 'start' in style and id == style['start']: r = "A"
    if 'goal' in style and id == style['goal']: r = "Z"
    if 'path' in style and id in style['path']: r = "@"
    if id in graph.walls: r = "#"
    return r


def draw_grid(graph, width=2, **style):
    for y in range(graph.height):
        for x in range(graph.width):
            print("%%-%ds" % width % draw_tile(graph, (x, y), style, width), end="")
        print()


class SquareGrid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.walls = []

    def in_bounds(self, id):
        (x, y) = id
        return 0 <= x < self.width and 0 <= y < self.height

    def passable(self, id):
        return id not in self.walls

    def neighbors(self, id):
        (x, y) = id
        results = [(x + 1, y), (x, y - 1), (x - 1, y), (x, y + 1)]
        if (x + y) % 2 == 0: results.reverse()  # aesthetics
        results = filter(self.in_bounds, results)
        results = filter(self.passable, results)
        return results


class GridWithWeights(SquareGrid):
    def __init__(self, width, height):
        super().__init__(width, height)
        self.weights = {}

    def cost(self, from_node, to_node):
        return self.weights.get(to_node, 1)


class PriorityQueue:
    def __init__(self):
        self.elements = []

    def empty(self):
        return len(self.elements) == 0

    def put(self, item, priority):
        heapq.heappush(self.elements, (priority, item))

    def get(self):
        return heapq.heappop(self.elements)[1]


# thanks to @m1sp <Jaiden Mispy> for this simpler version of reconstruct_path that doesn't have duplicate entries
def reconstruct_path(came_from, start, goal):
    current = goal
    path = []
    while current != start:
        path.append(current)
        try:
            current = came_from[current]
        except KeyError:
            path = 0
            return path
    path.append(start)  # optional
    path.reverse()  # optional
    return path


def heuristic(a, b):
    # manhattan distance
    (x1, y1) = a
    (x2, y2) = b
    return abs(x1 - x2) + abs(y1 - y2)


def a_star_search(graph, start, goal):
    frontier = PriorityQueue()
    frontier.put(start, 0)
    came_from = {}
    cost_so_far = {}
    came_from[start] = None
    cost_so_far[start] = 0
    while not frontier.empty():
        current = frontier.get()
        if current == goal:
            break
        for next in graph.neighbors(current):
            new_cost = cost_so_far[current] + graph.cost(current, next)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + heuristic(goal, next) * (1.0 + 1/1000) # tie breaking
                frontier.put(next, priority)
                came_from[next] = current
    return came_from, cost_so_far


def grid_converter(factory, start_pos, end_pos):
    """ This method converts a layout to a maze that can be used for A*-Algorithm and sets path costs """
    # init grid
    y_shape, x_shape = np.shape(factory.layout)
    new_maze = GridWithWeights(y_shape, x_shape)
    # init walls
    walls_list = []
    for index, value in np.ndenumerate(factory.layout):
        if factory.layout[index] != factory.parameter["empty_cell_value"] \
                and factory.layout[index] != factory.parameter["local_road_value"] \
                and factory.layout[index] != factory.parameter["fixed_road_value"] \
                and index != start_pos \
                and index != end_pos:
            walls_list.append(index)
    # add walls
    new_maze.walls = walls_list
    # init weights
    empty_cells = []
    local_roads = []
    fixed_roads = []
    for index, value in np.ndenumerate(factory.layout):
        if factory.layout[index] == factory.parameter["empty_cell_value"]:
            empty_cells.append(index)
        elif factory.layout[index] == factory.parameter["local_road_value"]:
            local_roads.append(index)
        elif factory.layout[index] == factory.parameter["fixed_road_value"]:
            fixed_roads.append(index)
    # add weights

    new_maze.weights = {loc: factory.parameter["empty_cell_costs"] for loc in empty_cells}
    new_maze.weights.update({loc: factory.parameter["local_road_costs"] for loc in local_roads})
    new_maze.weights.update({loc: factory.parameter["fixed_road_costs"] for loc in fixed_roads})
    return new_maze


def a_star_algorithm(factory, start_pos, end_pos, write_path):
    """ This method executes the layout convert and A*-Algorithm """
    start_pos = tuple(start_pos)
    end_pos = tuple(end_pos)
    new_maze = grid_converter(factory, start_pos, end_pos)
    came_from, cost_so_far = a_star_search(new_maze, start_pos, end_pos)
    # grid = draw_grid(new_maze, width=3, number=cost_so_far, start=start_pos, goal=end_pos)
    shortest_path = reconstruct_path(came_from, start_pos, end_pos)
    # cut first and last element of list
    if shortest_path != 0:
        shortest_path.pop(0)  # cut first element of list
        shortest_path.pop()  # cut last element of the list
        if write_path:
            if shortest_path != 0:
                for index in shortest_path:
                    if factory.layout[index] != factory.parameter["fixed_road_value"]:
                        factory.layout[index] = factory.parameter["local_road_value"]
    return shortest_path  # len(shortest_path)
