import logging
import os

from mefap.optimization.metaheuristics import tabu_search
from mefap.optimization.metaheuristics import simulated_annealing
from mefap.optimization.metaheuristics import genetic_algorithm
from mefap.optimization.ga_sub_functions import encode_population
from mefap.factory.reset_layout import reset_layout
from mefap.factory.optimization import set_optimization_npp
from mefap.optimization.ga_sub_functions import decode_individual
from mefap.factory.database import Database
from mefap.optimization.positionPath import try_path_planning_for_facility
from mefap.evaluation.noise_propagation.factory_wall_corner_cells import wall_and_corner_cells
from mefap.evaluation.noise_propagation.calc_noise_propagation import calc_noise_room_condition
from mefap.evaluation.evaluate_layout import reset_quamfab_min
from mefap.optimization.deletePath import delete_all_paths
from mefap.optimization.positionPath import try_path_planning_for_layout
from mefap.evaluation.noise_propagation.noise_propagation_conditions import NoisePropagationMatrix


def npp_optimization(factory):
    """

    :param factory:
    :return:
    """

    # setup wall and corner cell lists
    factory.noise_wall_list, factory.noise_corner_list = wall_and_corner_cells(factory.layout, factory.noise_area)

    """
    # calc noise_room_condition
    if factory.parameter["noise_semi_diffuse"]:
        calc_noise_room_condition(factory)
        
    # setup noise_propagation_matrix
    factory.noise_propagation_matrix = NoisePropagationMatrix(factory.columns, factory.rows)
    factory.noise_propagation_matrix.calc_noise_propagation_conditions(factory)
    """

    # reset quamfab_min -> need for second optimization
    reset_quamfab_min(factory)

    # reset paths -> need for second optimization
    delete_all_paths(factory)
    try_path_planning_for_layout(factory, write_path=True)

    if factory.parameter["no_path_planning"]:
        for facility in factory.facility_list:
            if facility.fixed_position:
                try_path_planning_for_facility(factory, facility, facility.curr_position[1], facility.curr_position[0],
                                               facility.curr_rotation, facility.curr_mirror, write_path=True)

    # choose metaheuristics
    if factory.optimization.metaheuristic_type.value == 0:
        print("Process {}: Starting tabu search".format(os.getpid()))
        logging.info("Process {}: Starting tabu search".format(os.getpid()))
        tabu_search(factory)
    elif factory.optimization.metaheuristic_type.value == 1:
        print("Process {}: Starting simulated annealing".format(os.getpid()))
        logging.info("Process {}: Starting simulated annealing".format(os.getpid()))
        simulated_annealing(factory)
    else:
        print("Process {}: Starting genetic algorithm".format(os.getpid()))
        logging.info("Process {}: Starting genetic algorithm".format(os.getpid()))
        genetic_algorithm(factory)

    # second optimization step if npp
    if factory.parameter["no_path_planning"]:
        # npp init for TS and SA
        if not factory.optimization.metaheuristic_type.value == 4:
            break_flac = False
            # set control parameter
            set_optimization_npp(factory)
            # delete npp_objectives
            overwrite_eval_data(factory)
            # init database
            database = Database()
            # save layouts in database
            database.transfer_all_variants_between_databases(factory)
            # decode population
            population = encode_population(database)
            # delete variants from factory
            factory.delete_all_from_layouts_data()
            # reset layout
            reset_layout(factory)
            # allow path planning
            factory.parameter["no_path_planning"] = False
            # choose first individual
            individual_counter = len(population) - 1
            individual = population[individual_counter]
            # create layout with paths
            while not decode_individual(factory, individual):
                # OLD: post_npp_init(factory, individual):
                # reset layout
                reset_layout(factory)
                # choose first individual
                individual_counter -= 1
                if individual_counter >= 0:  # len(population):
                    individual = population[individual_counter]
                else:
                    break_flac = True
                    factory.delete_all_from_layouts_data()
                    raise Exception("Process {}: post_npp_init was NOT successful".format(os.getpid()))

            else:
                # init successful
                print("Process {}: post_npp_init was successful".format(os.getpid()))

        # npp_init for GA
        else:
            break_flac = False
            # set control parameter
            set_optimization_npp(factory)
            # reset layout
            reset_layout(factory)
            # allow path planning
            factory.parameter["no_path_planning"] = False
            # delete npp_objectives
            overwrite_eval_data(factory)
            # init successful
            print("Process {}: post_npp_init was successful".format(os.getpid()))

        # todo: optimize for more then 1 variant (GA_LC, GA_TS, GA_SA)?
        if not break_flac:
            # choose metaheuristics
            if factory.optimization.metaheuristic_type.value == 0 or factory.optimization.metaheuristic_type.value == 5:
                print("Process {}: Starting tabu search".format(os.getpid()))
                logging.info("Process {}: Starting tabu search".format(os.getpid()))
                tabu_search(factory)
            elif factory.optimization.metaheuristic_type.value == 1 or factory.optimization.metaheuristic_type.value == 6:
                print("Process {}: Starting simulated annealing".format(os.getpid()))
                logging.info("Process {}: Starting simulated annealing".format(os.getpid()))
                simulated_annealing(factory)
            else:  # 4
                print("Process {}: Starting genetic algorithm".format(os.getpid()))
                logging.info("Process {}: Starting genetic algorithm".format(os.getpid()))
                genetic_algorithm(factory)


def overwrite_eval_data(factory):
    # min_objectives = ["mf_distance", "mf_overlapping", "mf_constancy", "direct_com", "formal_com", "noise"]
    # max_objectives = ["media_availability", "vibration", "cleanliness", "temperature"]
    # rate_objectives = ["illuminance", "media_compatibility", "area_utilization"]

    for variant in factory.layouts_evaluation_data:
        # variant.media_compatibility[0] = [None, None]
        # variant.media_availability[0] = [None, None]
        variant.mf_distance[0] = variant.mf_distance[0] * 1000
        variant.mf_overlapping[0] = variant.mf_overlapping[0] * 1000
        variant.mf_constancy[0] = variant.mf_constancy[0] * 1000
        # variant.area_utilization[0] = [None, None]
        # variant.illuminance[0] = [None, None]
        # variant.noise[0] = [None, None]
        # variant.vibration[0] = [None, None]
        # variant.temperature[0] = [None, None]
        # variant.cleanliness[0] = [None, None]
        variant.direct_com[0] = variant.direct_com[0] * 1000
        variant.formal_com[0] = variant.formal_com[0] * 1000
        variant.weight_sum = 0

    # factory.quamfab_min.media_compatibility = [None, None]
    # factory.quamfab_min.media_availability = [None, None]
    factory.quamfab_min.mf_distance = [None, None]
    factory.quamfab_min.mf_overlapping = [None, None]
    factory.quamfab_min.mf_constancy = [None, None]
    # factory.quamfab_min.area_utilization = [None, None]
    # factory.quamfab_min.illuminance = [None, None]
    # factory.quamfab_min.noise = [None, None]
    # factory.quamfab_min.vibration = [None, None]
    # factory.quamfab_min.temperature = [None, None]
    # factory.quamfab_min.cleanliness = [None, None]
    factory.quamfab_min.direct_com = [None, None]
    factory.quamfab_min.formal_com = [None, None]
    factory.quamfab_min.weight_sum = 0
