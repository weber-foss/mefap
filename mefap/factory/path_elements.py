class PathElements:
    """
    This class contains all path for a facility.ﬂ
    """

    def __init__(self, curr_facility_index):
        self.facility_index = curr_facility_index
        self.paths = []
