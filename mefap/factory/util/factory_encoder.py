from json import JSONEncoder
from typing import List

import numpy as np
from PySide2.QtGui import QColor

from mefap.evaluation.noise_propagation.noise_propagation_conditions import NoisePropagationMatrix
from mefap.evaluation.noise_propagation.noise_propagation_list_entry import NoisePropagationEntry
from mefap.factory.evaluation_data import EvaluationData
from mefap.factory.facility_attributs import FacilityAttributes
from mefap.factory.facility_characteristics import FacilityCharacteristics
from mefap.factory.facility_data import FacilityData
from mefap.factory.media_availability import MediaAvailability
from mefap.factory.media_requirements_import import MediaRequirementsImport
from mefap.factory.noise_area import NoiseArea
from mefap.factory.optimization import Optimization
from mefap.factory.optimization_doe import Optimization_DOE
from mefap.factory.path_elements import PathElements
from mefap.factory.restriction import Restriction
from mefap.factory.restrictive_area import RestrictiveArea
from mefap.factory.util.light_level import LightLevel
from mefap.factory.util.metaheuristik_type import MetaheuristicType
from mefap.factory.util.mutation_type import MutationType
from mefap.factory.util.neighborhood_search_type import NeighborhoodSearchType
from mefap.factory.util.opening_type import OpeningType
from mefap.factory.util.sa_cooling_type import SimulatedAnnealingCoolingType
from mefap.factory.weighting import Weighting


class FactoryEncoder(JSONEncoder):
    """
    This class is for encoding the factory into an JSON file. And is needed because the standard
    JSONEncoder isn't capable of encoding all data types used within the FactoryModel object.
    """

    def default(self, obj):
        """
        This method is overwriting the default method of the JSONEncoder.
        It is needed for handling the different data types within the FactoryModel.
        :param obj: The Object which should be encoded.
        :return: The encoded object as String
        """
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, MediaAvailability) or isinstance(obj, MediaRequirementsImport) \
                or isinstance(obj, EvaluationData) \
                or isinstance(obj, Restriction) \
                or isinstance(obj, RestrictiveArea) \
                or isinstance(obj, FacilityAttributes) \
                or isinstance(obj, FacilityCharacteristics) \
                or isinstance(obj, PathElements) \
                or isinstance(obj, FacilityData) \
                or isinstance(obj, Optimization) \
                or isinstance(obj, Optimization_DOE) \
                or isinstance(obj, Weighting) \
                or isinstance(obj, NoiseArea)\
                or isinstance(obj, NoisePropagationMatrix)\
                or isinstance(obj, NoisePropagationEntry):
            return obj.__dict__
        elif isinstance(obj, LightLevel) or isinstance(obj, MetaheuristicType) \
                or isinstance(obj, MutationType) \
                or isinstance(obj, NeighborhoodSearchType) \
                or isinstance(obj, OpeningType) \
                or isinstance(obj, SimulatedAnnealingCoolingType):
            return {"__enum__": str(obj)}
        elif isinstance(obj, QColor):
            return obj.getRgb()
        return JSONEncoder.default(self, obj)


def enum_decode(enc_object):
    """
    This method checks for the "__enum__" keyword inside the encoded string.
    If the keyword is found the corresponding Enum object is returned.
    The method is used as object_hook in the json.load method.
    :param enc_object: A Json encoded string
    :return: If no enum is found it returns the input, else the enum object.
    """
    if "__enum__" in enc_object:
        name, member = enc_object["__enum__"].split(".")
        if name == "MetaheuristicType":
            return getattr(MetaheuristicType, member)
        elif name == "MutationType":
            return getattr(MutationType, member)
        elif name == "LightLevel":
            return getattr(LightLevel, member)
        elif name == "NeighborhoodSearchType":
            return getattr(NeighborhoodSearchType, member)
        elif name == "OpeningType":
            return getattr(OpeningType, member)
        elif name == "SimulatedAnnealingCoolingType":
            return getattr(SimulatedAnnealingCoolingType, member)
    else:
        return enc_object


def factory_decode(factory, temp) -> None:
    """
    Ths method is the starting point for decoding the dict from the json into the FactoryModel.
    :param factory: The factory where the dict should decoded in.
    :param temp: The dict return from the standard JSONDecoder
    :return: None
    """
    factory.rows = temp["rows"]
    factory.columns = temp["columns"]
    factory.cell_size = temp["cell_size"]
    factory.cell_zoom = temp["cell_zoom"]
    factory.layout = np.array(temp["layout"])
    factory.media_availability = __decode_media_availability_list(temp["media_availability"])
    factory.media_requirements = __decode_media_requirements_import_list(temp["media_requirements"])
    factory.quamfab = __decode_evaluation_data(temp["quamfab"])
    factory.quamfab_min = __decode_evaluation_data(temp["quamfab_min"])
    factory.path_list = __decode_path_list(temp["path_list"])
    factory.restrictions = __decode_restrictions_list(temp["restrictions"])
    factory.restrictive_area = np.array(temp["restrictive_area"])
    factory.area_types = __decode_area_types_list(temp["area_types"])
    factory.facility_list = __decode_facility_list(temp["facility_list"])
    factory.facility_characteristics = __decode_facility_characteristics(
        temp["facility_characteristics"])
    factory.illuminance_matrix = np.array(temp["illuminance_matrix"])
    factory.noise_matrix = np.array(temp["noise_matrix"])
    factory.mf_matrix = np.array(temp["mf_matrix"])
    factory.cf_matrix = np.array(temp["cf_matrix"])
    factory.parameter = temp["parameter"]
    factory.layouts = __decode_layouts(temp['layouts'])
    factory.layouts_facility_data = __decode_layouts_facility(temp['layouts_facility_data'])
    factory.layouts_path_data = __decode_layout_path_list(temp['layouts_path_data'])
    factory.layouts_evaluation_data = __decode_evaluation_data_list(temp['layouts_evaluation_data'])
    factory.optimization = __decode_optimization(temp['optimization'])
    factory.weighting = __decode_weighting(temp['weighting'])
    factory.noise_area = __decode_noise_area(temp['noise_area'])
    factory.noise_propagation_matrix = __decode_noise_propagation_matrix(temp['noise_propagation_matrix'])
    factory.noise_wall_list = temp['noise_wall_list']
    factory.noise_corner_list = temp['noise_corner_list']


def __decode_noise_area(data) -> NoiseArea:
    ne = NoiseArea(1, 1)
    ne.noise_area_matrix = np.array(data['noise_area_matrix'])
    return ne


def __decode_noise_propagation_matrix(data) -> NoisePropagationMatrix:
    npm = NoisePropagationMatrix(1, 1)
    npm.noise_prop_list = []
    for entry in data['noise_prop_list']:
        npm.noise_prop_list.append(__decode_noise_propagation_entry(entry))
    return npm


def __decode_noise_propagation_entry(data) -> NoisePropagationEntry:
    npe = NoisePropagationEntry(1, 1, 1, 1)
    npe.cell = data['cell']
    npe.layout = np.array(data['layout'])
    return npe


def __decode_media_availability_list(data) -> List[MediaAvailability]:
    """
    This method constructs a list of MediaAvailability from the given input.
    :param data: Dict parsed from JSON
    :return: List of MediaAvailability
    """
    med_avail = []
    for media_avail in data:
        med_avail.append(MediaAvailability(media_avail['name'], None, True, media_avail['availability_layout']))
    return med_avail


def __decode_media_requirements_import_list(data) -> List[MediaRequirementsImport]:
    """
    This method constructs a list of MediaRequirementsImport from the given input.
    :param data: Dict parsed from JSON
    :return: List of MediaRequirementsImport
    """
    med_reqs = []
    for med_req in data:
        med_reqs.append(MediaRequirementsImport(med_req['name'], med_req['requirements']))
    return med_reqs


def __decode_evaluation_data(data) -> EvaluationData:
    """
    This method constructs a EvaluationData object from the given input.
    :param data: Dict parsed from JSON
    :return: EvaluationData
    """
    eval_dat = EvaluationData()
    eval_dat.__dict__.clear()
    eval_dat.__dict__.update(data)
    return eval_dat


def __decode_path_list(data) -> List[PathElements]:
    """
    This method constructs a list of PathElements from the given input.
    :param data: Dict parsed from JSON
    :return: List of PathElements
    """
    path_list = []
    for path_element in data:
        element = PathElements(path_element['facility_index'])
        for path in path_element['paths']:
            path_l = []
            for tup in path:
                path_l.append((tup[0], tup[1]))
            element.paths.append(path_l)
        path_list.append(element)
    return path_list


def __decode_restrictions_list(data) -> List[Restriction]:
    """
    This method constructs a list of Restriction from the given input.
    :param data: Dict parsed from JSON
    :return: List of Restriction
    """
    restriction_list = []
    for restriction in data:
        matrix = np.array(restriction['matrix'])
        res = Restriction(restriction['name'], matrix.shape[0], matrix.shape[1],
                          restriction['max_value'])
        res.matrix = matrix
        restriction_list.append(res)
    return restriction_list


def __decode_area_types_list(data) -> List[RestrictiveArea]:
    """
    This method constructs a list of RestrictiveArea from the given input.
    :param data: Dict parsed from JSON
    :return: List of RestrictiveArea
    """
    area_list = []
    for area in data:
        area_list.append(RestrictiveArea(area['name'], area['id'],
                                         QColor(area['color'][0], area['color'][1],
                                                area['color'][2], area['color'][3])))
    return area_list


def __decode_facility_list(data) -> List[FacilityAttributes]:
    """
    This method constructs a list of FacilityAttributes from the given input.
    :param data: Dict parsed from JSON
    :return: List of FacilityAttributes
    """
    facility_list = []
    for facility in data:
        facility_list.append(FacilityAttributes(None, json_load=True, **facility))
    return facility_list


def __decode_facility_characteristics(data) -> List[FacilityCharacteristics]:
    """
    This method constructs a list of FacilityCharacteristics from the given input.
    :param data: Dict parsed from JSON
    :return: List of FacilityCharacteristics
    """
    facility_char_list = []
    for facility_char in data:
        facility_char_list.append(FacilityCharacteristics(None, None, from_json=True, **facility_char))
    return facility_char_list


def __decode_layouts(data) -> List[np.array]:
    """
    This method constructs a list of np.array from the given input.
    :param data: Dict parsed from JSON
    :return: List of np.array
    """
    layouts = []
    for layout in data:
        layouts.append(np.array(layout))
    return layouts


def __decode_layouts_facility(data) -> List[List[FacilityAttributes]]:
    """
    This method constructs a list of lists of FacilityAttributes from the given input.
    :param data: Dict parsed from JSON
    :return: List of lists of FacilityAttributes
    """
    lay_fac = []
    for fac_list in data:
        lay_fac.append(__decode_facility_list(fac_list))
    return lay_fac


def __decode_layout_path_list(data) -> List[List[PathElements]]:
    """
    This method constructs a list of lists of PathElements from the given input.
    :param data: Dict parsed from JSON
    :return: List of lists of PathElements
    """
    lay_path_list = []
    for layout in data:
        lay_path_list.append(__decode_path_list(layout))
    return lay_path_list


def __decode_evaluation_data_list(data) -> List[EvaluationData]:
    """
    This method constructs a list of EvaluationData from the given input.
    :param data: Dict parsed from JSON
    :return: List of lists of EvaluationData
    """
    eval_data = []
    for eval_d in data:
        eval_data.append(__decode_evaluation_data(eval_d))
    return eval_data


def __decode_weighting(data) -> Weighting:
    """
    This method constructs a Weighting object from the given input.
    :param data: Dict parsed from JSON
    :return: Weighting
    """
    weights = Weighting()
    weights.__dict__.clear()
    weights.__dict__.update(data)
    return weights


def __decode_optimization(data) -> Optimization:
    """
    This method constructs a Optimization object from the given input.
    :param data: Dict parsed from JSON
    :return: Optimization
    """
    opti = Optimization()
    opti.__dict__.clear()
    opti.__dict__.update(data)
    return opti
