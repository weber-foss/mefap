from enum import Enum


class LightLevel(Enum):
    """
    This Enum class is there to represent the 7 different LightLevels that can occur in the factory.
    """
    OFF = 0
    VERY_DARK = 1
    DARK = 2
    NORMAL = 3
    BRIGHT = 4
    VERY_BRIGHT = 5
    MAXIMAL = 6
